﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Xml.Serialization;

namespace RecodingService
{
	[ServiceContract(Namespace = "")]
	public interface IRecodingService
	{
		[OperationContract]
		void RecodeMessage(RecodeMessageRequest recodeRequest);
	}

	[DataContract(Namespace = "")]
	[Serializable]
	public class RecodeMessageRequest
	{
		[DataMember(Order=0)]
		public string OriginalXmlPath { get; set; }

		[DataMember(Order = 1)]
		public string CurrentXmlPath { get; set; }

		[DataMember(Order = 2)]
		public string OriginalDocPath { get; set; }

		[DataMember(Order = 3)]
		public string CurrentDocPath { get; set; }
	}

	/*/
	[DataContract(Namespace = "")]
	[Serializable]
	public class RecodeMessageResponse
	{
		[DataMember(Order = 1)]
		public string Folder { get; set; }

		[DataMember(Order = 2)]
		public string Message { get; set; }

		[DataMember(Order = 3)]
		public string ErrorMailSubject { get; set; }

		[DataMember(Order = 4)]
		public string ErrorMailAddress { get; set; }

		[DataMember(Order = 5)]
		public string ErrorFolder { get; set; }

		[DataMember(Order = 6)]
		public string DefaultErrorFolder { get; set; }

		[DataMember(Order = 7)]
		public bool MailOnSuccess { get; set; }

		[DataMember(Order = 8)]
		public string MainDocumentInputPath { get; set; }
		
		[DataMember(Order = 9)]
		public string MainDocumentOutputPath { get; set; }

		[DataMember(Order = 10)]
		public string MainDocumentErrorPath { get; set; }
	} */

	//Не используется. Пусть пока будет, чтобы не потерять, как правильно сериализовать списки
	[CollectionDataContract(Name = "MoveFilesFromXpathsListOnError", ItemName = "xpathToItem", Namespace="")]
	public class CustomList<T> : List<T>
	{
		public CustomList() : base()
		{
		}

		public CustomList(T[] items) : base()
		{
			foreach (T item in items)
			{
				Add(item);
			}
		}
	}
}