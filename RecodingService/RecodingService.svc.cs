﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.IO;
using ExchangeRecode.DataAccess;
using System.Data.Entity;
using ExchangeRecode.DataAccess.ViewModels;
using RecodingService.SimpleHelpers;
using RecodingService.Extensions;
using System.Text;
using System.Diagnostics;

namespace RecodingService
{
	public class RecodingService : IRecodingService
	{
		private CodesMappingService mappingService = new CodesMappingService();
		//private MailingService mailingServiceInstance = new MailingService();

		private DbSet<Settings> defaultSettings;
		private NotificationServiceProxy emailProxy;
		private Document document;
		private XmlFile xmlFile;
		private RecodingProcess recodProcess;
		
		/// <summary>
		/// Производит обработку запроса на перекодировку.
		/// </summary>
		/// <param name="recodeRequest">Запрос на перекодировку</param>
		/// <returns>Перекодированное сообщение и метаданные для дальнейшей обработки сообщения</returns>
		public void RecodeMessage(RecodeMessageRequest recodeRequest)
		{
			using (var db = new ExchangeRecodeDbContext())
			{
				defaultSettings = db.Settings;

				//Прокси к системе уведомлений
				emailProxy = new NotificationServiceProxy( db);

				try
				{
					DetectRecodingProcess(db, recodeRequest);

					//Теперь, определив процесс перекодировки, сообщаем его сервису рассылки, чтобы получить доступ к уникальным для этого процесса перекодировки шаблонам писем
					emailProxy.SetRecodingProcess(recodProcess);

					//По каждому правилу вызываем сервис перекодировки и делаем замену
					//TODO: Возможно стоит распараллелить, а мб вообще зашить весь этот сервис внутрь CodesMappingServicа
					foreach (var rule in recodProcess.RecodingRules)
					{
						var conditionPassed = true;

						//Проверяем условия перекодировки. Если хоть одно не сходится, то не перекодируем
						foreach (var condition in rule.Conditions)
						{
							var nodesToCheckCondition = xmlFile.Xml.SelectNodes(condition.Xpath);

							//Если Xpath выбрал какое-то значение, то все ок
							//Если нужно проверить значение определенного нода, то Xpath например такой: //Element/Child[text()="Значение1" or text()="Значение2"]
							if (nodesToCheckCondition.Count == 0)
							{
								conditionPassed = false;
								break;
							}
						}

						//Пропускаем перекодировку этого правила, если условие не прошло
						if (conditionPassed == false)
							continue;

						var nodes = xmlFile.Xml.SelectNodes(rule.XPath);

						//Если в правиле указано, что оно обязательно, но мы не нашли по нему тегов - вызываем ошибку
						if (rule.IsRequired && (nodes == null || nodes.Count == 0))
						{
							//Переносим файлы в папку с ошибками
							var fileMoveReport = MoveFilesCausedError();

							//Отправляем уведомление
							emailProxy.XpathNoMathchesFound(recodeRequest.OriginalXmlPath, rule.XPath, fileMoveReport);
							throw new NotificatedException();
						}

						//Если ничего не нашли, но правило не обязательное, продолжаем
						if (nodes == null || nodes.Count == 0)
							continue;

						foreach (XmlNode val in nodes)
						{
							//Если сказано заменить на жестко заданное значение, то так и делаем
							if (!String.IsNullOrEmpty(rule.SetToValue))
							{
								val.InnerText = rule.SetToValue;
								continue;
							}

							//Если в правиле указано, что оно обязательно, но в найденном теге нет значения - вызываем ошибку
							if (val == null || String.IsNullOrEmpty(val.InnerText))
							{
								if (rule.IsRequired)
								{
									//Переносим файлы в папку с ошибками
									var fileMoveReport = MoveFilesCausedError();

									//Отправляем уведомление
									emailProxy.NoValueToRecode(recodeRequest.OriginalXmlPath, rule.XPath, val.OuterXml, fileMoveReport);
									throw new NotificatedException();
								}
								else
									continue;
							}

							CodesMappingContract mapping;

							try
							{
								//Вызываем сервис маппинга Naviconа
								mapping = mappingService.GetMapping(rule.EntityName, rule.SourceSystemName, val.InnerText ?? val.InnerXml, rule.DestinationSystemName);
							}
							catch (Exception e)
							{
								//Переносим файлы в папку с ошибками
								var fileMoveReport = MoveFilesCausedError();

								//Отправляем уведомление
								emailProxy.CodesMappingError(recodeRequest.OriginalXmlPath, document.OriginalInputPath, rule.XPath, val.OuterXml,
									rule.EntityName, val.InnerText ?? val.InnerXml, rule.SourceSystemName, rule.DestinationSystemName,
									e, fileMoveReport);
								throw new NotificatedException();
							}

							if (mapping != null)
								val.InnerText = mapping.DestCode;
							else
							{
								//Переносим файлы в папку с ошибками
								var fileMoveReport = MoveFilesCausedError();

								string elementName = null;

								if (rule.DestinationSystemName == "SAPFIR")
								{
									elementName = GetElementNameFromNaviconMdm(val.InnerText ?? val.InnerXml, rule.EntityName, rule.SourceSystemName);
								}

								//Отправляем уведомление
								emailProxy.NoMappingsFound(recodeRequest.OriginalXmlPath, rule.XPath, val.OuterXml,
									rule.EntityName, val.InnerText ?? val.InnerXml, rule.SourceSystemName, rule.DestinationSystemName,
									fileMoveReport, elementName);
								throw new NotificatedException();
							}

						}
					}

					//Пишем файлы (xml и докум) в output папку
					WriteFilesToOutputFolders();

					//Уведомление об успехе
					if (recodProcess.MailOnSuccess)
					{
						var docInputPath = document != null ? document.OriginalInputPath : null;
						var docOutputPath = document != null ? document.OutputPath : null;

						emailProxy.Success(xmlFile.OriginalInputPath, xmlFile.OutputPath, docInputPath, docOutputPath);
					}

					return;
				}
				catch (NotificatedException)
				{
					//Ничего не делаем в этом случае. Такие ошибки обрабатываются до того, как они вызваны (аналог GOTO)
				}
				catch (Exception ex)
				{
					if (emailProxy == null)
						throw;

					var fileMoveReport = MoveFilesCausedError();

					//Отправляем уведомление
					emailProxy.GeneralError(recodeRequest.OriginalXmlPath, document!=null ? document.OriginalInputPath : null, fileMoveReport, ex);

					throw;
				}
			}
		}

		private string GetElementNameFromNaviconMdm(string id, string entityName, string sourceSystemName)
		{
			try
			{
				var connectionString = ConfigurationManager.ConnectionStrings["RecodeRulesDb"].ConnectionString;

				using (var cn = new SqlConnection(connectionString))
				{
					cn.Open();

					var cmd = new SqlCommand("GetElementsName") { CommandType = CommandType.StoredProcedure, Connection = cn };
					
					cmd.Parameters.AddWithValue("@Id", id);
					cmd.Parameters.AddWithValue("@ClientName", sourceSystemName);
					cmd.Parameters.AddWithValue("@EntityName", entityName);

					using (var da = new SqlDataAdapter(cmd))
					{
						var data = new DataTable();

						da.Fill(data);

						return (string) data.Rows[0][0];
					}
				}
			}
			catch
			{
				return null;
			}
		}

		private void DetectRecodingProcess(ExchangeRecodeDbContext db, RecodeMessageRequest recodeRequest)
		{
			//Если указан путь к документу, то получаем процесс по пути к документу
			if (!String.IsNullOrEmpty(recodeRequest.OriginalDocPath))
			{
				var documentFileDir = Path.HasExtension(recodeRequest.CurrentDocPath) ? Path.GetDirectoryName(recodeRequest.CurrentDocPath) : recodeRequest.CurrentDocPath;

				//Берем первый процесс перекодировки, у которого путь до основного документа указан соответсвующий
				recodProcess = db.RecodingProcesses.ToList().SingleOrDefault(r =>
				{
					var path = Path.HasExtension(r.MainDocumentInputFileDirectory) ? Path.GetDirectoryName(r.MainDocumentInputFileDirectory) : r.MainDocumentInputFileDirectory;

					return Paths.AreEqual(path, documentFileDir);
				});

				//Не нашли процесса перекодировки
				if (recodProcess == null)
				{
					//Заполняем данные о документе, чтобы переложить его в Error папку. Т.к. процесс перекодировки не найден, перекладываем в default папку для ошибочных файлов
					document = new Document
					{
						OriginalInputPath = recodeRequest.OriginalDocPath,
						CurrentInputPath = recodeRequest.CurrentDocPath,
						ErrorPath = Path.Combine(defaultSettings.DefaultErrorFolder(), Path.GetFileName(recodeRequest.OriginalDocPath))
					};

					//переносим документ в Error директорию
					var fileMoveReport = MoveFilesCausedError();
					emailProxy.CannotFindRecodeProcessFromReceivedDocumentLocation(recodeRequest.OriginalDocPath, fileMoveReport);
					throw new NotificatedException();
				}

				recodeRequest.OriginalXmlPath = GetXmlFileLocation();
				recodeRequest.CurrentXmlPath = recodeRequest.OriginalXmlPath;

				//Парсим xml файл в XmlDocument
				var xml = ParseXmlMessage(recodeRequest);

				/*ИНИЦИАЛИЗИРУЕМ XML и ДОКУМЕНТ*/
				xmlFile = new XmlFile(recodProcess, recodeRequest.OriginalXmlPath, recodeRequest.CurrentXmlPath, xml);
				document = GetDocumentDetailsFromKnownDocumentInputPath(recodeRequest.OriginalDocPath, recodeRequest.CurrentDocPath, emailProxy);
			}
			else
			{
				//Парсим xml файл в XmlDocument
				var xml = ParseXmlMessage(recodeRequest);
				var dirName = Path.GetDirectoryName(recodeRequest.OriginalXmlPath);

				var matchingProcesses = db.RecodingProcesses.ToList().Where(r =>Paths.AreEqual(r.InputFolderName, dirName));

				//Ищем процесс перекодировки, которые соответсвуют Root ноду xml-ки и папки, из которой она было получена 
				recodProcess = GetRecodingProcessMatchingXml(matchingProcesses, xml);

				//Не нашли процесса перекодировки
				if (recodProcess == null)
				{
					//Заполняем данные XML файла. Т.к. процесс перекодировки не найден, перекладываем в default папку для ошибочных файлов
					xmlFile = new XmlFile
					{
						CurrentInputPath = recodeRequest.CurrentXmlPath,
						OriginalInputPath = recodeRequest.OriginalXmlPath,
						ErrorPath = Path.Combine(defaultSettings.DefaultErrorFolder(), Path.GetFileName(recodeRequest.OriginalXmlPath))
					};
					//Переносим Xml файл в Error папку
					var fileMoveReport = MoveFilesCausedError();

					emailProxy.NoRecodingProcessFound(recodeRequest.OriginalXmlPath, xml.DocumentElement.Name, fileMoveReport);
					throw new NotificatedException();
				}
				
				/*ИНИЦИАЛИЗИРУЕМ XML и ДОКУМЕНТ*/
				xmlFile = new XmlFile(recodProcess, recodeRequest.OriginalXmlPath, recodeRequest.CurrentXmlPath, xml);
				document = GetDocumentDetailsFromKnownXmlFileDetails(emailProxy);

				if (recodProcess.MainDocumentIsRequired && (document == null || !File.Exists(document.CurrentInputPath)))
				{
					var docPath = document != null ? document.CurrentInputPath : null; 

					//Обнуляем документ, т.к. его нет
					document = null;
					//Обрабатываем ошибку
					var fileMoveReport = MoveFilesCausedError();
					emailProxy.DocumentNotFound(recodeRequest.OriginalXmlPath, docPath, fileMoveReport);
					throw new NotificatedException();
				}
			}
		}

		/// <summary>
		/// Выбирает из указанного списка процессов перекодировки тот, который подходит указанному xml.
		/// Если соответствия не найдено, возвращает null
		/// </summary>
		private RecodingProcess GetRecodingProcessMatchingXml(IEnumerable<RecodingProcess> processes, XmlDocument xmldoc)
		{
			if (processes.Count() == 1)
				return  processes.First();

			//Сначала проверяем на соответствие процессы перекодировки, у которых указан Xpath в качестве идентификатора
			var processesIdentifiedByXpath = processes.Where(p => !String.IsNullOrEmpty(p.Xpath));
			
			foreach (var xpathProc in processesIdentifiedByXpath)
			{
				if( xmldoc.DocumentElement.SelectNodes(xpathProc.Xpath).Count > 0)
					return xpathProc;
			}

			//Если процессы, идентифицирующиеся по Xpath запросу не подошли, то возвращаем тот, у которого указан root node, совпадающий с тем, что в xml
			var processByRootNode = processes.FirstOrDefault(p => p.RootNodeName == xmldoc.DocumentElement.Name);

			if (processByRootNode != null)
				return processByRootNode;

			return processes.SingleOrDefault(p => (p.Xpath == null) && (p.RootNodeName == null));
		}

		/// <summary>
		/// Получить путь к сопроводительному xml по пути к основному документу
		/// </summary>
		/// <param name="documentFileLocation">Полный путь к основному документу</param>
		/// <returns>Путь к сопроводительному XML</returns>
		public string GetXmlFileLocation()
		{
			return Path.Combine(recodProcess.InputFolderName, Path.GetFileNameWithoutExtension(document.CurrentInputPath)) + ".xml";
		}

		private XmlDocument ParseXmlMessage(RecodeMessageRequest recodeRequest)
		{
			try
			{
				var xml = new XmlDocument();
				xml.Load(recodeRequest.CurrentXmlPath);
				return xml;
			}
			catch
			{
				var fileMoveReport = MoveFilesCausedError();

				emailProxy.CannotParseXml(recodeRequest.OriginalXmlPath, fileMoveReport);
				throw new NotificatedException();
			}
		}

		#region Инициализация Документа
		private Document GetDocumentDetailsFromKnownXmlFileDetails(NotificationServiceProxy emailProxy)
		{
			//Если для этого процесса не подразумевается работы с документом (только с сопроводительным XML) - возвращаем null
			if (String.IsNullOrEmpty(recodProcess.MainDocumentInputFileDirectory) && String.IsNullOrEmpty(recodProcess.MainDocumentInputXPath))
				return null;

			var document = new Document();

			//Определяем название файла документа
			document.FileName = GetDocumentName();

			if (!String.IsNullOrEmpty(recodProcess.MainDocumentErrorOutputFileDirectory))
			{
				document.ErrorPath = Path.Combine(recodProcess.MainDocumentErrorOutputFileDirectory, document.FileName);
			}
			else if (!String.IsNullOrEmpty(recodProcess.MainDocumentErrorOutputXPath))
			{
				var nodes = xmlFile.Xml.SelectNodes(recodProcess.MainDocumentErrorOutputXPath);

				if (nodes.Count != 0)
				{
					document.ErrorPath = nodes[0].InnerText;
				}
				else
				{
					//Ничего по Xpath не найдено - ошибка
					//Перемещаем в папку для ошибочных файлов
					var fileMoveReport = MoveFilesCausedError();

					//Посылаем письмо
					emailProxy.XpathToDocumentNoMathchesFound(xmlFile.OriginalInputPath, recodProcess.MainDocumentInputXPath, fileMoveReport);

					throw new NotificatedException();
				}
			}
			else
				document.ErrorPath = Path.Combine(defaultSettings.DefaultErrorFolder(), Guid.NewGuid().ToString());

			//Определяемся с путем к файлу документа
			if (!String.IsNullOrEmpty(recodProcess.MainDocumentInputFileDirectory))
			{
				document.CurrentInputPath = Path.Combine(recodProcess.MainDocumentInputFileDirectory, document.FileName);
				document.OriginalInputPath = document.CurrentInputPath;
			}
			else if (!String.IsNullOrEmpty(recodProcess.MainDocumentInputXPath))
			{
				var nodes = xmlFile.Xml.SelectNodes(recodProcess.MainDocumentInputXPath);

				if (nodes.Count != 0)
				{
					document.CurrentInputPath = nodes[0].InnerText;
					document.OriginalInputPath = document.CurrentInputPath;
				}
				else
				{
					//Ничего по Xpath не найдено - ошибка
					//Перемещаем в папку для ошибочных файлов
					var fileMoveReport = MoveFilesCausedError();

					//Посылаем письмо
					emailProxy.XpathToDocumentNoMathchesFound(xmlFile.OriginalInputPath, recodProcess.MainDocumentInputXPath, fileMoveReport);

					throw new NotificatedException();
				}
			}

			//Определяемся с путем к папке, куда нужно переложить файл основного документа
			if (!String.IsNullOrEmpty(recodProcess.MainDocumentOutputFileDirectory))
			{
				document.OutputPath = Path.Combine(recodProcess.MainDocumentOutputFileDirectory, document.FileName);
			}
			else if (!String.IsNullOrEmpty(recodProcess.MainDocumentOutputXPath))
			{
				var nodes = xmlFile.Xml.SelectNodes(recodProcess.MainDocumentOutputXPath);

				if (nodes.Count != 0)
				{
					var path = nodes[0].InnerText;

					if (Path.HasExtension(path))
						document.OutputPath = path;
					else
						document.OutputPath = Path.Combine(path, document.FileName);
				}
				else
				{
					//Ничего по Xpath не найдено - ошибка
					//Перемещаем в папку для ошибочных файлов
					var fileMoveReport = MoveFilesCausedError();

					//Посылаем письмо
					emailProxy.XpathToDocumentNoMathchesFound(xmlFile.OriginalInputPath, recodProcess.MainDocumentInputXPath, fileMoveReport);

					throw new NotificatedException();
				}
			}
			
			return document;
		}

		private string GetDocumentName()
		{
			if (recodProcess == null || (String.IsNullOrEmpty (recodProcess.MainDocumentInputFileDirectory) && String.IsNullOrEmpty (recodProcess.MainDocumentInputXPath)) )
				return null;

			if (!String.IsNullOrEmpty (recodProcess.MainDocumentInputFileDirectory))
			{
				var xmlFileName = Path.GetFileNameWithoutExtension(xmlFile.FileName);

				var docDir = new DirectoryInfo(recodProcess.MainDocumentInputFileDirectory);


				if (docDir == null)
				{
					//Перемещаем в папку для ошибочных файлов
					var fileMoveReport = MoveFilesCausedError();

					//Посылаем письмо
					emailProxy.DocumentNotFound(xmlFile.OriginalInputPath, recodProcess.MainDocumentInputFileDirectory, fileMoveReport);

					throw new NotificatedException();
				}
				var docFile = docDir.EnumerateFiles().FirstOrDefault(f => f.Name.Replace(f.Extension, "") == xmlFileName);

				if (docFile == null)
				{
					//Перемещаем в папку для ошибочных файлов
					var fileMoveReport = MoveFilesCausedError();

					//Посылаем письмо
					emailProxy.DocumentNotFound(xmlFile.OriginalInputPath, Path.Combine(recodProcess.MainDocumentInputFileDirectory, xmlFileName), fileMoveReport);

					throw new NotificatedException();
				}
					
				return docFile.Name;
			}

			else if (!String.IsNullOrEmpty(recodProcess.MainDocumentInputXPath) && xmlFile.Xml != null)
			{
				var nodes = xmlFile.Xml.SelectNodes(recodProcess.MainDocumentInputXPath);

				if (nodes != null && nodes.Count != 0)
				{
					var path = nodes[0].InnerText;
					return Path.GetFileName(path);
				}
				else
				{
					//Ничего по Xpath не найдено - ошибка
					//Перемещаем в папку для ошибочных файлов
					var fileMoveReport = MoveFilesCausedError();

					//Посылаем письмо
					emailProxy.XpathToDocumentNoMathchesFound(xmlFile.OriginalInputPath, recodProcess.MainDocumentInputXPath, fileMoveReport);

					throw new NotificatedException();
				}
			}

			return null;
		}

		private Document GetDocumentDetailsFromKnownDocumentInputPath(string originalInputPath, string currentInputPath, NotificationServiceProxy emailProxy)
		{
			var document = new Document();

			//Определяем название файла документа
			document.FileName = Path.GetFileNameWithoutExtension(originalInputPath);

			document.CurrentInputPath = currentInputPath;
			document.OriginalInputPath = originalInputPath;

			if (!String.IsNullOrEmpty(recodProcess.MainDocumentErrorOutputFileDirectory))
			{
				document.ErrorPath = Path.Combine(recodProcess.MainDocumentErrorOutputFileDirectory, document.FileName);
			}
			else if (!String.IsNullOrEmpty(recodProcess.MainDocumentErrorOutputXPath) && xmlFile != null)
			{
				var nodes = xmlFile.Xml.SelectNodes(recodProcess.MainDocumentErrorOutputXPath);

				if (nodes.Count != 0)
				{
					document.ErrorPath = nodes[0].InnerText;
				}
				else
				{
					//Ничего по Xpath не найдено - ошибка
					//Перемещаем в папку для ошибочных файлов
					var fileMoveReport = MoveFilesCausedError();

					//Посылаем письмо
					emailProxy.XpathToDocumentNoMathchesFound(xmlFile.OriginalInputPath, recodProcess.MainDocumentInputXPath, fileMoveReport);

					throw new NotificatedException();
				}
			}
			else
				document.ErrorPath = Path.Combine(defaultSettings.DefaultErrorFolder(), Guid.NewGuid().ToString());

			//Определяемся с путем к папке, куда нужно переложить файл основного документа
			if (!String.IsNullOrEmpty(recodProcess.MainDocumentOutputFileDirectory))
			{
				document.OutputPath = recodProcess.MainDocumentOutputFileDirectory;
			}
			else if (!String.IsNullOrEmpty(recodProcess.MainDocumentOutputXPath) && xmlFile != null)
			{
				var nodes = xmlFile.Xml.SelectNodes(recodProcess.MainDocumentOutputXPath);

				if (nodes.Count != 0)
				{
					document.OutputPath = nodes[0].InnerText;
				}
				else
				{
					//Ничего по Xpath не найдено - ошибка
					//Перемещаем в папку для ошибочных файлов
					var fileMoveReport = MoveFilesCausedError();

					//Посылаем письмо
					emailProxy.XpathToDocumentNoMathchesFound(xmlFile.OriginalInputPath, recodProcess.MainDocumentInputXPath, fileMoveReport);

					throw new NotificatedException();
				}
			}
			else
			{
				//Перемещаем в папку для ошибочных файлов
				var fileMoveReport = MoveFilesCausedError();

				//Посылаем письмо
				emailProxy.DocumentOutputPathNotSpecified(originalInputPath, fileMoveReport);

				throw new NotificatedException();
			}

			return document;
		}

		#endregion

		/// <summary>
		/// Переместить документ и сопроводительный Xml в папку для ошибочных файлов
		/// </summary>
		private string MoveFilesCausedError()
		{
			var fileMoveReport = "";

			fileMoveReport += MoveXmlCausedError();

			if (document != null)
			{
				fileMoveReport += MoveDocCausedError();
			}

			return fileMoveReport;
		}

		private string MoveXmlCausedError()
		{
			if (xmlFile != null && !String.IsNullOrEmpty(xmlFile.CurrentInputPath) && File.Exists(xmlFile.CurrentInputPath))
			{
				if (File.Exists(xmlFile.ErrorPath))
					File.Delete(xmlFile.ErrorPath);

				var dir = Path.GetDirectoryName(xmlFile.ErrorPath);
				Directory.CreateDirectory(dir);

				File.Move(xmlFile.CurrentInputPath, xmlFile.ErrorPath);

				return emailProxy.XmlCausedErrorWasMoved(xmlFile);
			}

			return "";
		}

		private string MoveDocCausedError()
		{
			//Если документ не найден
			if (recodProcess != null && recodProcess.MainDocumentIsRequired && !File.Exists(document.CurrentInputPath))
				return "Обязательный файл документа не найден по адресу: " + document.CurrentInputPath;

			//Если положить в ту же папку из которой взяли, то кладем в подпапку Error
			if (document.OriginalInputPath == document.ErrorPath)
			{
				var docOutDir = Path.Combine(Path.GetDirectoryName(document.ErrorPath), @"Error");

				Directory.CreateDirectory(docOutDir);

				document.ErrorPath = Path.Combine(docOutDir, document.FileName);
			}

			if (!String.IsNullOrEmpty(document.CurrentInputPath) && !String.IsNullOrEmpty(document.ErrorPath) && File.Exists(document.CurrentInputPath))
			{
				var docFileName = Path.GetFileName(document.CurrentInputPath);

				if (File.Exists(document.ErrorPath))
					File.Delete(document.ErrorPath);

				File.Move(document.CurrentInputPath, document.ErrorPath);

				return emailProxy.DocumentCausedErrorWasMoved(document);
			}

			return "";
		}

		private void WriteFilesToOutputFolders()
		{
			if (document != null && !String.IsNullOrEmpty(document.OutputPath))
			{
				if (!File.Exists(document.CurrentInputPath))
				{
					if (!recodProcess.MainDocumentIsRequired)
						return;

					var docOriginalPath = document.OriginalInputPath;
					document = null;

					var fileMoveReport = MoveFilesCausedError();
					emailProxy.DocumentNotFound(xmlFile.OriginalInputPath, docOriginalPath, fileMoveReport);
					throw new NotificatedException();
				}

				//Пишем документ в output папку
				try
				{
					File.Move(document.CurrentInputPath, document.OutputPath);
				}
				catch (DirectoryNotFoundException ex)
				{
					var fileMoveReport = MoveFilesCausedError();
					emailProxy.DocOutputDirectoryNotFound(document.OriginalInputPath, document.OutputPath, fileMoveReport, ex);
					throw new NotificatedException();
				}
				catch (UnauthorizedAccessException ex)
				{
					var fileMoveReport = MoveFilesCausedError();
					emailProxy.DocOutputDirectoryUnauthorizedAccess(document.OriginalInputPath, document.OutputPath, fileMoveReport, ex);
					throw new NotificatedException();
				}
			}

			//Пишем Xml файл в output папку
			try
			{
				var settings = new XmlWriterSettings()
				{
					Indent = true,
					Encoding = xmlFile.Encoding ?? Encoding.UTF8
				};

				using(XmlWriter writer = XmlWriter.Create(xmlFile.OutputPath, settings))
				{
					xmlFile.Xml.Save(writer);

					writer.Close();
				}
								
				//Удаляем копию файла, и папку, в которой она лежит, если там больше ничего нет
				File.Delete(xmlFile.CurrentInputPath);
				var folder = Path.GetDirectoryName(xmlFile.CurrentInputPath);
				var dir = new DirectoryInfo(folder);
				if (dir.EnumerateFiles().Count() == 0)
					dir.Delete();
			}
			catch (DirectoryNotFoundException ex)
			{
				var fileMoveReport = MoveFilesCausedError();
				emailProxy.XmlOutputDirectoryNotFound(xmlFile.OriginalInputPath, xmlFile.OutputPath, fileMoveReport, ex);
				throw new NotificatedException();
			}
			catch (UnauthorizedAccessException ex)
			{
				var fileMoveReport = MoveFilesCausedError();
				emailProxy.XmlOutputDirectoryUnauthorizedAccess(xmlFile.OriginalInputPath, xmlFile.OutputPath, fileMoveReport, ex);
				throw new NotificatedException();
			}
		}
	}
}