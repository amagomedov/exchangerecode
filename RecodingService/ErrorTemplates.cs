﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace RecodingService
{
    public static class RecodeExceptionFactory
    {
        public static void ThrowXpathNoMathchesFoundException(string InputFolderName, string RootNodeName, string Xpath)
        {
            throw new Exception(String.Format(@"Ошибка перекодировки: Для полученного из папки <b>{0}</b> XML файла c корневым тегом <b>{1}</b> " +
                        "было найдено обязательное правило c XPath <b>{2}</b>, однако по нему (XPath-у) не было найдено ни одного тега.",
                        InputFolderName, RootNodeName, Xpath));
        }

        public static void ThrowNoRulesFoundException(string InputFolderName, string RootNodeName)
        {
            throw new Exception(String.Format(@"Ошибка перекодировки: Для полученного из папки <b>{0}</b> XML файла c корневым тегом <b>{1}</b> " +
                            "не было найдено правил перекодировки. Пожалуйста, убедитесь, что в БД RecodeRulesDb существуют правила перекодировки для указанного типа XML.",
                            InputFolderName, RootNodeName));
        }

        public static void ThrowNoValueToRecodeException(string inputFolderName, string rootNodeName, string xpath, string tagName)
        {
            throw new Exception( String.Format(@"Ошибка перекодировки: Для полученного из папки <b>{0}</b> XML файла c корневым тегом <b>{1}</b> " +
                            "было найдено обязательное правило c XPath <b>{2}</b>, однако по нему (XPath-у) был найден тег {3}, в котором не указано значение.", 
                            inputFolderName, rootNodeName, xpath, tagName));
        }

        public static void ThrowMappingException(string entityName, string sourceId, string sourceSystemName, string destinationSystemName)
        {
            throw new Exception(String.Format("Ошибка перекодировки: не удалось преобразовать {0} с кодом {1} системы {2} в код системы {3}", entityName, sourceId, sourceSystemName, destinationSystemName));
        }

        public static void ThrowMDXpathNoMathchesFoundException(string InputFolderName, string RootNodeName, string Xpath)
        {
            throw new Exception(String.Format(@"Ошибка перекодировки: Для полученного из папки <b>{0}</b> XML файла c корневым тегом <b>{1}</b> " +
                        "был задан поиск тега по XPath <b>{2}</b>, однако по нему (XPath-у) не было найдено ни одного тега.",
                        InputFolderName, RootNodeName, Xpath));
        }

        public static void ThrowProcessForMDNotFoundException(string DocumentFolderName, string exceptionMessage)
        {
            throw new Exception(String.Format(@"Для полученного из папки <b>{0}</b> документа не было найдено ни одного правила перекодировки, либо было найдено более одного."+
                " Убедитесь, что в базе данных с правилами перекодировки присутствует правило, для которого указан соответствующий путь к основному документу"+
                " (колонка MainDocumentInputFileDirectory в таблице RecodingProcess должна быть равна <b>{1}</b> для соответствующего процесса перекодировки)."+
                "<br> Подробнее: {2}",
                        DocumentFolderName, Path.GetDirectoryName(DocumentFolderName).TrimEnd('/', '\\'), exceptionMessage));
        }
    }
}