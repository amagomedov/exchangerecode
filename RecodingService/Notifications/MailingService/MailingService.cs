﻿using System.ServiceModel;
using System.Collections.Generic;
using System.Text;
using System;
using System.Linq;

namespace RecodingService
{
    public class MailingService
    {
        ChannelFactory<IMailingService> mailingServiceChannelFactory;
        IMailingService client;

        public MailingService()
        {
            //Get binding from config
            mailingServiceChannelFactory = new ChannelFactory<IMailingService>("*");

            client = mailingServiceChannelFactory.CreateChannel();
        }

        /// <summary>
        /// Отправить сообщение
        /// </summary>
        /// <param name="message">Текст сообщения</param>
        public void SendMessage(string message, string subject, string mailTo)
        {
            try
            {
                ((IServiceChannel) client).Open();

                client.SendMessage(message, subject, mailTo);
            }
            catch
            {
                ((IServiceChannel) client).Abort();
                throw;
            }
            finally
            {
                ((IServiceChannel) client).Close();
            }
        }
    }
}
