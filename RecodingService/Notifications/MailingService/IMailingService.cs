﻿using System.Runtime.Serialization;
using System.ServiceModel;

namespace RecodingService
{
    [ServiceContract(Namespace = "http://navicongroup.ru/mdm")]
    public interface IMailingService
    {
        //[OperationContract]
        //void TriggerMailing(string messageXml, string entityName, MailingTriggerType triggerType);
        
        [OperationContract]
        void SendMessage(string message, string subject, string mailTo);
        
        [OperationContract]
        void CustomMailing(CustomMailingRequest mailingRequest);
    }

    [DataContract(Namespace = "http://navicongroup.ru/mdm")]
    public class CustomMailingRequest
    {
        [DataMember(IsRequired = true, EmitDefaultValue = false)]
        public string TriggerName { get; set; }
        
        [DataMember]
        public string EmailBody { get; set; }

        [DataMember]
        public string EmailSubject { get; set; }
    }
}
