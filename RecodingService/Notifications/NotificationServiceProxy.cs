﻿using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Xml.Serialization;
using System.Text;
using ExchangeRecode;
using ExchangeRecode.DataAccess;
using ExchangeRecode.DataAccess.ViewModels;
using System.Text.RegularExpressions;
using System.Web;

namespace RecodingService
{
	/// <summary>
	/// Прокси сервиса уведомлений. Позволяет формировать и отправлять уведомления, соответствующие требованиям конкретного процесса перекодировки
	/// </summary>
	public class NotificationServiceProxy
	{
		private MailingService mailingService;
		private IEnumerable<EmailTemplate> processSpecificTemplates;
		private IEnumerable<EmailTemplate> defaultTemplates;
		private string processId;
		private string mailTo;
		private string mailSubject;

		public void SetRecodingProcess(RecodingProcess process)
		{
			processId = process.Id.ToString();

			mailTo = process.MailAddress;
			mailSubject = String.IsNullOrEmpty(process.MailSubject) ? null : process.MailSubject;
			processSpecificTemplates = process.EmailTemplates;
		}

		/// <summary>
		/// Прокси сервиса уведомлений. Позволяет формировать и отправлять уведомления, соответствующие требованиям конкретного процесса перекодировки
		/// </summary>
		/// <param name="mailingServiceInstance">Инициализированный сервис MailingService</param>
		/// <param name="emailTemplate">Идентификатор текущего процесса</param>
		public NotificationServiceProxy(ExchangeRecodeDbContext db)
		{
			mailingService = new MailingService();
			defaultTemplates = db.EmailTemplates.Where(t => t.IsDefault);
			
			mailTo = db.Settings.DefaultEmailAddress();
		}

		/// <summary>
		/// Возвращает шаблон соответствующего типа, который указан для текущего процесса, либо по-умолчанию
		/// </summary>
		private EmailTemplate getTemplateOfType(TemplateTypes type)
		{
			var specificTemplate = processSpecificTemplates != null
				? processSpecificTemplates.FirstOrDefault(t => t.TemplateType.Id == (int)type)
				: null;

			return specificTemplate ?? defaultTemplates.FirstOrDefault(t => t.TemplateType.Id == (int)type);
		}

		/// <summary>
		/// Возвращает шаблон указанного типа с подставленными в него значениями
		/// </summary>
		/// <param name="emailTemplate">Шаблон уведомления</param>
		/// <param name="paramNameAndValue">Параметры, которые нужно подставить в шаблон указанного типа уведомления. Какие параметры - в какие шаблоны, знают публичные методы этого класса</param>
		/// <param name="paramsToInjectAsHtml">Список параметров, которые не нужно HTML encodить</param>
		/// <returns></returns>
		private string InjectValuesToTemplate(EmailTemplate emailTemplate, Dictionary<string, string> paramNameAndValue, IEnumerable<string> paramsToInjectAsHtml = null)
		{
			if (emailTemplate == null)
				throw new Exception("Для процесса перекодировки с идентификатором '" + processId + "' не найден шаблон уведомления с типом '" + emailTemplate.TemplateType + "'");

			//Разбираемся с условными блоками @if(paramName) {<html>}
			var templateWithParsedConditions = ParseConditions(emailTemplate.Template, paramNameAndValue);

			var template = new StringBuilder(templateWithParsedConditions);

			//Подставляем значения в шаблон
			//Метку каждого параметра (например, @ErrorFolder) заменяем на значение этого параметра
			foreach (var replacement in paramNameAndValue)
			{
				//Если параметр в списке параметром, которые не нужно Html-encodить, то берем сырое значение, иначе делаем HtmlEncode значения
				var value = paramsToInjectAsHtml.Contains(replacement.Key) ? replacement.Value : HttpUtility.HtmlEncode(replacement.Value);

				template.Replace("@" + replacement.Key, value);
			}

			//Возвращаем готовый текст уведомления
			return template.ToString();
		}

		/// <summary>
		/// Обработать условные выражения (@if(paramName) {<html>}) в шаблона с использованием указанного списка параметров
		/// </summary>
		/// <returns>шаблон уведомления, в котором условные блоки удалены, либо заменены их значениями</returns>
		private string ParseConditions(string template, Dictionary<string, string> paramNameAndValue)
		{
			var processedTemplate = template;
			var conditionRegex = new Regex(@"(?is)@if\s*\(@?(?<condition>\w*)\)\s*\{(?<html>[^}]*)\}");
			var matches = conditionRegex.Matches(template);

			foreach (Match match in matches)
			{
				string paramVal;
				paramNameAndValue.TryGetValue(match.Groups["condition"].Value, out paramVal);
				
				//Если такой параметр не передан, либо он пустой, то убираем из шаблона весь @if блок
				if (String.IsNullOrEmpty(paramVal))
				{
					processedTemplate = processedTemplate.Replace(match.Value, "");
				}
				//Если параметр передан, то весь @if блок заменяем на его содержимое (тело)
				else
				{
					processedTemplate = processedTemplate.Replace(match.Value, match.Groups["html"].Value);
				}
			}

			//Возвращаем шаблон уведомления, в котором условные блоки удалены, либо заменены их значениями
			return processedTemplate;
		}

		#region public

		/// <summary>
		/// Уведомление об успешной перекодировке
		/// </summary>
		/// <param name="fullPathToReceivedXmlFile">Полный путь к сопроводительному XML</param>
		/// <param name="xmlFileOutputLocation">Путь к папке, в которую был перемещен сопроводительный XML</param>
		/// <param name="fullPathToDocument">Полный путь к файлу документ</param>
		/// <param name="documentOutputLocation">Путь к папке, в которую был перемещен файл документа</param>
		/// <returns></returns>
		public void Success(string fullPathToReceivedXmlFile, string xmlFileOutputLocation, string fullPathToDocument, string documentOutputLocation)
		{
			var xmlFileName = System.IO.Path.GetFileName(fullPathToReceivedXmlFile);
			var xmlSourceDirectory = Path.GetDirectoryName(fullPathToReceivedXmlFile);

			var ix = xmlFileName.IndexOf('_');
			ix = ix > 0 ? ix : 0;

			var kkdDocNumber = xmlFileName.Substring(0, ix);

			//Получаем шаблон соответствующего типа
			var template = getTemplateOfType(TemplateTypes.Success);

			var parameterList = new Dictionary<string, string> {
				{ "xmlFileName", xmlFileName},
				{ "xmlSourceDirectory", xmlSourceDirectory },
				{ "xmlFileOutputLocation", Path.GetDirectoryName( xmlFileOutputLocation) },
				{ "kkdDocNumber", kkdDocNumber }
			};

			//Если понятие "документ" вообще существует для данного процесса
			if (!String.IsNullOrEmpty(documentOutputLocation) && !String.IsNullOrEmpty(fullPathToDocument))
			{
				var documentFileName = System.IO.Path.GetFileName(fullPathToDocument);
				var documentSourceDirectory = Path.GetDirectoryName(fullPathToDocument);

				parameterList.Add("documentFileName", documentFileName);
				parameterList.Add("documentSourceDirectory", documentSourceDirectory);
				parameterList.Add("documentOutputLocation", Path.GetDirectoryName(documentOutputLocation));
			}

			var notificationMessage = InjectValuesToTemplate(template, parameterList, new List<string> { "fileMoveReport" });
			mailingService.SendMessage(notificationMessage, template.EmailSubject, mailTo);
		}

		/// <summary>
		/// Уведомление о том, что для полученного из папки XML файла c корневым тегом RootNodeName 
		/// было найдено обязательное правило c XPath, однако по нему (XPath-у) не было найдено ни одного тега
		/// </summary>
		/// <param name="fullPathToReceivedXmlFile">Полный путь к сопроводительному XML</param>
		/// <param name="fullPathToDocument">Полный путь к файлу документ</param>
		/// <param name="RootNodeName">Имя корневого элемента XML</param>
		/// <param name="Xpath">Xpath к тегу, который нужно перекодировать</param>
		/// <returns></returns>
		public void XpathNoMathchesFound(string fullPathToReceivedXmlFile, string xpath, string fileMoveReport)
		{
			var xmlFileName = System.IO.Path.GetFileName(fullPathToReceivedXmlFile);
			var xmlSourceDirectory = Path.GetDirectoryName(fullPathToReceivedXmlFile);

			//Получаем шаблон соответствующего типа
			var template = getTemplateOfType(TemplateTypes.XpathNoMathches);
			var parameterList = new Dictionary<string, string> 
			{
				{ "xmlFileName", xmlFileName},
				{ "xmlSourceDirectory", xmlSourceDirectory },
				{ "fileMoveReport", fileMoveReport },
				{ "xpath", xpath }
			};

			var notificationMessage = InjectValuesToTemplate(template, parameterList, new List<string> { "fileMoveReport" });
			mailingService.SendMessage(notificationMessage, mailSubject ?? template.EmailSubject, mailTo);
		}

		/// <summary>
		/// Уведомление о том, что для полученного из папки fullPathToReceivedXmlFile XML файла 
		/// c корневым тегом RootNodeName не было найдено правил перекодировки
		/// </summary>
		/// <param name="fullPathToReceivedXmlFile">Полный путь к сопроводительному XML</param>
		/// <param name="fullPathToDocument">Полный путь к файлу документ</param>
		/// <param name="RootNodeName">Имя корневого элемента XML</param>
		/// <returns></returns>
		public void NoRecodingProcessFound(string fullPathToReceivedXmlFile, string rootNodeName, string fileMoveReport)
		{
			var xmlFileName = System.IO.Path.GetFileName(fullPathToReceivedXmlFile);
			var xmlSourceDirectory = Path.GetDirectoryName(fullPathToReceivedXmlFile);

			//Получаем шаблон соответствующего типа
			var template = getTemplateOfType(TemplateTypes.NoRecodingProcessFound);
			var parameterList = new Dictionary<string, string> 
			{
				{ "xmlFileName", xmlFileName},
				{ "xmlSourceDirectory", xmlSourceDirectory },
				{ "fileMoveReport", fileMoveReport },
				{ "rootNodeName", rootNodeName }
			};

			var notificationMessage = InjectValuesToTemplate(template, parameterList, new List<string> { "fileMoveReport" });
			mailingService.SendMessage(notificationMessage, mailSubject ?? template.EmailSubject, mailTo);
		}

		public void NoValueToRecode(string fullPathToReceivedXmlFile, string Xpath, string tagName, string fileMoveReport)
		{
			var xmlFileName = System.IO.Path.GetFileName(fullPathToReceivedXmlFile);
			var xmlSourceDirectory = Path.GetDirectoryName(fullPathToReceivedXmlFile);

			//Получаем шаблон соответствующего типа
			var template = getTemplateOfType(TemplateTypes.NoValueToRecode);
			var parameterList = new Dictionary<string, string> 
			{
				{ "xmlFileName", xmlFileName},
				{ "xmlSourceDirectory", xmlSourceDirectory },
				{ "xpath", Xpath },
				{ "fileMoveReport", fileMoveReport },
				{ "tagName", tagName}
			};

			var notificationMessage = InjectValuesToTemplate(template, parameterList, new List<string> { "fileMoveReport" });
			mailingService.SendMessage(notificationMessage, mailSubject ?? template.EmailSubject, mailTo);
		}

		public void NoMappingsFound(string fullPathToReceivedXmlFile, string Xpath, string tagName,
									string entityName, string sourceId, string sourceSystemName, string destinationSystemName, string fileMoveReport, string elementName = null)
		{
			var xmlFileName = System.IO.Path.GetFileName(fullPathToReceivedXmlFile);
			var xmlSourceDirectory = Path.GetDirectoryName(fullPathToReceivedXmlFile);

			//Получаем шаблон соответствующего типа
			var template = getTemplateOfType(TemplateTypes.MappingException);
			var parameterList = new Dictionary<string, string> 
			{
				{ "xmlFileName", xmlFileName},
				{ "xmlSourceDirectory", xmlSourceDirectory },
				{ "xpath", Xpath },
				{ "tagName", tagName},
				{ "entityName", entityName },
				{ "sourceId", sourceId },
				{ "sourceSystemName", sourceSystemName },
				{ "destinationSystemName", destinationSystemName },
				{ "fileMoveReport", fileMoveReport }
			};

			if (!String.IsNullOrEmpty(elementName))
			{
				parameterList.Add("Name", elementName);
			}

			var notificationMessage = InjectValuesToTemplate(template, parameterList, new List<string> { "fileMoveReport" });
			mailingService.SendMessage(notificationMessage, mailSubject ?? template.EmailSubject, mailTo);
		}

		public void CodesMappingError(string fullPathToReceivedXmlFile, string fullPathToDocument, string Xpath, string tagName,
									  string entityName, string sourceId, string sourceSystemName, string destinationSystemName,
									  Exception exception, string fileMoveReport)
		{
			var xmlFileName = System.IO.Path.GetFileName(fullPathToReceivedXmlFile);
			var xmlSourceDirectory = Path.GetDirectoryName(fullPathToReceivedXmlFile);
			var documentFileName = System.IO.Path.GetFileName(fullPathToDocument);
			var documentSourceDirectory = Path.GetDirectoryName(fullPathToDocument);

			//Получаем шаблон соответствующего типа
			var template = getTemplateOfType(TemplateTypes.MappingException);
			var parameterList = new Dictionary<string, string> 
			{
				{ "xmlFileName", xmlFileName},
				{ "xmlSourceDirectory", xmlSourceDirectory },
				{ "documentFileName", documentFileName },
				{ "documentSourceDirectory", documentSourceDirectory },
				{ "xpath", Xpath },
				{ "tagName", tagName},
				{ "entityName", entityName },
				{ "sourceId", sourceId },
				{ "sourceSystemName", sourceSystemName },
				{ "destinationSystemName", destinationSystemName },
				{ "exception", exception.ToString() },
				{ "fileMoveReport", fileMoveReport }
			};

			var notificationMessage = InjectValuesToTemplate(template, parameterList, new List<string> { "fileMoveReport" });
			mailingService.SendMessage(notificationMessage, mailSubject ?? template.EmailSubject, mailTo);
		}

		public void XpathToDocumentNoMathchesFound(string fullPathToReceivedXmlFile, string xpath, string fileMoveReport)
		{
			var xmlFileName = System.IO.Path.GetFileName(fullPathToReceivedXmlFile);
			var xmlSourceDirectory = Path.GetDirectoryName(fullPathToReceivedXmlFile);

			//Получаем шаблон соответствующего типа
			var template = getTemplateOfType(TemplateTypes.MDXpathNoMathchesFound);
			var parameterList = new Dictionary<string, string> 
			{
				{ "xmlFileName", xmlFileName},
				{ "xmlSourceDirectory", xmlSourceDirectory },
				{ "xpath", xpath },
				{ "fileMoveReport", fileMoveReport }
			};

			var notificationMessage = InjectValuesToTemplate(template, parameterList, new List<string> { "fileMoveReport" });
			mailingService.SendMessage(notificationMessage, mailSubject ?? template.EmailSubject, mailTo);
		}

		public void DocumentOutputPathNotSpecified(string fullPathToDocument, string fileMoveReport)
		{
			var documentFileName = System.IO.Path.GetFileName(fullPathToDocument);
			var documentSourceDirectory = Path.GetDirectoryName(fullPathToDocument);

			//Получаем шаблон соответствующего типа
			var template = getTemplateOfType(TemplateTypes.DocumentOutputPathNotSpecified);
			var parameterList = new Dictionary<string, string> 
			{
				{ "documentFileName", documentFileName},
				{ "documentSourceDirectory", documentSourceDirectory },
				{ "fileMoveReport", fileMoveReport }
			};

			var notificationMessage = InjectValuesToTemplate(template, parameterList, new List<string> { "fileMoveReport" });
			mailingService.SendMessage(notificationMessage, mailSubject ?? template.EmailSubject, mailTo);
		}

		public void CannotFindRecodeProcessFromReceivedDocumentLocation(string fullPathToDocument, string fileMoveReport)
		{
			var documentFileName = System.IO.Path.GetFileName(fullPathToDocument);
			var documentSourceDirectory = Path.GetDirectoryName(fullPathToDocument);

			//Получаем шаблон соответствующего типа
			var template = getTemplateOfType(TemplateTypes.CannotFindRecodeProcessForReceivedDocument);
			var parameterList = new Dictionary<string, string> 
			{
				{ "documentFileName", documentFileName},
				{ "documentSourceDirectory", documentSourceDirectory },
				{ "fileMoveReport", fileMoveReport }
			};

			var notificationMessage = InjectValuesToTemplate(template, parameterList, new List<string> { "fileMoveReport" });
			mailingService.SendMessage(notificationMessage, mailSubject ?? template.EmailSubject, mailTo);
		}

		public void XmlOutputDirectoryNotFound(string fullPathToReceivedXmlFile, string xmlOutputPath, string fileMoveReport, Exception exception)
		{
			var xmlFileName = System.IO.Path.GetFileName(fullPathToReceivedXmlFile);
			var xmlSourceDirectory = Path.GetDirectoryName(fullPathToReceivedXmlFile);

			//Получаем шаблон соответствующего типа
			var template = getTemplateOfType(TemplateTypes.XmlOutputDirectoryNotFound);
			var parameterList = new Dictionary<string, string> 
			{
				{ "xmlFileName", xmlFileName},
				{ "xmlSourceDirectory", xmlSourceDirectory },
				{ "fileMoveReport", fileMoveReport },
				{ "exception", exception.ToString() },
				{ "xmlOutputPath", xmlOutputPath }
			};

			var notificationMessage = InjectValuesToTemplate(template, parameterList, new List<string> { "fileMoveReport" });
			mailingService.SendMessage(notificationMessage, mailSubject ?? template.EmailSubject, mailTo);
		}

		public void DocOutputDirectoryNotFound(string fullPathToDocument, string docOutputPath, string fileMoveReport, Exception exception)
		{
			var documentFileName = System.IO.Path.GetFileName(fullPathToDocument);
			var documentSourceDirectory = Path.GetDirectoryName(fullPathToDocument);

			//Получаем шаблон соответствующего типа
			var template = getTemplateOfType(TemplateTypes.DocOutputDirectoryNotFound);
			var parameterList = new Dictionary<string, string> 
			{
				{ "documentFileName", documentFileName},
				{ "documentSourceDirectory", documentSourceDirectory },
				{ "fileMoveReport", fileMoveReport },
				{ "docOutputPath", docOutputPath},
				{ "exception", exception.ToString() }
			};

			var notificationMessage = InjectValuesToTemplate(template, parameterList, new List<string> { "fileMoveReport" });
			mailingService.SendMessage(notificationMessage, mailSubject ?? template.EmailSubject, mailTo);
		}

		public void XmlOutputDirectoryUnauthorizedAccess(string fullPathToReceivedXmlFile, string xmlOutputPath, string fileMoveReport, Exception exception)
		{
			var xmlFileName = System.IO.Path.GetFileName(fullPathToReceivedXmlFile);
			var xmlSourceDirectory = Path.GetDirectoryName(fullPathToReceivedXmlFile);

			//Получаем шаблон соответствующего типа
			var template = getTemplateOfType(TemplateTypes.XmlOutputDirectoryUnauthorizedAccess);
			var parameterList = new Dictionary<string, string> 
			{
				{ "xmlFileName", xmlFileName},
				{ "xmlSourceDirectory", xmlSourceDirectory },
				{ "fileMoveReport", fileMoveReport },
				{ "exception", exception.ToString() },
				{ "xmlOutputPath", xmlOutputPath }
			};

			var notificationMessage = InjectValuesToTemplate(template, parameterList, new List<string> { "fileMoveReport" });
			mailingService.SendMessage(notificationMessage, mailSubject ?? template.EmailSubject, mailTo);
		}

		public void DocOutputDirectoryUnauthorizedAccess(string fullPathToDocument, string docOutputPath, string fileMoveReport, Exception exception)
		{
			var documentFileName = System.IO.Path.GetFileName(fullPathToDocument);
			var documentSourceDirectory = Path.GetDirectoryName(fullPathToDocument);

			//Получаем шаблон соответствующего типа
			var template = getTemplateOfType(TemplateTypes.DocOutputDirectoryUnauthorizedAccess);
			var parameterList = new Dictionary<string, string> 
			{
				{ "documentFileName", documentFileName},
				{ "documentSourceDirectory", documentSourceDirectory },
				{ "fileMoveReport", fileMoveReport },
				{ "exception", exception.ToString() },
				{ "docOutputPath", docOutputPath}
			};

			var notificationMessage = InjectValuesToTemplate(template, parameterList, new List<string> { "fileMoveReport" });
			mailingService.SendMessage(notificationMessage, mailSubject ?? template.EmailSubject, mailTo);
		}

		public void CannotParseXml(string fullPathToReceivedXmlFile, string fileMoveReport)
		{
			var xmlFileName = System.IO.Path.GetFileName(fullPathToReceivedXmlFile);
			var xmlSourceDirectory = Path.GetDirectoryName(fullPathToReceivedXmlFile);

			//Получаем шаблон соответствующего типа
			var template = getTemplateOfType(TemplateTypes.CannotParseXml);
			var parameterList = new Dictionary<string, string> 
			{
				{ "xmlFileName", xmlFileName},
				{ "xmlSourceDirectory", xmlSourceDirectory },
				{ "fileMoveReport", fileMoveReport }
			};

			var notificationMessage = InjectValuesToTemplate(template, parameterList, new List<string> { "fileMoveReport" });
			mailingService.SendMessage(notificationMessage, mailSubject ?? template.EmailSubject, mailTo);
		}

		/// <summary>
		/// Возвращает текст уведомления о том, что файл сопроводительного Xml был перемещен
		/// </summary>
		public string XmlCausedErrorWasMoved(XmlFile xmlFile)
		{
			if (xmlFile == null || String.IsNullOrEmpty(xmlFile.OriginalInputPath))
				return null;
			else
			{
				var xmlFileName = System.IO.Path.GetFileName(xmlFile.OriginalInputPath);
				var xmlSourceDirectory = Path.GetDirectoryName(xmlFile.OriginalInputPath);

				//Получаем шаблон соответствующего типа
				var template = getTemplateOfType(TemplateTypes.XmlFileWasMoved);
				var parameterList = new Dictionary<string, string> 
				{
					{ "xmlFileName", xmlFileName},
					{ "xmlSourceDirectory", xmlSourceDirectory },
					{ "xmlErrorPath", Path.GetDirectoryName(xmlFile.ErrorPath) }
				};

				return InjectValuesToTemplate(template, parameterList, new List<string> { "fileMoveReport" });
			}
		}

		/// <summary>
		/// Возвращает текст уведомления о том, что файл документа перемещен
		/// </summary>
		public string DocumentCausedErrorWasMoved(Document doc)
		{
			if (doc == null || String.IsNullOrEmpty(doc.OriginalInputPath))
				return null;
			else
			{
				var documentFileName = System.IO.Path.GetFileName(doc.OriginalInputPath);
				var documentSourceDirectory = Path.GetDirectoryName(doc.OriginalInputPath);

				//Получаем шаблон соответствующего типа
				var template = getTemplateOfType(TemplateTypes.DocFileWasMoved);
				var parameterList = new Dictionary<string, string> 
				{
					{ "documentFileName", documentFileName},
					{ "documentSourceDirectory", documentSourceDirectory },
					{ "docErrorPath", Path.GetDirectoryName(doc.ErrorPath) }
				};

				return InjectValuesToTemplate(template, parameterList, new List<string> { "fileMoveReport" });
			}
		}

		public void DocumentNotFound(string fullPathToReceivedXmlFile, string documentPath, string fileMoveReport)
		{
			var xmlFileName = System.IO.Path.GetFileName(fullPathToReceivedXmlFile);
			var xmlSourceDirectory = Path.GetDirectoryName(fullPathToReceivedXmlFile);

			//Получаем шаблон соответствующего типа
			var template = getTemplateOfType(TemplateTypes.DocumentNotFound);
			var parameterList = new Dictionary<string, string> 
			{
				{ "xmlFileName", xmlFileName },
				{ "xmlSourceDirectory", xmlSourceDirectory },
				{ "fileMoveReport", fileMoveReport }
			};

			if (!String.IsNullOrEmpty(documentPath))
			{
				var documentFileName = System.IO.Path.GetFileName(documentPath);
				var documentSourceDirectory = Path.GetDirectoryName(documentPath);

				parameterList.Add("documentFileName", documentFileName);
				parameterList.Add("documentSourceDirectory", documentSourceDirectory);
			}

			var notificationMessage = InjectValuesToTemplate(template, parameterList, new List<string> { "fileMoveReport" });
			mailingService.SendMessage(notificationMessage, mailSubject ?? template.EmailSubject, mailTo);
		}

		public void GeneralError(string fullPathToReceivedXmlFile, string documentPath, string fileMoveReport, Exception exception)
		{
			var xmlFileName = System.IO.Path.GetFileName(fullPathToReceivedXmlFile);
			var xmlSourceDirectory = Path.GetDirectoryName(fullPathToReceivedXmlFile);

			//Получаем шаблон соответствующего типа
			var template = getTemplateOfType(TemplateTypes.GeneralException);
			var parameterList = new Dictionary<string, string> 
			{
				{ "xmlFileName", xmlFileName },
				{ "xmlSourceDirectory", xmlSourceDirectory },
				{ "fileMoveReport", fileMoveReport },
				{ "exception", exception.ToString() }
			};

			if (!String.IsNullOrEmpty(documentPath))
			{
				var documentFileName = System.IO.Path.GetFileName(documentPath);
				var documentSourceDirectory = Path.GetDirectoryName(documentPath);

				parameterList.Add( "documentFileName", documentFileName);
				parameterList.Add( "documentSourceDirectory", documentSourceDirectory);
			}

			var notificationMessage = InjectValuesToTemplate(template, parameterList, new List<string> { "fileMoveReport" });
			mailingService.SendMessage(notificationMessage, mailSubject ?? template.EmailSubject, mailTo);
		}

		#endregion
	}
}