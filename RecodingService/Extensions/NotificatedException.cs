﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RecodingService.Extensions
{
	/// <summary>
	/// Exception для завершения вызова сервис. (GOTO -костыль для завершения вызова без отправки письма)
	/// </summary>
	public class NotificatedException : Exception
	{
	}
}