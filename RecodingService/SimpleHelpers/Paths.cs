﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RecodingService.SimpleHelpers
{
	public static class Paths
	{
		public static bool AreEqual(string pathA, string pathB)
		{
			return String.Compare(pathA.TrimEnd('/', '\\'), pathB.TrimEnd('/', '\\'), StringComparison.OrdinalIgnoreCase) == 0;
		}
	}
}