﻿using System.ServiceModel;
using System.Collections.Generic;
using System.Text;
using System;
using System.Linq;

namespace RecodingService
{
	public class CodesMappingService
	{
		/// <summary>
		/// Отправить сообщение
		/// </summary>
		/// <param name="message">Текст сообщения</param>
		public CodesMappingContract GetMapping(string entityName, string sourceName, string sourceCode, string destName)
		{
			//Get binding from config
			ChannelFactory<ICodesMappingService> myChannelFactory = new ChannelFactory<ICodesMappingService>("*");

			ICodesMappingService client = myChannelFactory.CreateChannel();

			try
			{
				((IServiceChannel) client).Open();

				return client.GetMapping(entityName, sourceName, sourceCode, destName).FirstOrDefault();
			}
			catch
			{
				((IServiceChannel) client).Abort();
				throw;
			}
			finally
			{
				((IServiceChannel) client).Close();
			}
		}
	}
}
