﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExchangeRecode.Helpers
{
    public class RecodeException : Exception
    {
        public string ErrorMailAddress { get; set; }
        public string ErrorMailSubject { get; set; }
        public string ErrorFolderName { get; set; }
        public string FallbackErrorFolderName { get; set; }

        public RecodeException(string errorMailAddress, string errorMailSubject, string errorFolderName, string fallbackErrorFolderName, string errorMessage) : base(errorMessage)
        {
            ErrorMailAddress = errorMailAddress;
            ErrorMailSubject = errorMailSubject;
            ErrorFolderName = errorFolderName;
            FallbackErrorFolderName = fallbackErrorFolderName;
        }
    }
}
