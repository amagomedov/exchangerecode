﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;

namespace ExchangeRecode
{
	public static class Helpers
	{
		/*
		public static string WriteToErrorOrDefaultFolder(string text, string errorPath, string defaultFolder, string fileTypeName = "сопроводительный XML")
		{
			try
			{
				Directory.CreateDirectory(Path.GetDirectoryName(errorPath));
				File.WriteAllText(errorPath, text);
				return String.Format("<div style='color:darkgreen'>Вызвавший ошибку {1} был помещен в папку {0}</div>", errorPath, fileTypeName);
			}
			catch (Exception e)
			{
				try
				{
					var defaultFolderFullPath = Path.Combine(defaultFolder, Path.GetFileName(errorPath));

					Directory.CreateDirectory(defaultFolder);
					File.WriteAllText(defaultFolderFullPath, text);
					return String.Format(@"<div style='color:darkred'>При попытке переместить вызвавший ошибку {3} в папку '{0}' произошла ошибка '{2}'.</div>" +
						@"<div style='color:darkgreen'>Поэтому файл был перемещен в папку '{1}'.</div>", errorPath, defaultFolderFullPath, e.Message, fileTypeName);
				}
				catch (Exception ex)
				{
					var lastFallbackPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,"ErrorXmls");
					Directory.CreateDirectory(lastFallbackPath);
					lastFallbackPath = Path.Combine(lastFallbackPath, Path.GetFileName(errorPath));

					File.WriteAllText(lastFallbackPath, text);
					return String.Format(@"<div style='color:darkred'>При попытке переместить вызвавший ошибку {5} в папку '{0}' произошла ошибка '{2}'.<br>" +
						@"Переместить его в папку по-умолчанию ({1}) также не удалось. Произошла ошибка: {3} </div>" +
						"<div style='color:darkgreen'>В итоге, {5} был перемещен в папку {4}</div>", errorPath, defaultFolder, e.Message, ex.Message, lastFallbackPath, fileTypeName);
				}
			}
		}

		public static XmlDocument ReadAndDeleteFile(string path)
		{
			if (!File.Exists(path))
				throw new Exception(String.Format("Сопроводительный XML не был найден по адресу {0}", path));

			//Переименовываем. Имитирую то, как читает файлы Biztalk
			var filePath = path.Replace(".xml", ".BTS-WIP");

			File.Move(path, filePath);

			var xml = new XmlDocument();
			xml.Load(filePath);

			File.Delete(filePath);

			return xml;
		}

		public static void WriteDocumentToOutputFolder(string documentInputPath, string outputPath, string content)
		{
			try
			{
				WriteDocumentToFolder(documentInputPath, outputPath, content);
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("<div style='color:darkred'>Не удалось переместить файл документа из папки '{0}' в папку '{1}'<br> Подробности: {2} </div>", documentInputPath, outputPath, ex.ToString()));
			}
		}

		public static string WriteDocumentToErrorFolder(string documentInputPath, string errorFolderPath, string fallbackErrorFolderPath, string content)
		{

			var errPath = Path.Combine(Path.GetDirectoryName(errorFolderPath), Path.GetFileName(documentInputPath));
			var fallbackPath = Path.Combine(Path.GetDirectoryName(fallbackErrorFolderPath), Path.GetFileName(documentInputPath));

			return WriteToErrorOrDefaultFolder(content, errPath, fallbackPath, "документ");
		}

		private static void WriteDocumentToFolder(string documentInputPath, string outputPath, string content)
		{
			var outFullPath = Path.Combine(Path.GetDirectoryName(outputPath), Path.GetFileName(documentInputPath));

			File.WriteAllText(outFullPath, content);
		}

		public static string MoveFiles(string inputPath, string outputPath, string originalFolderName )
		{
			if (Path.HasExtension(inputPath))
			{
				return MoveFile(inputPath, outputPath, originalFolderName);
			}
			else
			{
				var fileMoveReport = "";
				try
				{
					var dir = Path.GetDirectoryName(inputPath);
					var fileSearchPattern = Path.GetFileName(inputPath + ".*");
					
					var dirInfo = new DirectoryInfo(dir);
					FileInfo[] listfiles = dirInfo.GetFiles(fileSearchPattern);

					if (listfiles.Length > 0)
					{
						foreach (var file in listfiles)
						{
							fileMoveReport += MoveFile(file.FullName, outputPath, originalFolderName);
						}
					}
					else
					{
						fileMoveReport += String.Format("<div style='color:darkred'>Файл документа не был найден в папке '{0}' </div>", originalFolderName ?? inputPath);
					}
				}
				catch (Exception ex)
				{
					fileMoveReport += String.Format("<div style='color:darkred'>Не удалось переместить файл документа из папки '{0}' в папку '{1}'<br> Подробности: {2} </div>", originalFolderName ?? inputPath, outputPath, ex.ToString());
				}
				
				return fileMoveReport;
			}
		}

		private static string MoveFile(string inputPath, string outputPath, string originalFolderName )
		{
			try
			{
				outputPath = Path.HasExtension(outputPath) ? outputPath : Path.Combine(outputPath, Path.GetFileName(inputPath));

				if (File.Exists(outputPath + ".tmp"))
					File.Delete(outputPath + ".tmp");
				//Copy to tmp file
				File.Move(inputPath, outputPath + ".tmp");
				//Remove duplicate
				if (File.Exists(outputPath))
					File.Delete(outputPath);
				//rename to end file name
				File.Move(outputPath + ".tmp", outputPath);

				return String.Format("<div style='color:darkgreen'>Также, файл '{0}' был перемещен в папку '{1}'</div>", originalFolderName ?? inputPath, outputPath);
			}
			catch (Exception e)
			{
				return String.Format("<div style='color:darkred'>При попытке переместить файл документа '{0}' в папку '{1}' произошла ошибка: <br> {2}</div>", originalFolderName ?? inputPath, outputPath, e.ToString());
			}
		}
		 */

		/// <summary>
		/// Создает временную папку на одном из доступных дисков
		/// </summary>
		/// <returns></returns>
		public static string GetBufferDir()
		{
			var di = DriveInfo.GetDrives();

			var drive = di.First(dr => dr.IsReady && dr.RootDirectory.Exists).RootDirectory.Name;

			var path = Path.Combine(drive, "ExchangeRecodeBuffer");

			Directory.CreateDirectory(path);

			return path;
		}
	}
}
