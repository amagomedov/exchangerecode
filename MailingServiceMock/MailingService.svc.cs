﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace MailingServiceMock
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class MailingService : IMailingService
    {

        public void SendMessage(string message, string subject, string mailTo)
        {
            System.IO.File.WriteAllText(@"C:\\log.txt", "subject:" + subject + "\n" + "mailTo:" + mailTo + "\n" +"message: " + message );
        }

        public void CustomMailing(CustomMailingRequest mailingRequest)
        {
            System.IO.File.WriteAllText(@"C:\\log.txt", "EmailSubject: " + mailingRequest.EmailSubject);
            System.IO.File.WriteAllText(@"C:\\log.txt", "TriggerName: " + mailingRequest.TriggerName);
            System.IO.File.WriteAllText(@"C:\\log.txt", "EmailBody: " + mailingRequest.EmailBody);
        }
    }
}
