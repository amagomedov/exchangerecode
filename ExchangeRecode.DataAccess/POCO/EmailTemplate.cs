﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ExchangeRecode.DataAccess
{
    /// <summary>
    /// Шаблон письма уведомления
    /// </summary>
    public class EmailTemplate
    {
        [Key]
        public Guid Id { get; set; }
        public virtual TemplateType TemplateType { get; set; }
        public string Template { get; set; }
        public string EmailSubject { get; set; }
        public bool IsDefault { get; set; }

        public virtual ICollection<RecodingProcess> RecodingProcesses { get; set; }
    }
}