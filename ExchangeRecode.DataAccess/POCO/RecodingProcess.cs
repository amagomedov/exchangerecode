﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ExchangeRecode.DataAccess
{
	/// <summary>
	/// Процесс перекодировки. Соответствует определенного вида и полученному из определенной папки.
	/// Для каждого процесса есть список ReplacementRule (список перекодировок которые нужно провести для XML такого типа) 
	/// </summary>
	public class RecodingProcess
	{
		[Key]
		public Guid Id { get; set; }

		/// <summary>
		/// Директория, из которой был получен сопроводительный XML
		/// </summary>
		public string InputFolderName { get; set; }

		/// <summary>
		/// Корневой тег сопроводительного XML
		/// </summary>
		public string RootNodeName { get; set; }

		/// <summary>
		/// Xpath выражение, которое может быть использовано вместо RootNodeName для идентификации процесса. Для соответствующих XML файлов, Xpath должен возвращать непустой результат
		/// </summary>
		public string Xpath { get; set; }

		/// <summary>
		/// Директория, в которую необходимо перенести перекодированный сопроводительный XML
		/// </summary>
		public string OutputFolderName { get; set; }

		/// <summary>
		/// Директория, в которую необходимо перенести сопроводительный XML, если в процессе перекодировки возникнет ошибка
		/// </summary>
		public string ErrorFolder { get; set; }
		
		/// <summary>
		/// Email адрес для отправки уведомлений
		/// </summary>
		public string MailAddress { get; set; }

		/// <summary>
		/// Тема для email уведомлений. Переопределяет тему, указанную в EmailTemplate
		/// </summary>
		public string MailSubject { get; set; }

		/// <summary>
		/// Путь к папке, в которой находится основной документ (С таким же именем, что и сопроводительный XML файл)
		/// </summary>
		public string MainDocumentInputFileDirectory { get; set; }

		/// <summary>
		/// Документ обязан существовать
		/// </summary>
		public bool MainDocumentIsRequired { get; set; }

		/// <summary>
		/// XPath, который определяет в сопроводительном XML тег, в котором указан путь к основному документу. 
		/// Если указан путь к папке, то будет выполнен поиск документа с таким же именем, что и у сопроводительного XML файла
		/// Будет использовано, если статический путь MainDocumentInputFileDirectory НЕ УКАЗАН
		/// </summary>
		public string MainDocumentInputXPath { get; set; }

		/// <summary>
		/// Путь к папке, в которую будет перемещен основной документ после перекодировки сопроводительного XML файла
		/// </summary>
		public string MainDocumentOutputFileDirectory { get; set; }

		/// <summary>
		/// XPath, который определяет в сопроводительном XML тег, в котором указан путь к папке, в которую будет перемещен основной документ после перекодировки сопроводительного XML файла
		/// Будет использовано, если путь MainDocumentOutputFilePath НЕ УКАЗАН
		/// </summary>
		public string MainDocumentOutputXPath { get; set; }

		/// <summary>
		/// Путь к папке, в которую будет перемещен основной документ при возникновении ошибки в процессе перекодировки сопроводительного XML файла
		/// </summary>
		public string MainDocumentErrorOutputFileDirectory { get; set; }

		/// <summary>
		/// XPath, который определяет в сопроводительном XML тег, в котором указан путь к папке, 
		/// в которую будет перемещен основной документ при возникновении ошибки перекодировки сопроводительного XML файла
		/// Будет использовано, если путь MainDocumentErrorOutputFileDirectory НЕ УКАЗАН
		/// </summary>
		public string MainDocumentErrorOutputXPath { get; set; }

		/// <summary>
		/// Отправлять письмо об успешной перекодировке
		/// </summary>
		public bool MailOnSuccess { get; set; }

		public virtual ICollection<RecodingRule> RecodingRules { get; set; }
		public virtual ICollection<EmailTemplate> EmailTemplates { get; set; }
	}
}