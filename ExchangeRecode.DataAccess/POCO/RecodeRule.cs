﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ExchangeRecode.DataAccess
{
	/// <summary>
	/// Правило перекодировки
	/// </summary>
	public class RecodingRule
	{
		[Key]
		public Guid Id { get; set; }

		public virtual RecodingProcess Process { get; set; }
		public string SourceSystemName { get; set; }
		public string DestinationSystemName { get; set; }
		public string EntityName { get; set; }
		public bool IsRequired { get; set; }
		public string XPath { get; set; }
		public string SetToValue { get; set; }

		public virtual ICollection<Condition> Conditions  { get; set; }
	}
}