﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ExchangeRecode.DataAccess
{
    /// <summary>
    /// Условие перекодировки. Указывается отдельно для каждого правила перекодировки.
    /// Если не выполняется, то перекодировка не будет выполнена. Указанный Xpath должен выбирать не пустое значение
    /// </summary>
    public class Condition
    {
        [Key]
        public Guid Id { get; set; }
        public virtual RecodingRule Rule { get; set; }
        public string Xpath { get; set; }
    }
}