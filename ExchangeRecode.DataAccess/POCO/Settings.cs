﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ExchangeRecode.DataAccess
{
    public class Settings
    {
        [Key]
        [MaxLength(256)]
        public string Name { get; set; }

        public string Value { get; set; }
    }
}