﻿CREATE PROCEDURE GetElementsName (@Id NVARCHAR(MAX), @ClientName NVARCHAR(MAX), @EntityName NVARCHAR(MAX))
AS
	BEGIN

	DECLARE @NsiCode NVARCHAR(MAX);
	DECLARE @SubjectsChildNsiCode NVARCHAR(MAX);
	DECLARE @SubjectsChildEntityName NVARCHAR(MAX);

	IF(@EntityName = 'Subject')
	BEGIN

		SELECT @Id = cdChild.ClientId, @EntityName = cdChild.EntityName
		FROM NaviconMDM.dbo.CodesMappingDetails cdSubj
		JOIN NaviconMDM.dbo.CodesMappingDetails cdChild
		ON cdSubj.DetailsId = cdChild.ParentId 
		AND cdSubj.ClientId = @Id AND cdSubj.ClientName = @ClientName AND cdSubj.EntityName = 'Subject'
		AND cdChild.EntityName IN ('LegalEntity', 'PIF', 'PhysicalPerson')

	END

	SELECT @NsiCode = cdNsi.ClientId, @SubjectsChildNsiCode = cdChild.ClientId, @SubjectsChildEntityName = cdChild.EntityName
	FROM NaviconMDM.dbo.CodesMappingDetails cd
	JOIN NaviconMDM.dbo.CodesMappingDetails cdNsi
		ON cd.MasterCode = cdNsi.MasterCode

	LEFT JOIN NaviconMDM.dbo.CodesMappingDetails cdChild
		ON cdChild.ParentId = cdNsi.DetailsId

	WHERE cd.EntityName = @EntityName AND cd.ClientId = @Id AND cd.ClientName = @ClientName
	AND cdNsi.ClientName = 'NSI'



	IF (@SubjectsChildNsiCode IS NOT NULL AND @SubjectsChildEntityName IS NOT NULL)
	BEGIN
		EXEC('SELECT Name FROM MDS.mdm.[Infinitum_'+@SubjectsChildEntityName+'] WHERE Code = ''' + @SubjectsChildNsiCode +'''')
	END
	ELSE
	BEGIN
		EXEC('SELECT Name FROM MDS.mdm.[Infinitum_'+@EntityName+'] WHERE Code = ''' + @NsiCode +'''')
	END
END