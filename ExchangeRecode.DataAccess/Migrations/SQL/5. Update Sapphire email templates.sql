﻿USE [RecodingRulesDb]
GO

INSERT INTO [dbo].[EmailTemplates] 
(
	[Id],
	[TemplateType_Id],
	[IsDefault],
	[EmailSubject],
	[Template]
)
VALUES
--XmlOutputDirectoryNotFound
('E09FD20F-2901-4309-B95A-3609FE494D54', 0, 0, 'Ошибка перекодировки', 
	N'<!DOCTYPE html>
	<html>
	  <head>
		<meta charset="utf-8">
		<title>Ошибка перекодировки</title>
		<style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} .exc {{ color: darkgrey; }}</style>
	  </head>
	  <body>
		<h1>Ошибка перекодировки</h1>
		<br>
		<div>При обработке файла @xmlFileName, полученного из папки @xmlSourceDirectory интеграционной шиной, произошла ошибка.</div>
		<div>Папка не найдена: @xmlOutputPath.</div>
		<br>
		
		<br>
		<div>@fileMoveReport</div>
		<br>
		<br>
		<div class="exc">@exception</div>
	  </body>
	</html>'),

--DocOutputDirectoryNotFound
('8E13AAA6-E96A-4566-BA65-361A0A498D42', 1, 0, 'Ошибка перекодировки', 
	N'<!DOCTYPE html>
	<html>
	  <head>
		<meta charset="utf-8">
		<title>Ошибка перекодировки</title>
		<style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} .exc {{ color: darkgrey; }}</style>
	  </head>
	  <body>
		<h1>Ошибка перекодировки</h1>
		<br>
		<div>При обработке документа @documentFileName, полученного из папки @documentSourceDirectory интеграционной шиной, произошла ошибка.</div>
		<div>Папка не найдена: @docOutputPath.</div>
		<br>
		
		<br>
		<div>@fileMoveReport</div>
		<br>
		<br>
		<div class="exc">@exception</div>
	  </body>
	</html>'),

--XmlOutputDirectoryUnauthorizedAccess
('625A4B16-73FA-4B58-8D33-3CBB11176082', 2, 0, 'Ошибка перекодировки', 
	N'<!DOCTYPE html>
	<html>
	  <head>
		<meta charset="utf-8">
		<title>Ошибка перекодировки</title>
		<style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} .exc {{ color: darkgrey; }}</style>
	  </head>
	  <body>
		<h1>Ошибка перекодировки</h1>
		<br>
		<div>При обработке файла @xmlFileName, полученного из папки @xmlSourceDirectory интеграционной шиной, произошла ошибка.</div>
		<div>Папка недоступна: @xmlOutputPath.</div>
		<br>
		
		<br>
		<div>@fileMoveReport</div>
		<br>
		<br>
		<div class="exc">@exception</div>
	  </body>
	</html>'),

--DocOutputDirectoryUnauthorizedAccess
('BF19F101-5E33-4FB1-947A-3E724CBF9FDC', 3, 0, 'Ошибка перекодировки', 
	N'<!DOCTYPE html>
	<html>
	  <head>
		<meta charset="utf-8">
		<title>Ошибка перекодировки</title>
		<style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} .exc {{ color: darkgrey; }}</style>
	  </head>
	  <body>
		<h1>Ошибка перекодировки</h1>
		<br>
		<div>При обработке документа @documentFileName, полученного из папки @documentSourceDirectory интеграционной шиной, произошла ошибка.</div>
		<div>Папка недоступна: @docOutputPath.</div>
		<br>
		
		<br>
		<div>@fileMoveReport</div>
		<br>
		<br>
		<div class="exc">@exception</div>
	  </body>
	</html>'),

--DocumentNotFound
('DEBEA055-6DC1-4439-AFCC-53CF14D48F25', 4, 0, 'Ошибка перекодировки', 
	N'<!DOCTYPE html>
	<html>
	  <head>
		<meta charset="utf-8">
		<title>Ошибка перекодировки</title>
		<style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} .exc {{ color: darkgrey; }}</style>
	  </head>
	  <body>
		<h1>Ошибка перекодировки</h1>
		<br>
		<div>При обработке файла @xmlFileName, полученного из папки @xmlSourceDirectory интеграционной шиной, произошла ошибка.</div>
		<div>Обязательный файл документа @documentFileName не был найден по адресу @documentSourceDirectory.</div>
		<br>
		
		<br>
		<div>@fileMoveReport</div>
	  </body>
	</html>'),

--GeneralException
('76F0A14A-130A-40C4-B779-5736DB914FCF', 5, 0, 'Ошибка перекодировки', 
	N'<!DOCTYPE html>
	<html>
	  <head>
		<meta charset="utf-8">
		<title>Ошибка перекодировки</title>
		<style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} .exc {{ color: darkgrey; }}</style>
	  </head>
	  <body>
		<h1>Ошибка перекодировки</h1>
		<br>
		<div>Произошла ошибка при обработке файла @xmlFileName, полученного из папки @xmlSourceDirectory,
		  <br>А также документа @documentFileName, полученного из папки @documentSourceDirectory</div>
		<br>
		
		<br>
		<div>@fileMoveReport</div>
		<br>
		<br>
		<div class="exc">@exception</div>
	  </body>
	</html>'),

--MappingException
('6075FB53-F51A-4362-BF42-60FA2F0E59FE', 6, 0, 'Ошибка перекодировки', 
	N'<!DOCTYPE html>
	<html>
	  <head>
		<meta charset="utf-8">
		<title>Ошибка перекодировки</title>
		<style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} .exc {{ color: darkgrey; }}</style>
	  </head>
	  <body>
		<h1>Ошибка перекодировки</h1>
		<br>
		<div>При обработке файла @xmlFileName, полученного из папки @xmlSourceDirectory интеграционной шиной, произошла ошибка.</div>
		<div>Указанное сопоставление идентификаторов не найдено в КХ НСИ</div>
		<div>В указанном выше XML файле по XPath выражению "@xpath" был найден тег "@tagName"</div>
		<div>Не удалось перекодировать его значение "@sourceId", 
		  как идентификатор сущности КХ НСИ "@entityName" из кодов @sourceSystemName в коды 
		  @destinationSystemName.</div>
		<br>
		<div style="color:red;font-weight:bold">После принудительной передачи контрагента в САПФИР сообщите о выполнении в ОО</div>
		@if(Name)
		{
		<div>Ожидаемый элемент: @Name</div>
		}
		<br>
		<div>@fileMoveReport</div>
	  </body>
	</html>
	'),

--MDXpathNoMathchesFound
('F957628A-4335-499F-B5F5-880C5325A9E3', 7, 0, 'Ошибка перекодировки', 
	N'<!DOCTYPE html>
	<html>
	  <head>
		<meta charset="utf-8">
		<title>Ошибка перекодировки</title>
		<style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} .exc {{ color: darkgrey; }}</style>
	  </head>
	  <body>
		<h1>Ошибка перекодировки</h1>
		<br>
		<div>При обработке файла @xmlFileName, полученного из папки @xmlSourceDirectory интеграционной шиной, произошла ошибка.</div>
		<div>В указанном выше XML файле, путь к документу определяется по Xpath выражению.
		   <br>
		   По XPath выражению "@xpath" не был найдено ни одного тега</div>
		<br>
		
		<br>
		<div>@fileMoveReport</div>
	  </body>
	</html>'),

--NoRecodingProcessFound
('21AFBCF5-8A17-44B8-93BF-9D5312DC9BBC', 8, 0, 'Ошибка перекодировки', 
	N'<!DOCTYPE html>
	<html>
	  <head>
		<meta charset="utf-8">
		<title>Ошибка перекодировки</title>
		<style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} .exc {{ color: darkgrey; }}</style>
	  </head>
	  <body>
		<h1>Ошибка перекодировки</h1>
		<br>
		<div>Для полученного файла @xmlFileName с корневым тегом @rootNodeName, 
		  полученного из папки @xmlSourceDirectory интеграционной шиной, не найдено процесса перекодировки.</div>
		  <br>
		
		<br>
		<div>@fileMoveReport</div>
	  </body>
	</html>'),

--CannotFindRecodeProcessForReceivedDocument
('3E68F7F4-BE75-4A39-A5D6-A067586745CB', 15, 0, 'Ошибка перекодировки', 
	N'<!DOCTYPE html>
	<html>
	  <head>
		<meta charset="utf-8">
		<title>Ошибка перекодировки</title>
		<style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} .exc {{ color: darkgrey; }}</style>
	  </head>
	  <body>
		<h1>Ошибка перекодировки</h1>
		<br>
		<div>Для полученного документа @documentFileName, полученного из папки @documentSourceDirectory интеграционной шиной, 
		  не найдено процесса перекодировки.</div>
		  <br>
		
		<br>
		<div>@fileMoveReport</div>
	  </body>
	</html>'),

--NoValueToRecode
('2880C0C8-BDC8-409B-A305-A459A24A6AC4', 9, 0, 'Ошибка перекодировки', 
	N'<!DOCTYPE html>
	<html>
	  <head>
		<meta charset="utf-8">
		<title>Ошибка перекодировки</title>
		<style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} .exc {{ color: darkgrey; }}</style>
	  </head>
	  <body>
		<h1>Ошибка перекодировки</h1>
		<br>
		<div>При обработке файла @xmlFileName, полученного из папки @xmlSourceDirectory интеграционной шиной, произошла ошибка.</div>
		<div>В указанном выше XML файле, для обязательного правила перекодировки с XPath выражением @xpath
		   <br>
		   был найден тег, но в нем отсутствует значение</div>
		<br>
		
		<br>
		<div>@fileMoveReport</div>
	  </body>
	</html>'),

--CannotParseXml
('0287E301-09C0-4785-B20C-D0CFF71B9E7C', 10, 0, 'Ошибка перекодировки', 
	N'<!DOCTYPE html>
	<html>
	  <head>
		<meta charset="utf-8">
		<title>Ошибка перекодировки</title>
		<style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} .exc {{ color: darkgrey; }}</style>
	  </head>
	  <body>
		<h1>Ошибка перекодировки</h1>
		<br>
		<div>При обработке файла @xmlFileName, полученного из папки @xmlSourceDirectory интеграционной шиной, произошла ошибка.</div>
		<div>Не удалось прочитать файл, как XML. Убедитесь, что файл является валидным XML файлом</div>
		<br>
		
		<br>
		<div>@fileMoveReport</div>
	  </body>
	</html>'),

--Success
('0D4323A2-F84F-4839-9FED-D53747E74945', 11, 0, 'Ошибка перекодировки', 
	N'<!DOCTYPE html>
	<html>
	  <head>
		<meta charset="utf-8">
		<title>Перекодировка прошла успешно</title>
		<style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} h3 {{ color: darkgrey; }}</style>
	  </head>
	  <body>
		<h1>Перекодировка прошла успешно</h1>
		<br>
		<div>Перекодировка файла @xmlFileName, полученного из папки @xmlSourceDirectory прошла успешно.</div>
		<div>Файл перемещен в папку @xmlFileOutputLocation.</div>
		@if (@documentSourceDirectory)
		{
		  <br>
		  <div>Также, документ @documentFileName был перемещен из папки @documentSourceDirectory <br>
		  в папку @documentOutputLocation
		  </div>
		}
	  </body>
	</html>'),

--XpathNoMathches
('73D1E28F-B545-4842-B8EA-E161BDC49CA5', 14, 0, 'Ошибка перекодировки',
	N'<!DOCTYPE html>
		<html>
		  <head>
			<meta charset="utf-8">
			<title>Ошибка перекодировки</title>
			<style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} .exc {{ color: darkgrey; }}</style>
		  </head>
		  <body>
			<h1>Ошибка перекодировки</h1>
			<br>
			<div>При обработке файла @xmlFileName, полученного из папки @xmlSourceDirectory интеграционной шиной, произошла ошибка.</div>
			<div>В указанном выше XML файле, для обязательного правила перекодировки с XPath выражением @xpath
			   <br>
			   не был найдено ни одного тега</div>
			<br>
			
			<br>
			<div>@fileMoveReport</div>
			
		  </body>
	</html>'),

--XmlFileWasMoved
('ECB498D3-A341-4BE8-BAF8-E4D67FBD1AE2', 12, 0, 'Ошибка перекодировки', 
	N'<div>
	  Xml файл @xmlFileName был перемещен из папки 
	  <br>@xmlSourceDirectory 
	  <br> в папку
	  <br>@xmlErrorPath
	</div>'),

--DocFileWasMoved
('7E5E2493-A2AF-4B5C-83E8-ECA4FB4F5F04', 13, 0, 'Ошибка перекодировки', 
	N'<div>
	  Документ @documentFileName был перемещен из папки 
	  <br>@documentSourceDirectory 
	  <br> в папку
	  <br>@docErrorPath
	</div>'),

--DocFileWasMoved
('CCE31FA6-AF44-458D-986C-F94604835B2B', 16, 0, 'Ошибка перекодировки', 
	N'<!DOCTYPE html>
	<html>
	  <head>
		<meta charset="utf-8">
		<title>Ошибка перекодировки</title>
		<style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} .exc {{ color: darkgrey; }}</style>
	  </head>
	  <body>
		<h1>Ошибка перекодировки</h1>
		<br>
		<div>Для документа @documentFileName, полученного из папки @documentSourceDirectory интеграционной шиной, 
		  не указана папка в которую его необходимо переложить.</div>
		<br>
		
		<br>
		<div>@fileMoveReport</div>
	  </body>
	</html>');

INSERT INTO [dbo].[EmailTemplateRecodingProcesses] 
([EmailTemplate_Id], [RecodingProcess_Id])
VALUES
('0287E301-09C0-4785-B20C-D0CFF71B9E7C',	'3806CC1F-FF1E-468E-AF87-E4CD1C23768A'),
('0D4323A2-F84F-4839-9FED-D53747E74945',	'3806CC1F-FF1E-468E-AF87-E4CD1C23768A'),
('21AFBCF5-8A17-44B8-93BF-9D5312DC9BBC',	'3806CC1F-FF1E-468E-AF87-E4CD1C23768A'),
('2880C0C8-BDC8-409B-A305-A459A24A6AC4',	'3806CC1F-FF1E-468E-AF87-E4CD1C23768A'),
('3E68F7F4-BE75-4A39-A5D6-A067586745CB',	'3806CC1F-FF1E-468E-AF87-E4CD1C23768A'),
('6075FB53-F51A-4362-BF42-60FA2F0E59FE',	'3806CC1F-FF1E-468E-AF87-E4CD1C23768A'),
('625A4B16-73FA-4B58-8D33-3CBB11176082',	'3806CC1F-FF1E-468E-AF87-E4CD1C23768A'),
('73D1E28F-B545-4842-B8EA-E161BDC49CA5',	'3806CC1F-FF1E-468E-AF87-E4CD1C23768A'),
('76F0A14A-130A-40C4-B779-5736DB914FCF',	'3806CC1F-FF1E-468E-AF87-E4CD1C23768A'),
('7E5E2493-A2AF-4B5C-83E8-ECA4FB4F5F04',	'3806CC1F-FF1E-468E-AF87-E4CD1C23768A'),
('8E13AAA6-E96A-4566-BA65-361A0A498D42',	'3806CC1F-FF1E-468E-AF87-E4CD1C23768A'),
('BF19F101-5E33-4FB1-947A-3E724CBF9FDC',	'3806CC1F-FF1E-468E-AF87-E4CD1C23768A'),
('CCE31FA6-AF44-458D-986C-F94604835B2B',	'3806CC1F-FF1E-468E-AF87-E4CD1C23768A'),
('DEBEA055-6DC1-4439-AFCC-53CF14D48F25',	'3806CC1F-FF1E-468E-AF87-E4CD1C23768A'),
('E09FD20F-2901-4309-B95A-3609FE494D54',	'3806CC1F-FF1E-468E-AF87-E4CD1C23768A'),
('ECB498D3-A341-4BE8-BAF8-E4D67FBD1AE2',	'3806CC1F-FF1E-468E-AF87-E4CD1C23768A'),
('F957628A-4335-499F-B5F5-880C5325A9E3',	'3806CC1F-FF1E-468E-AF87-E4CD1C23768A'),
('0287E301-09C0-4785-B20C-D0CFF71B9E7C',	'5678038C-6CFD-40C1-AC6F-357534BE44B3'),
('0D4323A2-F84F-4839-9FED-D53747E74945',	'5678038C-6CFD-40C1-AC6F-357534BE44B3'),
('21AFBCF5-8A17-44B8-93BF-9D5312DC9BBC',	'5678038C-6CFD-40C1-AC6F-357534BE44B3'),
('2880C0C8-BDC8-409B-A305-A459A24A6AC4',	'5678038C-6CFD-40C1-AC6F-357534BE44B3'),
('3E68F7F4-BE75-4A39-A5D6-A067586745CB',	'5678038C-6CFD-40C1-AC6F-357534BE44B3'),
('6075FB53-F51A-4362-BF42-60FA2F0E59FE',	'5678038C-6CFD-40C1-AC6F-357534BE44B3'),
('625A4B16-73FA-4B58-8D33-3CBB11176082',	'5678038C-6CFD-40C1-AC6F-357534BE44B3'),
('73D1E28F-B545-4842-B8EA-E161BDC49CA5',	'5678038C-6CFD-40C1-AC6F-357534BE44B3'),
('76F0A14A-130A-40C4-B779-5736DB914FCF',	'5678038C-6CFD-40C1-AC6F-357534BE44B3'),
('7E5E2493-A2AF-4B5C-83E8-ECA4FB4F5F04',	'5678038C-6CFD-40C1-AC6F-357534BE44B3'),
('8E13AAA6-E96A-4566-BA65-361A0A498D42',	'5678038C-6CFD-40C1-AC6F-357534BE44B3'),
('BF19F101-5E33-4FB1-947A-3E724CBF9FDC',	'5678038C-6CFD-40C1-AC6F-357534BE44B3'),
('CCE31FA6-AF44-458D-986C-F94604835B2B',	'5678038C-6CFD-40C1-AC6F-357534BE44B3'),
('DEBEA055-6DC1-4439-AFCC-53CF14D48F25',	'5678038C-6CFD-40C1-AC6F-357534BE44B3'),
('E09FD20F-2901-4309-B95A-3609FE494D54',	'5678038C-6CFD-40C1-AC6F-357534BE44B3'),
('ECB498D3-A341-4BE8-BAF8-E4D67FBD1AE2',	'5678038C-6CFD-40C1-AC6F-357534BE44B3'),
('F957628A-4335-499F-B5F5-880C5325A9E3',	'5678038C-6CFD-40C1-AC6F-357534BE44B3'),
('0287E301-09C0-4785-B20C-D0CFF71B9E7C',	'C3909076-72F6-46EE-BD19-2084ECF960DE'),
('0D4323A2-F84F-4839-9FED-D53747E74945',	'C3909076-72F6-46EE-BD19-2084ECF960DE'),
('21AFBCF5-8A17-44B8-93BF-9D5312DC9BBC',	'C3909076-72F6-46EE-BD19-2084ECF960DE'),
('2880C0C8-BDC8-409B-A305-A459A24A6AC4',	'C3909076-72F6-46EE-BD19-2084ECF960DE'),
('3E68F7F4-BE75-4A39-A5D6-A067586745CB',	'C3909076-72F6-46EE-BD19-2084ECF960DE'),
('6075FB53-F51A-4362-BF42-60FA2F0E59FE',	'C3909076-72F6-46EE-BD19-2084ECF960DE'),
('625A4B16-73FA-4B58-8D33-3CBB11176082',	'C3909076-72F6-46EE-BD19-2084ECF960DE'),
('73D1E28F-B545-4842-B8EA-E161BDC49CA5',	'C3909076-72F6-46EE-BD19-2084ECF960DE'),
('76F0A14A-130A-40C4-B779-5736DB914FCF',	'C3909076-72F6-46EE-BD19-2084ECF960DE'),
('7E5E2493-A2AF-4B5C-83E8-ECA4FB4F5F04',	'C3909076-72F6-46EE-BD19-2084ECF960DE'),
('8E13AAA6-E96A-4566-BA65-361A0A498D42',	'C3909076-72F6-46EE-BD19-2084ECF960DE'),
('BF19F101-5E33-4FB1-947A-3E724CBF9FDC',	'C3909076-72F6-46EE-BD19-2084ECF960DE'),
('CCE31FA6-AF44-458D-986C-F94604835B2B',	'C3909076-72F6-46EE-BD19-2084ECF960DE'),
('DEBEA055-6DC1-4439-AFCC-53CF14D48F25',	'C3909076-72F6-46EE-BD19-2084ECF960DE'),
('E09FD20F-2901-4309-B95A-3609FE494D54',	'C3909076-72F6-46EE-BD19-2084ECF960DE'),
('ECB498D3-A341-4BE8-BAF8-E4D67FBD1AE2',	'C3909076-72F6-46EE-BD19-2084ECF960DE'),
('F957628A-4335-499F-B5F5-880C5325A9E3',	'C3909076-72F6-46EE-BD19-2084ECF960DE'),
('0287E301-09C0-4785-B20C-D0CFF71B9E7C',	'F6E04023-A906-472E-9AF3-BEFCF9CB91DE'),
('0D4323A2-F84F-4839-9FED-D53747E74945',	'F6E04023-A906-472E-9AF3-BEFCF9CB91DE'),
('21AFBCF5-8A17-44B8-93BF-9D5312DC9BBC',	'F6E04023-A906-472E-9AF3-BEFCF9CB91DE'),
('2880C0C8-BDC8-409B-A305-A459A24A6AC4',	'F6E04023-A906-472E-9AF3-BEFCF9CB91DE'),
('3E68F7F4-BE75-4A39-A5D6-A067586745CB',	'F6E04023-A906-472E-9AF3-BEFCF9CB91DE'),
('6075FB53-F51A-4362-BF42-60FA2F0E59FE',	'F6E04023-A906-472E-9AF3-BEFCF9CB91DE'),
('625A4B16-73FA-4B58-8D33-3CBB11176082',	'F6E04023-A906-472E-9AF3-BEFCF9CB91DE'),
('73D1E28F-B545-4842-B8EA-E161BDC49CA5',	'F6E04023-A906-472E-9AF3-BEFCF9CB91DE'),
('76F0A14A-130A-40C4-B779-5736DB914FCF',	'F6E04023-A906-472E-9AF3-BEFCF9CB91DE'),
('7E5E2493-A2AF-4B5C-83E8-ECA4FB4F5F04',	'F6E04023-A906-472E-9AF3-BEFCF9CB91DE'),
('8E13AAA6-E96A-4566-BA65-361A0A498D42',	'F6E04023-A906-472E-9AF3-BEFCF9CB91DE'),
('BF19F101-5E33-4FB1-947A-3E724CBF9FDC',	'F6E04023-A906-472E-9AF3-BEFCF9CB91DE'),
('CCE31FA6-AF44-458D-986C-F94604835B2B',	'F6E04023-A906-472E-9AF3-BEFCF9CB91DE'),
('DEBEA055-6DC1-4439-AFCC-53CF14D48F25',	'F6E04023-A906-472E-9AF3-BEFCF9CB91DE'),
('E09FD20F-2901-4309-B95A-3609FE494D54',	'F6E04023-A906-472E-9AF3-BEFCF9CB91DE'),
('ECB498D3-A341-4BE8-BAF8-E4D67FBD1AE2',	'F6E04023-A906-472E-9AF3-BEFCF9CB91DE'),
('F957628A-4335-499F-B5F5-880C5325A9E3',	'F6E04023-A906-472E-9AF3-BEFCF9CB91DE')