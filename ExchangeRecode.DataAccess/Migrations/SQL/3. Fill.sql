﻿USE [RecodingRulesDb]
GO
INSERT [dbo].[RecodingProcesses]
([Id], [InputFolderName], [OutputFolderName], [RootNodeName], [MailAddress], [ErrorFolder], [MailOnSuccess], [MainDocumentInputFileDirectory], [MainDocumentInputXPath], [MainDocumentOutputFileDirectory], [MainDocumentOutputXPath], [MainDocumentErrorOutputFileDirectory], [MainDocumentErrorOutputXPath], [MailSubject], [MainDocumentIsRequired]) 
VALUES 
--Sapphire
 (N'f6e04023-a906-472e-9af3-befcf9cb91de', N'\\btsrv-nav\SPECDEP\APP\Sapphire\IN\XML_cover\Original', N'\\btsrv-nav\SPECDEP\APP\Sapphire\IN\XML_cover\Converted', N'income_spd_sapphire',  N'L.Chapakov@specdep.ru', N'\\btsrv-nav\SPECDEP\APP\Sapphire\IN\Error', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'SPD. Ошибка интеграции с Сапфиром.', 0)
 ,(N'cbd1084e-8e76-460f-9532-a9bbd552131b', N'\\btsrv-nav\SPECDEP\APP\Sapphire\OUT\XML_cover\Original', N'\\btsrv-nav\SPECDEP\APP\Sapphire\OUT\XML_cover\Converted', N'outcome_sapphire_spd',  N'L.Chapakov@specdep.ru', N'\\btsrv-nav\SPECDEP\APP\Sapphire\OUT\Error\', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'SPD. Ошибка интеграции с Сапфиром.', 0)
 ,(N'3806cc1f-ff1e-468e-af87-e4cd1c23768a', N'\\btsrv-nav\SPECDEP\APP\Sapphire\Модуль_расчета_графиков\xml_scout\Original', N'\\btsrv-nav\SPECDEP\APP\Sapphire\Модуль_расчета_графиков\xml_scout\Converted', N'Кредитный_актив',  N'L.Chapakov@specdep.ru', N'\\btsrv-nav\SPECDEP\APP\Sapphire\Модуль_расчета_графиков\xml_scout\Error', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'SPD. Ошибка интеграции с Сапфиром.', 0)
 ,(N'5678038c-6cfd-40c1-ac6f-357534be44b3', N'\\btsrv-nav\SPECDEP\APP\Sapphire\IN\XML_formilized\Original', N'\\btsrv-nav\SPECDEP\APP\Sapphire\IN\XML_formilized\Converted', N'formalized_agreement_spd_sapphire',  N'L.Chapakov@specdep.ru', N'\\btsrv-nav\SPECDEP\APP\Sapphire\IN\Error', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'SPD. Ошибка интеграции с Сапфиром.', 0)
 ,(N'c3909076-72f6-46ee-bd19-2084ecf960de', N'\\btsrv-nav\SPECDEP\APP\Sapphire\IN\XML_formilized\Original', N'\\btsrv-nav\SPECDEP\APP\Sapphire\IN\XML_formilized\Converted', N'formalized_bank_spd_sapphire',  N'L.Chapakov@specdep.ru', N'\\btsrv-nav\SPECDEP\APP\Sapphire\IN\Error', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'SPD. Ошибка интеграции с Сапфиром.', 0)
--'Test
 ,(N'd9ef8966-ea0c-41e3-aeab-4540ba84ccec', N'C:\Temp\InputTest', N'C:\Temp\OutTest', N'catalog',  N'navicon@specdep.ru', N'C:\Temp\OutError', 1, NULL, NULL, NULL, NULL, NULL, NULL, N'SPD. Ошибка интеграции с Сапфиром.', 0)


INSERT [dbo].[RecodingRules] 
([Process_Id], [SourceSystemName], [DestinationSystemName], [EntityName], [XPath], [Id], [IsRequired], [SetToValue]) 
VALUES 
(N'c3909076-72f6-46ee-bd19-2084ecf960de', N'SPD', N'SAPFIR', N'OperationsCashFlow', N'//operation', N'96307c1d-71e6-4401-9cc1-14add0165186', 1, NULL)
,(N'c3909076-72f6-46ee-bd19-2084ecf960de', N'SPD', N'SAPFIR', N'Subject', N'//bank', N'1d4d8e8b-4fa7-477f-b496-1708788a9a71', 1, NULL)
,(N'c3909076-72f6-46ee-bd19-2084ecf960de', N'SPD', N'SAPFIR', N'Currency', N'//deal_currency', N'3046053e-2ad0-4577-a2b1-2ec56ce9a1d1', 1, NULL)
,(N'5678038c-6cfd-40c1-ac6f-357534be44b3', N'SPD', N'SAPFIR', N'Subject', N'//contractor', N'1e98fbf2-f104-46e9-a408-317f412afca5', 1, NULL)
,(N'f6e04023-a906-472e-9af3-befcf9cb91de', N'SPD', N'SAPFIR', N'Subject', N'//sender', N'c858f6b5-fc57-4a03-b214-4050c94fb02c', 1, NULL)
,(N'c3909076-72f6-46ee-bd19-2084ecf960de', N'SPD', N'SAPFIR', N'BankAccount', N'//customer_account', N'd7669439-afb2-4edc-ba15-4d00893d5544', 1, NULL)
,(N'5678038c-6cfd-40c1-ac6f-357534be44b3', N'SPD', N'SAPFIR', N'Asset', N'//assets', N'e09e41d9-d9a6-451f-99d8-4e659c087324', 0, NULL)
,(N'f6e04023-a906-472e-9af3-befcf9cb91de', N'SPD', N'SAPFIR', N'LegalEntity', N'//UK', N'd865dbc8-a633-401e-b54e-51d41bfa8939', 1, NULL)
,(N'cbd1084e-8e76-460f-9532-a9bbd552131b', N'SAPFIR', N'SPD', N'Employee', N'//emp_code', N'73880548-b2a1-4784-b159-594a8828adcb', 1, NULL)
,(N'd9ef8966-ea0c-41e3-aeab-4540ba84ccec', N'NSI', N'SPD', N'LegalEntity', N'catalog/book/price', N'4f747f5f-df48-44ad-a181-796304cb5d94', 1, N'BABABABABAB')
,(N'd9ef8966-ea0c-41e3-aeab-4540ba84ccec', N'SPD', N'Fansy', N'LegalEntity', N'catalog/book/price', N'd6f8a0f4-1fc8-45fb-80be-8904121a89b6', 1, NULL)
,(N'cbd1084e-8e76-460f-9532-a9bbd552131b', N'SAPFIR', N'SPD', N'LegalEntity', N'//UK', N'36b472d6-3b62-40f3-9acf-8bb787c27cbb', 1, NULL)
,(N'5678038c-6cfd-40c1-ac6f-357534be44b3', N'SPD', N'SAPFIR', N'OperationsCashFlow', N'//operation', N'6083cfb3-995a-45e6-bd24-8eeaf04e5e74', 0, NULL)
,(N'5678038c-6cfd-40c1-ac6f-357534be44b3', N'SPD', N'SAPFIR', N'OperationsDirectionOfTransaction', N'//deal_type', N'3af764ef-2fa6-40c1-8842-a2af4432c851', 0, NULL)
,(N'c3909076-72f6-46ee-bd19-2084ecf960de', N'SPD', N'SAPFIR', N'Subject', N'//contractor', N'e457925d-3342-41c5-a2e0-b507a67a7592', 1, NULL)
,(N'5678038c-6cfd-40c1-ac6f-357534be44b3', N'SPD', N'SAPFIR', N'Currency', N'//currency', N'd0305e02-4967-4eac-846a-e23a9dee613f', 1, NULL)
,(N'5678038c-6cfd-40c1-ac6f-357534be44b3', N'SPD', N'SAPFIR', N'BankAccount', N'//customer_account', N'e38bb9c0-a16d-4c6c-9bb3-eb2260cbc29f', 1, NULL)
,(N'3806cc1f-ff1e-468e-af87-e4cd1c23768a', N'SPD', N'SAPFIR', N'Currency', N'//Валюта_кредита', N'f12c23aa-63b9-499a-928a-f836041720d3', 1, NULL)


INSERT [dbo].[Conditions] ([Id], [Rule_Id], [Xpath]) 
VALUES 
(N'76aa334c-f91b-40a6-9203-37ccf93356eb', N'd6f8a0f4-1fc8-45fb-80be-8904121a89b6', N'catalog/somecondition')


INSERT [dbo].[Settings] (Name, Value)
VALUES 
(N'DefaultEmailAddress', N'navicon@specdep.ru')
,(N'DefaultErrorFolder', N'C:\XML_Error');

--'

INSERT INTO [dbo].[EmailTemplates] 
(
	[Id],
	[TemplateType_Id],
	[IsDefault],
	[EmailSubject],
	[Template]
)
VALUES
--XmlOutputDirectoryNotFound
('247AD86C-BE8A-4F3E-88C8-032FCAB75B99', 0, 1, 'Ошибка перекодировки', 
	N'<!DOCTYPE html>
	<html>
	  <head>
	    <meta charset="utf-8">
	    <title>Ошибка перекодировки</title>
	    <style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} .exc {{ color: darkgrey; }}</style>
	  </head>
	  <body>
	    <h1>Ошибка перекодировки</h1>
	    <br>
	    <div>При обработке файла @xmlFileName, полученного из папки @xmlSourceDirectory интеграционной шиной, произошла ошибка.</div>
	    <div>Папка не найдена: @xmlOutputPath.</div>
	    <br>
	    <div>@fileMoveReport</div>
	    <br>
	    <br>
	    <div class="exc">@exception</div>
	  </body>
	</html>'),

--DocOutputDirectoryNotFound
('06A594EB-564F-430B-96A0-2C408731CB48', 1, 1, 'Ошибка перекодировки', 
	N'<!DOCTYPE html>
	<html>
	  <head>
	    <meta charset="utf-8">
	    <title>Ошибка перекодировки</title>
	    <style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} .exc {{ color: darkgrey; }}</style>
	  </head>
	  <body>
	    <h1>Ошибка перекодировки</h1>
	    <br>
	    <div>При обработке документа @documentFileName, полученного из папки @documentSourceDirectory интеграционной шиной, произошла ошибка.</div>
	    <div>Папка не найдена: @docOutputPath.</div>
	    <br>
	    <div>@fileMoveReport</div>
	    <br>
	    <br>
	    <div class="exc">@exception</div>
	  </body>
	</html>'),

--XmlOutputDirectoryUnauthorizedAccess
('E958D341-204E-4964-B201-37284EA81383', 2, 1, 'Ошибка перекодировки', 
	N'<!DOCTYPE html>
	<html>
	  <head>
	    <meta charset="utf-8">
	    <title>Ошибка перекодировки</title>
	    <style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} .exc {{ color: darkgrey; }}</style>
	  </head>
	  <body>
	    <h1>Ошибка перекодировки</h1>
	    <br>
	    <div>При обработке файла @xmlFileName, полученного из папки @xmlSourceDirectory интеграционной шиной, произошла ошибка.</div>
	    <div>Папка недоступна: @xmlOutputPath.</div>
	    <br>
	    <div>@fileMoveReport</div>
	    <br>
	    <br>
	    <div class="exc">@exception</div>
	  </body>
	</html>'),

--DocOutputDirectoryUnauthorizedAccess
('D2572D7E-8A8E-4D4A-AFB9-38DCFBFC343D', 3, 1, 'Ошибка перекодировки', 
	N'<!DOCTYPE html>
	<html>
	  <head>
	    <meta charset="utf-8">
	    <title>Ошибка перекодировки</title>
	    <style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} .exc {{ color: darkgrey; }}</style>
	  </head>
	  <body>
	    <h1>Ошибка перекодировки</h1>
	    <br>
	    <div>При обработке документа @documentFileName, полученного из папки @documentSourceDirectory интеграционной шиной, произошла ошибка.</div>
	    <div>Папка недоступна: @docOutputPath.</div>
	    <br>
	    <div>@fileMoveReport</div>
	    <br>
	    <br>
	    <div class="exc">@exception</div>
	  </body>
	</html>'),

--DocumentNotFound
('5220263F-AD74-4BBC-A562-5F3654167258', 4, 1, 'Ошибка перекодировки', 
	N'<!DOCTYPE html>
	<html>
	  <head>
	    <meta charset="utf-8">
	    <title>Ошибка перекодировки</title>
	    <style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} .exc {{ color: darkgrey; }}</style>
	  </head>
	  <body>
	    <h1>Ошибка перекодировки</h1>
	    <br>
	    <div>При обработке файла @xmlFileName, полученного из папки @xmlSourceDirectory интеграционной шиной, произошла ошибка.</div>
	    <div>Обязательный файл документа @documentFileName не был найден по адресу @documentSourceDirectory.</div>
	    <br>
	    <div>@fileMoveReport</div>
	  </body>
	</html>'),

--GeneralException
('DE8329AC-BFDF-474A-B108-761CCA98E23B', 5, 1, 'Ошибка перекодировки', 
	N'<!DOCTYPE html>
	<html>
	  <head>
	    <meta charset="utf-8">
	    <title>Ошибка перекодировки</title>
	    <style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} .exc {{ color: darkgrey; }}</style>
	  </head>
	  <body>
	    <h1>Ошибка перекодировки</h1>
	    <br>
	    <div>Произошла ошибка при обработке файла @xmlFileName, полученного из папки @xmlSourceDirectory,
	      <br>А также документа @documentFileName, полученного из папки @documentSourceDirectory</div>
	    <br>
	    <div>@fileMoveReport</div>
	    <br>
	    <br>
	    <div class="exc">@exception</div>
	  </body>
	</html>'),

--MappingException
('E662495D-5BFF-4789-965F-7F13AF88A3A6', 6, 1, 'Ошибка перекодировки', 
	N'<!DOCTYPE html>
	<html>
	  <head>
	    <meta charset="utf-8">
	    <title>Ошибка перекодировки</title>
	    <style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} .exc {{ color: darkgrey; }}</style>
	  </head>
	  <body>
	    <h1>Ошибка перекодировки</h1>
	    <br>
	    <div>При обработке файла @xmlFileName, полученного из папки @xmlSourceDirectory интеграционной шиной, произошла ошибка.</div>
	    <div>Указанное сопоставление идентификаторов не найдено в КХ НСИ</div>
	    <div>В указанном выше XML файле по XPath выражению "@xpath" был найден тег "@tagName"</div>
	    <div>Не удалось перекодировать его значение "@sourceId", 
	      как идентификатор сущности КХ НСИ "@entityName" из кодов @sourceSystemName в коды 
	      @destinationSystemName.</div>
	    <br>
	    <div>@fileMoveReport</div>
	  </body>
	</html>
	'),

--MDXpathNoMathchesFound
('85946EAC-9CE6-453C-A419-81A960298805', 7, 1, 'Ошибка перекодировки', 
	N'<!DOCTYPE html>
	<html>
	  <head>
	    <meta charset="utf-8">
	    <title>Ошибка перекодировки</title>
	    <style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} .exc {{ color: darkgrey; }}</style>
	  </head>
	  <body>
	    <h1>Ошибка перекодировки</h1>
	    <br>
	    <div>При обработке файла @xmlFileName, полученного из папки @xmlSourceDirectory интеграционной шиной, произошла ошибка.</div>
	    <div>В указанном выше XML файле, путь к документу определяется по Xpath выражению.
	       <br>
	       По XPath выражению "@xpath" не был найдено ни одного тега</div>
	    <br>
	    <div>@fileMoveReport</div>
	  </body>
	</html>'),

--NoRecodingProcessFound
('15412E0B-5E05-4F4C-9C99-8819345D7891', 8, 1, 'Ошибка перекодировки', 
	N'<!DOCTYPE html>
	<html>
	  <head>
	    <meta charset="utf-8">
	    <title>Ошибка перекодировки</title>
	    <style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} .exc {{ color: darkgrey; }}</style>
	  </head>
	  <body>
	    <h1>Ошибка перекодировки</h1>
	    <br>
	    <div>Для полученного файла @xmlFileName с корневым тегом @rootNodeName, 
	      полученного из папки @xmlSourceDirectory интеграционной шиной, не найдено процесса перекодировки.</div>
	      <br>
	    <div>@fileMoveReport</div>
	  </body>
	</html>'),

--CannotFindRecodeProcessForReceivedDocument
('5A63AF55-357A-4FD6-A092-E66986544FC4', 15, 1, 'Ошибка перекодировки', 
	N'<!DOCTYPE html>
	<html>
	  <head>
	    <meta charset="utf-8">
	    <title>Ошибка перекодировки</title>
	    <style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} .exc {{ color: darkgrey; }}</style>
	  </head>
	  <body>
	    <h1>Ошибка перекодировки</h1>
	    <br>
	    <div>Для полученного документа @documentFileName, полученного из папки @documentSourceDirectory интеграционной шиной, 
	      не найдено процесса перекодировки.</div>
	      <br>
	    <div>@fileMoveReport</div>
	  </body>
	</html>'),

--NoValueToRecode
('9CC3C499-DCD8-41B1-B6E5-8F82480A1E96', 9, 1, 'Ошибка перекодировки', 
	N'<!DOCTYPE html>
	<html>
	  <head>
	    <meta charset="utf-8">
	    <title>Ошибка перекодировки</title>
	    <style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} .exc {{ color: darkgrey; }}</style>
	  </head>
	  <body>
	    <h1>Ошибка перекодировки</h1>
	    <br>
	    <div>При обработке файла @xmlFileName, полученного из папки @xmlSourceDirectory интеграционной шиной, произошла ошибка.</div>
	    <div>В указанном выше XML файле, для обязательного правила перекодировки с XPath выражением @xpath
	       <br>
	       был найден тег, но в нем отсутствует значение</div>
	    <br>
	    <div>@fileMoveReport</div>
	  </body>
	</html>'),

--CannotParseXml
('D8923CB7-A103-4B89-A3D4-B6E2C24054B1', 10, 1, 'Ошибка перекодировки', 
	N'<!DOCTYPE html>
	<html>
	  <head>
	    <meta charset="utf-8">
	    <title>Ошибка перекодировки</title>
	    <style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} .exc {{ color: darkgrey; }}</style>
	  </head>
	  <body>
	    <h1>Ошибка перекодировки</h1>
	    <br>
	    <div>При обработке файла @xmlFileName, полученного из папки @xmlSourceDirectory интеграционной шиной, произошла ошибка.</div>
	    <div>Не удалось прочитать файл, как XML. Убедитесь, что файл является валидным XML файлом</div>
	    <br>
	    <div>@fileMoveReport</div>
	  </body>
	</html>'),

--Success
('91725F49-EBE7-44CF-9A55-B940BEBD73DB', 11, 1, 'Ошибка перекодировки', 
	N'<!DOCTYPE html>
	<html>
	  <head>
	    <meta charset="utf-8">
	    <title>Перекодировка прошла успешно</title>
	    <style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} h3 {{ color: darkgrey; }}</style>
	  </head>
	  <body>
	    <h1>Перекодировка прошла успешно</h1>
	    <br>
	    <div>Перекодировка файла @xmlFileName, полученного из папки @xmlSourceDirectory прошла успешно.</div>
	    <div>Файл перемещен в папку @xmlFileOutputLocation.</div>
	    @if (@documentSourceDirectory)
	    {
	      <br>
	      <div>Также, документ @documentFileName был перемещен из папки @documentSourceDirectory <br>
	      в папку @documentOutputLocation
	      </div>
	    }
	  </body>
	</html>'),

--XpathNoMathches
('7A202F89-7775-4C94-9C2D-DB47FC869139', 14, 1, 'Ошибка перекодировки',
	N'<!DOCTYPE html>
		<html>
		  <head>
		    <meta charset="utf-8">
		    <title>Ошибка перекодировки</title>
		    <style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} .exc {{ color: darkgrey; }}</style>
		  </head>
		  <body>
		    <h1>Ошибка перекодировки</h1>
		    <br>
		    <div>При обработке файла @xmlFileName, полученного из папки @xmlSourceDirectory интеграционной шиной, произошла ошибка.</div>
		    <div>В указанном выше XML файле, для обязательного правила перекодировки с XPath выражением @xpath
		       <br>
		       не был найдено ни одного тега</div>
		    <br>
		    <div>@fileMoveReport</div>
		  </body>
	</html>'),

--XmlFileWasMoved
('4E74BB7C-F1A3-4F32-A9A5-BB5E77C69981', 12, 1, 'Ошибка перекодировки', 
	N'<div>
	  Xml файл @xmlFileName был перемещен из папки 
	  <br>@xmlSourceDirectory 
	  <br> в папку
	  <br>@xmlErrorPath
	</div>'),

--DocFileWasMoved
('61B9B9EF-A7EA-46C2-B798-C23B67FDB612', 13, 1, 'Ошибка перекодировки', 
	N'<div>
	  Документ @documentFileName был перемещен из папки 
	  <br>@documentSourceDirectory 
	  <br> в папку
	  <br>@docErrorPath
	</div>'),

--DocFileWasMoved
('D549A005-14B1-4042-A756-AA9079C217AD', 16, 1, 'Ошибка перекодировки', 
	N'<!DOCTYPE html>
	<html>
	  <head>
	    <meta charset="utf-8">
	    <title>Ошибка перекодировки</title>
	    <style>h1, h2, h3 {{text-align: center; }} h2 {{ color: red; }} .exc {{ color: darkgrey; }}</style>
	  </head>
	  <body>
	    <h1>Ошибка перекодировки</h1>
	    <br>
	    <div>Для документа @documentFileName, полученного из папки @documentSourceDirectory интеграционной шиной, 
	      не указана папка в которую его необходимо переложить.</div>
	      <br>
	    <div>@fileMoveReport</div>
	  </body>
	</html>');