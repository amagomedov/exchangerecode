﻿INSERT [dbo].[RecodingProcesses]
([Id], [InputFolderName], [OutputFolderName], [RootNodeName], [MailAddress], [ErrorFolder], [MailOnSuccess], [MainDocumentInputFileDirectory], [MainDocumentInputXPath], [MainDocumentOutputFileDirectory], [MainDocumentOutputXPath], [MainDocumentErrorOutputFileDirectory], [MainDocumentErrorOutputXPath], [MailSubject], [MainDocumentIsRequired], [Xpath]) 
VALUES 
--KKD
  (N'd615f054-eb93-4fda-8a1b-0f5d5f1fa955', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Original', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Converted', N'MeetingNotification',  N'navicon@specdep.ru;O.Yuryeva@specdep.ru', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Error', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'Ошибка регистрации депозитарного документа (Fansy)', 0, NULL)
 ,(N'986a1e55-c125-49e8-b910-2e69fdae4d82', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Original', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Converted', N'MeetingCancellation',  N'navicon@specdep.ru;O.Yuryeva@specdep.ru', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Error', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'Ошибка регистрации депозитарного документа (Fansy)', 0, NULL)
 ,(N'4deef7ef-9fe5-47cf-8511-3463759ea884', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Original', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Converted', N'MeetingInstruction',  N'navicon@specdep.ru;O.Yuryeva@specdep.ru', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Error', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'Ошибка регистрации депозитарного документа (Fansy)', 0, NULL)
 ,(N'6ef901a1-588c-4166-96b7-35a1fc589bb5', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Original', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Converted', N'MeetingInstructionStatus',  N'navicon@specdep.ru;O.Yuryeva@specdep.ru', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Error', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'Ошибка регистрации депозитарного документа (Fansy)', 0, NULL)
 ,(N'7a928ca2-e9d1-410a-b267-3abd40813aea', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Original', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Converted', N'MeetingResult Dissemination',  N'navicon@specdep.ru;O.Yuryeva@specdep.ru', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Error', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'Ошибка регистрации депозитарного документа (Fansy)', 0, NULL)
 ,(N'443f6912-30a4-405b-aa98-416647bcf252', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Original', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Converted', N'MeetingResultDissemination',  N'navicon@specdep.ru;O.Yuryeva@specdep.ru', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Error', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'Ошибка регистрации депозитарного документа (Fansy)', 0, NULL)
 ,(N'c177351e-5294-49bd-bde4-51e6a0f06961', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Original', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Converted', N'CorporateActionNotification',  N'navicon@specdep.ru;O.Yuryeva@specdep.ru', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Error', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'Ошибка регистрации депозитарного документа (Fansy)', 0, NULL)
 ,(N'5e5447f4-00a3-4141-8955-574f48c62aac', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Original', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Converted', N'CorporateActionEventProcessingStatusAdvice',  N'navicon@specdep.ru;O.Yuryeva@specdep.ru', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Error', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'Ошибка регистрации депозитарного документа (Fansy)', 0, NULL)
 ,(N'734f1e7e-308b-4e9d-9619-7774c227ef49', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Original', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Converted', N'CorporateActionInstruction',  N'navicon@specdep.ru;O.Yuryeva@specdep.ru', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Error', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'Ошибка регистрации депозитарного документа (Fansy)', 0, NULL)
 ,(N'e8760382-5dfa-4401-8366-7929b03c585a', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Original', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Converted', N'CorporateActionInstructionStatusAdvice',  N'navicon@specdep.ru;O.Yuryeva@specdep.ru', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Error', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'Ошибка регистрации депозитарного документа (Fansy)', 0, NULL)
 ,(N'c81b1182-ceaf-4529-8417-7a367b3fb945', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Original', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Converted', N'CorporateActionMovementConfirmation',  N'navicon@specdep.ru;O.Yuryeva@specdep.ru', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Error', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'Ошибка регистрации депозитарного документа (Fansy)', 0, NULL)
 ,(N'd628fa31-6646-4b7b-8186-7d5bb1592732', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Original', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Converted', N'CorporateActionCancellationAdvice',  N'navicon@specdep.ru;O.Yuryeva@specdep.ru', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Error', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'Ошибка регистрации депозитарного документа (Fansy)', 0, NULL)
 ,(N'9e4d1cb7-f6ac-4c77-b68a-8e4066350d20', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Original', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Converted', N'CorporateActionMovementPreliminaryAdvice',  N'navicon@specdep.ru;O.Yuryeva@specdep.ru', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Error', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'Ошибка регистрации депозитарного документа (Fansy)', 0, NULL)
 ,(N'4858a5fc-ac50-44ec-894c-985c16120be4', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Original', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Converted', N'CorporateActionInstructionCancellationRequest',  N'navicon@specdep.ru;O.Yuryeva@specdep.ru', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Error', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'Ошибка регистрации депозитарного документа (Fansy)', 0, NULL)
 ,(N'13a4eb2d-47d5-44d0-8fac-9c3854f2088f', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Original', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Converted', N'CorporateActionInstructionCancellationRequestStatusAdvice',  N'navicon@specdep.ru;O.Yuryeva@specdep.ru', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Error', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'Ошибка регистрации депозитарного документа (Fansy)', 0, NULL)
 ,(N'c54c1095-9758-451a-b192-9fd544f656fb', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Original', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Converted', N'MessageReject',  N'navicon@specdep.ru;O.Yuryeva@specdep.ru', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Error', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'Ошибка регистрации депозитарного документа (Fansy)', 0, NULL)
 ,(N'74e81a34-19ea-426a-986a-a7acab21083a', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Original', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Converted', N'SystemEventNotification',  N'navicon@specdep.ru;O.Yuryeva@specdep.ru', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Error', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'Ошибка регистрации депозитарного документа (Fansy)', 0, NULL)
 ,(N'5d302b2e-3365-42df-b5e4-ba9d284cd1fd', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Original', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Converted', N'CorporateActionMovementPreliminaryAdviceReport',  N'navicon@specdep.ru;O.Yuryeva@specdep.ru', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Error', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'Ошибка регистрации депозитарного документа (Fansy)', 0, NULL)
 ,(N'bdbe61c0-7073-4167-b4a8-d6451bb88757', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Original', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Converted', N'IntraPositionMovementConfirmation',  N'navicon@specdep.ru;O.Yuryeva@specdep.ru', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Error', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'Ошибка регистрации депозитарного документа (Fansy)', 0, NULL)
 ,(N'2954b204-ef52-45f6-9e75-ef2594008a9c', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Original', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Converted', N'IntraPositionMovementInstruction',  N'navicon@specdep.ru;O.Yuryeva@specdep.ru', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Error', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'Ошибка регистрации депозитарного документа (Fansy)', 0, NULL)
 ,(N'04434361-d804-4e4d-8743-ffeda4b65b80', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Original', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Converted', N'IntraPositionMovementStatusAdvice',  N'navicon@specdep.ru;O.Yuryeva@specdep.ru', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_IN\KKD\Error', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'Ошибка регистрации депозитарного документа (Fansy)', 0, NULL)

--FANSY OUT COVER_XML:Fansy_Depo_Metainformaion
 ,(N'53bf0f04-3b62-45cf-947b-3289f02ad660', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_OUT\XML\COVER_XML', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_OUT\XML\COVER_XML\Converted', N'Fansy_Depo_Metainformaion',  N'navicon@specdep.ru;O.Yuryeva@specdep.ru', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_OUT\XML\COVER_XML\Error', 1, NULL, N'//Document_Name', NULL, NULL, NULL, N'//Document_Name', N'Ошибка регистрации депозитарного документа (Fansy)', 0, NULL)
--FANSY OUT COVER_XML:\\Document_Type 
 ,(N'34793c59-6ca2-451f-abc8-7c8ca1432451', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_OUT\XML\COVER_XML', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_OUT\XML\COVER_XML\Converted', NULL,  N'navicon@specdep.ru;O.Yuryeva@specdep.ru', N'\\btsrv-nav\ExRecTest\FANSY_DEPO_OUT\XML\COVER_XML\Error', 1, NULL, N'//Document_Name', NULL, NULL, NULL, N'//Document_Name', N'Ошибка регистрации депозитарного документа (Fansy)', 0, N'//Document_Type[text()="CA011" or text()="CA012" or text()="CA013" or text()="CA014" or text()="CA021" or text()="CA041" or text()="CA042" or text()="CA043" or text()="CA044" or text()="CA045" or text()="CA061" or text()="CA081" or text()="CA082" or text()="CA312" or text()="CA311" or text()="CA321" or text()="CA331" or text()="CA341" or text()="CA361" or text()="CA391" or text()="CA351" or text()="CA401" or text()="CA411" or text()="AM021" or text()="SN041" or text()="ND001" ]')

INSERT [dbo].[RecodingRules] 
([Process_Id], [SourceSystemName], [DestinationSystemName], [EntityName], [XPath], [Id], [IsRequired], [SetToValue]) 
VALUES 
 (N'04434361-d804-4e4d-8743-ffeda4b65b80', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Id', N'96d85da7-0a5e-40c1-b049-3da5b27cbded', 0, NULL)
,(N'04434361-d804-4e4d-8743-ffeda4b65b80', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Id', N'6288E6C7-BC3A-4D15-9923-0E88BFFBD117', 0, NULL)
,(N'04434361-d804-4e4d-8743-ffeda4b65b80', N'SPD', N'Fansy', N'Subject', N'//AppHdr/To/OrgId/Id/OrgId/Othr/Id', N'27c77f3c-3a07-4f00-b97d-ecdb05edb840', 0, NULL)
,(N'04434361-d804-4e4d-8743-ffeda4b65b80', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Issr', N'5adac313-e917-4b42-9772-9ff42270aa00', 0, N'SPED')
,(N'04434361-d804-4e4d-8743-ffeda4b65b80', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Issr', N'6A038467-2E6D-47BC-AFAA-036E032CE5AD', 0, N'SPED')
,(N'04434361-d804-4e4d-8743-ffeda4b65b80', NULL, NULL, NULL, N'//AppHdr/To/OrgId/Id/OrgId/Othr/Issr', N'6cafa423-ba1e-47ff-9fc3-837e64b88522', 0, N'SPED')
,(N'13a4eb2d-47d5-44d0-8fac-9c3854f2088f', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Id', N'cf41487d-05d9-41e4-9b9d-2cd79d27011e', 0, NULL)
,(N'13a4eb2d-47d5-44d0-8fac-9c3854f2088f', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Id', N'21009C3F-5C74-40D4-9D9C-16609EC0F770', 0, NULL)
,(N'13a4eb2d-47d5-44d0-8fac-9c3854f2088f', N'SPD', N'Fansy', N'Subject', N'//AppHdr/To/OrgId/Id/OrgId/Othr/Id', N'4daac0d3-89d1-41a4-b45f-c6d7d378f038', 0, NULL)
,(N'13a4eb2d-47d5-44d0-8fac-9c3854f2088f', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Issr', N'635ceaa2-9cf6-4f62-9770-eb2efcc9a8d9', 0, N'SPED')
,(N'13a4eb2d-47d5-44d0-8fac-9c3854f2088f', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Issr', N'E0CD253E-858D-4A3C-9B08-052CBA3D9DB0', 0, N'SPED')
,(N'13a4eb2d-47d5-44d0-8fac-9c3854f2088f', NULL, NULL, NULL, N'//AppHdr/To/OrgId/Id/OrgId/Othr/Issr', N'd9bdabb5-20fa-4fdb-88e7-5185cba6a219', 0, N'SPED')
,(N'2954b204-ef52-45f6-9e75-ef2594008a9c', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Id', N'e84e96c1-a78e-4798-b518-c1aad88f8117', 0, NULL)
,(N'2954b204-ef52-45f6-9e75-ef2594008a9c', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Id', N'004C3919-1066-425E-AA74-1E7ABBB6EBC4', 0, NULL)
,(N'2954b204-ef52-45f6-9e75-ef2594008a9c', N'SPD', N'Fansy', N'Subject', N'//AppHdr/To/OrgId/Id/OrgId/Othr/Id', N'3037bbaa-fe49-4ea7-961e-73abf01a649b', 0, NULL)
,(N'2954b204-ef52-45f6-9e75-ef2594008a9c', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Issr', N'd94ae7ca-8b29-4393-8456-c9b1aed03845', 0, N'SPED')
,(N'2954b204-ef52-45f6-9e75-ef2594008a9c', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Issr', N'DD21E14A-6C4F-479A-AA3F-157368C58F82', 0, N'SPED')
,(N'2954b204-ef52-45f6-9e75-ef2594008a9c', NULL, NULL, NULL, N'//AppHdr/To/OrgId/Id/OrgId/Othr/Issr', N'098225d7-0a67-45a0-8faa-5c69fb946a8c', 0, N'SPED')
,(N'443f6912-30a4-405b-aa98-416647bcf252', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Id', N'3201f43f-9398-4717-8aec-864488d683c3', 0, NULL)
,(N'443f6912-30a4-405b-aa98-416647bcf252', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Id', N'DD7811A0-E63D-497A-8451-1FBD33EA0CD7', 0, NULL)
,(N'443f6912-30a4-405b-aa98-416647bcf252', N'SPD', N'Fansy', N'Subject', N'//AppHdr/To/OrgId/Id/OrgId/Othr/Id', N'2817f01d-c40c-471b-a6e6-ae33a1a632e1', 0, NULL)
,(N'443f6912-30a4-405b-aa98-416647bcf252', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Issr', N'f271849c-b17a-49f4-80d3-f178dd9bcedb', 0, N'SPED')
,(N'443f6912-30a4-405b-aa98-416647bcf252', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Issr', N'3A36AFCE-E354-4511-B3D1-158AF177ACEE', 0, N'SPED')
,(N'443f6912-30a4-405b-aa98-416647bcf252', NULL, NULL, NULL, N'//AppHdr/To/OrgId/Id/OrgId/Othr/Issr', N'9ed5a509-f9ce-4990-8e6b-88ed9591c0af', 0, N'SPED')
,(N'4858a5fc-ac50-44ec-894c-985c16120be4', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Id', N'cdd76126-4ece-4a37-92f1-927c620c6f56', 0, NULL)
,(N'4858a5fc-ac50-44ec-894c-985c16120be4', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Id', N'4AB51267-2EA1-4E0F-B496-223B7F032DC5', 0, NULL)
,(N'4858a5fc-ac50-44ec-894c-985c16120be4', N'SPD', N'Fansy', N'Subject', N'//AppHdr/To/OrgId/Id/OrgId/Othr/Id', N'4ff3886b-a3fb-4658-a303-3855fba42d3b', 0, NULL)
,(N'4858a5fc-ac50-44ec-894c-985c16120be4', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Issr', N'c6466416-24b4-499e-a753-4187c55b7a7a', 0, N'SPED')
,(N'4858a5fc-ac50-44ec-894c-985c16120be4', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Issr', N'90F23F74-F640-414B-A733-186671CD549F', 0, N'SPED')
,(N'4858a5fc-ac50-44ec-894c-985c16120be4', NULL, NULL, NULL, N'//AppHdr/To/OrgId/Id/OrgId/Othr/Issr', N'834d3846-12da-4249-8ef3-ce37821802dc', 0, N'SPED')
,(N'4deef7ef-9fe5-47cf-8511-3463759ea884', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Id', N'3def2bcd-ffc2-45ab-ae48-715bd7ebc615', 0, NULL)
,(N'4deef7ef-9fe5-47cf-8511-3463759ea884', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Id', N'25DED930-D432-4D23-8863-22CE61A28894', 0, NULL)
,(N'4deef7ef-9fe5-47cf-8511-3463759ea884', N'SPD', N'Fansy', N'Subject', N'//AppHdr/To/OrgId/Id/OrgId/Othr/Id', N'54f9a96d-2b1a-42df-939c-6072e70a6efe', 0, NULL)
,(N'4deef7ef-9fe5-47cf-8511-3463759ea884', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Issr', N'a3e33c33-27ba-4f62-a9be-91880ce5cbab', 0, N'SPED')
,(N'4deef7ef-9fe5-47cf-8511-3463759ea884', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Issr', N'1C0020CC-B6DE-42CF-B548-2F1714F932E9', 0, N'SPED')
,(N'4deef7ef-9fe5-47cf-8511-3463759ea884', NULL, NULL, NULL, N'//AppHdr/To/OrgId/Id/OrgId/Othr/Issr', N'3d01402d-24f9-4754-b69d-0d2aa36c561c', 0, N'SPED')

,(N'53bf0f04-3b62-45cf-947b-3289f02ad660', N'Fansy', N'SPD', N'Subject', N'//Recipient_ID', N'44fa0934-beb6-4b8e-a0be-1a61ee5efb61', 1, NULL)
,(N'34793c59-6ca2-451f-abc8-7c8ca1432451', N'Fansy', N'SPD', N'Subject', N'//Recipient_ID', N'338AFBB2-C9D0-48B2-ABAD-148255F1FC3E', 1, NULL)


,(N'5d302b2e-3365-42df-b5e4-ba9d284cd1fd', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Id', N'2c5b8228-b48a-4644-84ca-f94cd74b50d8', 0, NULL)
,(N'5d302b2e-3365-42df-b5e4-ba9d284cd1fd', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Id', N'6E477E3E-6BF8-4C73-8C79-29AEEEAC0BC8', 0, NULL)
,(N'5d302b2e-3365-42df-b5e4-ba9d284cd1fd', N'SPD', N'Fansy', N'Subject', N'//AppHdr/To/OrgId/Id/OrgId/Othr/Id', N'ae4f3ed9-6dcd-4d61-8d27-25e9e506bc7e', 0, NULL)
,(N'5d302b2e-3365-42df-b5e4-ba9d284cd1fd', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Issr', N'5ae14559-43d4-43d3-8562-ae4fc66c1995', 0, N'SPED')
,(N'5d302b2e-3365-42df-b5e4-ba9d284cd1fd', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Issr', N'048F34A2-DC34-49EF-9A05-4308DCB8BFA5', 0, N'SPED')
,(N'5d302b2e-3365-42df-b5e4-ba9d284cd1fd', NULL, NULL, NULL, N'//AppHdr/To/OrgId/Id/OrgId/Othr/Issr', N'e7f05208-de9a-426b-91c5-094630e0f8bf', 0, N'SPED')
,(N'5e5447f4-00a3-4141-8955-574f48c62aac', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Id', N'6b449f2d-e532-462d-91e0-d96f9c413f8a', 0, NULL)
,(N'5e5447f4-00a3-4141-8955-574f48c62aac', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Id', N'F84C2D63-6DF1-4C09-BF0A-37A73A13B061', 0, NULL)
,(N'5e5447f4-00a3-4141-8955-574f48c62aac', N'SPD', N'Fansy', N'Subject', N'//AppHdr/To/OrgId/Id/OrgId/Othr/Id', N'5374703e-fc47-49d0-bc36-193d0fd782a6', 0, NULL)
,(N'5e5447f4-00a3-4141-8955-574f48c62aac', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Issr', N'05a0236b-09ca-40b1-b9ee-0e6492008363', 0, N'SPED')
,(N'5e5447f4-00a3-4141-8955-574f48c62aac', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Issr', N'62F6F13E-897A-4A49-993C-43F7C6F819A7', 0, N'SPED')
,(N'5e5447f4-00a3-4141-8955-574f48c62aac', NULL, NULL, NULL, N'//AppHdr/To/OrgId/Id/OrgId/Othr/Issr', N'94d4125d-9aad-4c97-a044-8aed70b82f99', 0, N'SPED')
,(N'6ef901a1-588c-4166-96b7-35a1fc589bb5', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Id', N'2c50c7f6-51d9-429a-a481-df711288a931', 0, NULL)
,(N'6ef901a1-588c-4166-96b7-35a1fc589bb5', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Id', N'F5DD0C52-E657-46F1-83FF-3C5407415C02', 0, NULL)
,(N'6ef901a1-588c-4166-96b7-35a1fc589bb5', N'SPD', N'Fansy', N'Subject', N'//AppHdr/To/OrgId/Id/OrgId/Othr/Id', N'f1efa7e0-b40e-43ae-aa66-ac75f054cfe4', 0, NULL)
,(N'6ef901a1-588c-4166-96b7-35a1fc589bb5', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Issr', N'd9ad140c-59b8-4140-a472-6ca601c759e8', 0, N'SPED')
,(N'6ef901a1-588c-4166-96b7-35a1fc589bb5', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Issr', N'992B2A14-63F1-4DB7-B1B4-5C2890C5D08A', 0, N'SPED')
,(N'6ef901a1-588c-4166-96b7-35a1fc589bb5', NULL, NULL, NULL, N'//AppHdr/To/OrgId/Id/OrgId/Othr/Issr', N'f3480907-0e1c-4784-933d-b8e0e302373f', 0, N'SPED')
,(N'734f1e7e-308b-4e9d-9619-7774c227ef49', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Id', N'd47f51cc-18f5-40d0-8404-79f5b8ed5049', 0, NULL)
,(N'734f1e7e-308b-4e9d-9619-7774c227ef49', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Id', N'F0BA92F8-748C-41B9-BEB4-466B341EC620', 0, NULL)
,(N'734f1e7e-308b-4e9d-9619-7774c227ef49', N'SPD', N'Fansy', N'Subject', N'//AppHdr/To/OrgId/Id/OrgId/Othr/Id', N'435402f6-c80d-448c-8fdb-efcaec11b220', 0, NULL)
,(N'734f1e7e-308b-4e9d-9619-7774c227ef49', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Issr', N'775ea08f-f1b4-4ecc-8cf2-d54fc5ec7cf8', 0, N'SPED')
,(N'734f1e7e-308b-4e9d-9619-7774c227ef49', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Issr', N'5FBDFFA6-AC44-4972-A9FC-607FE280BEC1', 0, N'SPED')
,(N'734f1e7e-308b-4e9d-9619-7774c227ef49', NULL, NULL, NULL, N'//AppHdr/To/OrgId/Id/OrgId/Othr/Issr', N'2770a6d3-2271-444b-af94-7a7de7e2dfa7', 0, N'SPED')
,(N'74e81a34-19ea-426a-986a-a7acab21083a', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Id', N'326769b6-b674-4eae-a8a7-4e4b28876b1c', 0, NULL)
,(N'74e81a34-19ea-426a-986a-a7acab21083a', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Id', N'CF122E92-9DCE-4D48-A5DF-46C03B76482C', 0, NULL)
,(N'74e81a34-19ea-426a-986a-a7acab21083a', N'SPD', N'Fansy', N'Subject', N'//AppHdr/To/OrgId/Id/OrgId/Othr/Id', N'b4b86ab3-8c6c-4410-b744-3e7a99f6752b', 0, NULL)
,(N'74e81a34-19ea-426a-986a-a7acab21083a', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Issr', N'84a0330d-ee58-487b-a137-448da402cc24', 0, N'SPED')
,(N'74e81a34-19ea-426a-986a-a7acab21083a', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Issr', N'548463B2-8E49-465F-90C3-6414ADD7FA3C', 0, N'SPED')
,(N'74e81a34-19ea-426a-986a-a7acab21083a', NULL, NULL, NULL, N'//AppHdr/To/OrgId/Id/OrgId/Othr/Issr', N'34e1a049-315d-4671-9de9-8811e9bc6b0e', 0, N'SPED')
,(N'7a928ca2-e9d1-410a-b267-3abd40813aea', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Id', N'838498ed-e4c7-4123-9460-23af85477215', 0, NULL)
,(N'7a928ca2-e9d1-410a-b267-3abd40813aea', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Id', N'4F9491AF-A59D-4A6F-8D01-474FA271505D', 0, NULL)
,(N'7a928ca2-e9d1-410a-b267-3abd40813aea', N'SPD', N'Fansy', N'Subject', N'//AppHdr/To/OrgId/Id/OrgId/Othr/Id', N'bada6729-0316-4b0d-8a69-c9c6a451a503', 0, NULL)
,(N'7a928ca2-e9d1-410a-b267-3abd40813aea', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Issr', N'4530c160-e10f-4f1c-a59d-f5fd2c4a29a1', 0, N'SPED')
,(N'7a928ca2-e9d1-410a-b267-3abd40813aea', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Issr', N'5CAB8EE4-B8BB-4372-9258-64295C76D7A2', 0, N'SPED')
,(N'7a928ca2-e9d1-410a-b267-3abd40813aea', NULL, NULL, NULL, N'//AppHdr/To/OrgId/Id/OrgId/Othr/Issr', N'11a13b7a-c2cf-4927-a6b3-f4c4a6c6a892', 0, N'SPED')
,(N'986a1e55-c125-49e8-b910-2e69fdae4d82', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Id', N'81f86da4-5c03-4a32-95f5-448ff25c062d', 0, NULL)
,(N'986a1e55-c125-49e8-b910-2e69fdae4d82', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Id', N'DA52FE8B-89A7-4953-9B0C-4825E5FC86F8', 0, NULL)
,(N'986a1e55-c125-49e8-b910-2e69fdae4d82', N'SPD', N'Fansy', N'Subject', N'//AppHdr/To/OrgId/Id/OrgId/Othr/Id', N'f07932a5-4481-4893-bf7e-c9bea5cd6409', 0, NULL)
,(N'986a1e55-c125-49e8-b910-2e69fdae4d82', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Issr', N'6651ead2-c2fd-4962-8f90-fdb30a4a5909', 0, N'SPED')
,(N'986a1e55-c125-49e8-b910-2e69fdae4d82', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Issr', N'99C39225-8A2B-43D2-BB7A-66A8EAF76013', 0, N'SPED')
,(N'986a1e55-c125-49e8-b910-2e69fdae4d82', NULL, NULL, NULL, N'//AppHdr/To/OrgId/Id/OrgId/Othr/Issr', N'02073701-c006-40ac-ba51-9b4c8cf19073', 0, N'SPED')
,(N'9e4d1cb7-f6ac-4c77-b68a-8e4066350d20', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Id', N'faca2a47-6df4-491d-8eec-e5ef4188b800', 0, NULL)
,(N'9e4d1cb7-f6ac-4c77-b68a-8e4066350d20', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Id', N'BB1DB082-F38C-423D-A8F3-50C62F90E08A', 0, NULL)
,(N'9e4d1cb7-f6ac-4c77-b68a-8e4066350d20', N'SPD', N'Fansy', N'Subject', N'//AppHdr/To/OrgId/Id/OrgId/Othr/Id', N'04643855-02ff-4bff-a37c-82c5ec77c2aa', 0, NULL)
,(N'9e4d1cb7-f6ac-4c77-b68a-8e4066350d20', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Issr', N'947b9066-b3b3-481d-b7d6-a45c6049b8b0', 0, N'SPED')
,(N'9e4d1cb7-f6ac-4c77-b68a-8e4066350d20', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Issr', N'BC1099E6-BA80-46A2-BBD0-6E24BC569833', 0, N'SPED')
,(N'9e4d1cb7-f6ac-4c77-b68a-8e4066350d20', NULL, NULL, NULL, N'//AppHdr/To/OrgId/Id/OrgId/Othr/Issr', N'27ccf443-4d39-49be-b8b9-2c1956207845', 0, N'SPED')
,(N'bdbe61c0-7073-4167-b4a8-d6451bb88757', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Id', N'a372d5ea-a38e-4810-9a08-ff7b2f8c914c', 0, NULL)
,(N'bdbe61c0-7073-4167-b4a8-d6451bb88757', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Id', N'FEA38F32-B78B-458B-B9B8-5E91DFB8B5B7', 0, NULL)
,(N'bdbe61c0-7073-4167-b4a8-d6451bb88757', N'SPD', N'Fansy', N'Subject', N'//AppHdr/To/OrgId/Id/OrgId/Othr/Id', N'd76bc0f7-f2af-479a-b4df-a7bca451fe0d', 0, NULL)
,(N'bdbe61c0-7073-4167-b4a8-d6451bb88757', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Issr', N'6a87079d-5257-4e79-a6e2-066ea303aa97', 0, N'SPED')
,(N'bdbe61c0-7073-4167-b4a8-d6451bb88757', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Issr', N'B045F385-F1FA-4563-ABC1-6E3F5A363394', 0, N'SPED')
,(N'bdbe61c0-7073-4167-b4a8-d6451bb88757', NULL, NULL, NULL, N'//AppHdr/To/OrgId/Id/OrgId/Othr/Issr', N'02bfd224-a109-437e-88b3-365d9f82910d', 0, N'SPED')
,(N'c177351e-5294-49bd-bde4-51e6a0f06961', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Id', N'0fb4e544-db24-44af-87cc-562e066ad074', 0, NULL)
,(N'c177351e-5294-49bd-bde4-51e6a0f06961', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Id', N'055CD14D-C782-4720-90CF-60E2C61479CD', 0, NULL)
,(N'c177351e-5294-49bd-bde4-51e6a0f06961', N'SPD', N'Fansy', N'Subject', N'//AppHdr/To/OrgId/Id/OrgId/Othr/Id', N'e67db3e1-0b8b-447d-b661-135f020f0ab3', 0, NULL)
,(N'c177351e-5294-49bd-bde4-51e6a0f06961', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Issr', N'1aeb917e-28eb-471e-a879-47121ec724de', 0, N'SPED')
,(N'c177351e-5294-49bd-bde4-51e6a0f06961', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Issr', N'CE68C8B4-D3CE-4644-8A04-6F764EA18BEF', 0, N'SPED')
,(N'c177351e-5294-49bd-bde4-51e6a0f06961', NULL, NULL, NULL, N'//AppHdr/To/OrgId/Id/OrgId/Othr/Issr', N'a5342fd5-9bc6-4372-a727-536d41068d70', 0, N'SPED')
,(N'c54c1095-9758-451a-b192-9fd544f656fb', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Id', N'db8702c8-faf0-4afc-9067-e196651b145b', 0, NULL)
,(N'c54c1095-9758-451a-b192-9fd544f656fb', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Id', N'010E0E8F-2D5A-4FC5-8184-64C05C4381DF', 0, NULL)
,(N'c54c1095-9758-451a-b192-9fd544f656fb', N'SPD', N'Fansy', N'Subject', N'//AppHdr/To/OrgId/Id/OrgId/Othr/Id', N'2f1e7bdc-d2f5-4db5-85d1-1381af91b6d9', 0, NULL)
,(N'c54c1095-9758-451a-b192-9fd544f656fb', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Issr', N'34780e16-3055-45c7-a4f6-1b86f884d467', 0, N'SPED')
,(N'c54c1095-9758-451a-b192-9fd544f656fb', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Issr', N'CE21CBE9-7DC0-459B-9004-7A406D386CBE', 0, N'SPED')
,(N'c54c1095-9758-451a-b192-9fd544f656fb', NULL, NULL, NULL, N'//AppHdr/To/OrgId/Id/OrgId/Othr/Issr', N'1a94f91d-5b40-4c20-9206-5fabf7c08cf6', 0, N'SPED')
,(N'c81b1182-ceaf-4529-8417-7a367b3fb945', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Id', N'94c4d924-6621-4644-952f-14c0f5da6b52', 0, NULL)
,(N'c81b1182-ceaf-4529-8417-7a367b3fb945', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Id', N'E43C045A-DCE1-485F-8A6F-69690420B110', 0, NULL)
,(N'c81b1182-ceaf-4529-8417-7a367b3fb945', N'SPD', N'Fansy', N'Subject', N'//AppHdr/To/OrgId/Id/OrgId/Othr/Id', N'8276447f-2b63-4f1f-804f-4f3adef2604d', 0, NULL)
,(N'c81b1182-ceaf-4529-8417-7a367b3fb945', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Issr', N'48788dbf-b64f-4a08-9e76-df776e556007', 0, N'SPED')
,(N'c81b1182-ceaf-4529-8417-7a367b3fb945', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Issr', N'E3E6B583-4BA0-497F-BC72-7B4A7A60D760', 0, N'SPED')
,(N'c81b1182-ceaf-4529-8417-7a367b3fb945', NULL, NULL, NULL, N'//AppHdr/To/OrgId/Id/OrgId/Othr/Issr', N'453c727d-0a8f-446f-8c8b-fdb4f5e582fd', 0, N'SPED')
,(N'd615f054-eb93-4fda-8a1b-0f5d5f1fa955', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Id', N'd0e0de78-fe20-4fe2-a5d7-dfa1d6785845', 0, NULL)
,(N'd615f054-eb93-4fda-8a1b-0f5d5f1fa955', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Id', N'591BBA4F-52B0-4E33-AE18-72CCF2C711F1', 0, NULL)
,(N'd615f054-eb93-4fda-8a1b-0f5d5f1fa955', N'SPD', N'Fansy', N'Subject', N'//AppHdr/To/OrgId/Id/OrgId/Othr/Id', N'89f06181-332d-4381-bf0f-56f7447e7a21', 0, NULL)
,(N'd615f054-eb93-4fda-8a1b-0f5d5f1fa955', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Issr', N'2b3b393c-0d02-44b9-8116-dd93862c1c94', 0, N'SPED')
,(N'd615f054-eb93-4fda-8a1b-0f5d5f1fa955', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Issr', N'04E4F2C6-F870-4E1E-9697-80623F367018', 0, N'SPED')
,(N'd615f054-eb93-4fda-8a1b-0f5d5f1fa955', NULL, NULL, NULL, N'//AppHdr/To/OrgId/Id/OrgId/Othr/Issr', N'c4a39c14-362d-4ed1-a103-771782da07c0', 0, N'SPED')
,(N'd628fa31-6646-4b7b-8186-7d5bb1592732', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Id', N'e4784ba0-e911-461d-8f72-8123181bc5c0', 0, NULL)
,(N'd628fa31-6646-4b7b-8186-7d5bb1592732', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Id', N'2ED9586E-E0E9-453D-975B-7B203964787C', 0, NULL)
,(N'd628fa31-6646-4b7b-8186-7d5bb1592732', N'SPD', N'Fansy', N'Subject', N'//AppHdr/To/OrgId/Id/OrgId/Othr/Id', N'e0993ba5-58f6-45f6-8f72-4d1816da93df', 0, NULL)
,(N'd628fa31-6646-4b7b-8186-7d5bb1592732', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Issr', N'8991bb94-233d-42dd-9fa7-cab0a2770d0a', 0, N'SPED')
,(N'd628fa31-6646-4b7b-8186-7d5bb1592732', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Issr', N'15406D76-7D49-43D3-B512-828FB28F29EC', 0, N'SPED')
,(N'd628fa31-6646-4b7b-8186-7d5bb1592732', NULL, NULL, NULL, N'//AppHdr/To/OrgId/Id/OrgId/Othr/Issr', N'444dbe7d-d48e-4bf7-b2ed-713ee5d8965f', 0, N'SPED')
,(N'e8760382-5dfa-4401-8366-7929b03c585a', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Id', N'e97c2be0-e18f-4fe3-b917-8596e455afd1', 0, NULL)
,(N'e8760382-5dfa-4401-8366-7929b03c585a', N'SPD', N'Fansy', N'Subject', N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Id', N'172A8B37-5B54-45B7-AF6D-9B7BF2A395E3', 0, NULL)
,(N'e8760382-5dfa-4401-8366-7929b03c585a', N'SPD', N'Fansy', N'Subject', N'//AppHdr/To/OrgId/Id/OrgId/Othr/Id', N'0abe5741-ad75-4c64-997d-7d9ddf99a0e8', 0, NULL)
,(N'e8760382-5dfa-4401-8366-7929b03c585a', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/OrgId/Othr/Issr', N'1ec8d657-06ae-4c1a-b6f1-902ae23c3908', 0, N'SPED')
,(N'e8760382-5dfa-4401-8366-7929b03c585a', NULL, NULL, NULL, N'//AppHdr/Fr/OrgId/Id/PrvtId/Othr/Issr', N'513BEF35-C870-4D88-B96D-82E73BE6C76E', 0, N'SPED')
,(N'e8760382-5dfa-4401-8366-7929b03c585a', NULL, NULL, NULL, N'//AppHdr/To/OrgId/Id/OrgId/Othr/Issr', N'634fea15-3267-4539-978f-d6ec0d646564', 0, N'SPED')

--INSERT [dbo].[Conditions] ([Id], [Rule_Id], [Xpath]) 
--VALUES 
--(N'f7fdcbe9-d0b0-4fbd-81c6-429cbba9b5be', N'44fa0934-beb6-4b8e-a0be-1a61ee5efb61', N'//Document_Type[text()="CA011" or text()="CA012" or text()="CA013" or text()="CA014" or text()="CA021" or text()="CA041" or text()="CA042" or text()="CA043" or text()="CA044" or text()="CA045" or text()="CA061" or text()="CA081" or text()="CA082" or text()="CA312" or text()="CA311" or text()="CA321" or text()="CA331" or text()="CA341" or text()="CA361" or text()="CA391" or text()="CA351" or text()="CA401" or text()="CA411" or text()="AM021" or text()="SN041" or text()="ND001" ]')



INSERT INTO [dbo].[EmailTemplates] 
(
  [Id],
  [TemplateType_Id],
  [IsDefault],
  [EmailSubject],
  [Template]
)
VALUES
--XmlOutputDirectoryNotFound
('DF87BC4B-99C9-4472-A843-4A5CD6DA0624', 0, 0, 'Ошибка перекодировки КИШ', 
  N'<!DOCTYPE html>
  <html>
	<head>
	  <meta charset="utf-8">
	  <title>Ошибка перекодировки</title>
	</head>
	<body>
	  <h1>При перекодировке (КИШ) депозитарного документа произошла ошибка «Ошибка регистрации депозитарного документа (Fansy)»</h1>
	  <br>
	  <div>При перекодировке (КИШ) файла по КД @xmlFileName в директории @xmlSourceDirectory произошла ошибка перекодировки.</div>
	  <div>Папка не найдена: @xmlOutputPath.</div>
	  <br>
	  <div>@fileMoveReport</div>
	  <br>
	  <br>
	  <div class="exc">@exception</div>
	</body>
  </html>'),

--DocOutputDirectoryNotFound
('59CAE334-9C1C-44C3-88A9-2388FA994465', 1, 0, 'Ошибка перекодировки КИШ', 
  N'<!DOCTYPE html>
  <html>
	<head>
	  <meta charset="utf-8">
	  <title>Ошибка перекодировки</title>
	</head>
	<body>
	  <h1>При перекодировке (КИШ) депозитарного документа произошла ошибка «Ошибка регистрации депозитарного документа (Fansy)»</h1>
	  <br>
	  <div>При обработке документа @documentFileName, полученного из папки @documentSourceDirectory интеграционной шиной, произошла ошибка.</div>
	  <div>Папка не найдена: @docOutputPath.</div>
	  <br>
	  <div>@fileMoveReport</div>
	  <br>
	  <br>
	  <div class="exc">@exception</div>
	</body>
  </html>'),

--XmlOutputDirectoryUnauthorizedAccess
('C24B3942-D503-4DDC-B9D2-23024CC4CA7E', 2, 0, 'Ошибка перекодировки КИШ', 
  N'<!DOCTYPE html>
  <html>
	<head>
	  <meta charset="utf-8">
	  <title>Ошибка перекодировки</title>
	</head>
	<body>
	  <h1>При перекодировке (КИШ) депозитарного документа произошла ошибка «Ошибка регистрации депозитарного документа (Fansy)»</h1>
	  <br>
	  <div>При перекодировке (КИШ) файла по КД @xmlFileName в директории @xmlSourceDirectory произошла ошибка перекодировки.</div>
	  <div>Папка недоступна: @xmlOutputPath.</div>
	  <br>
	  <div>@fileMoveReport</div>
	  <br>
	  <br>
	  <div class="exc">@exception</div>
	</body>
  </html>'),

--DocOutputDirectoryUnauthorizedAccess
('0401BBFF-A7D7-4A4D-A01D-3B4AA71B201C', 3, 0, 'Ошибка перекодировки КИШ', 
  N'<!DOCTYPE html>
  <html>
	<head>
	  <meta charset="utf-8">
	  <title>Ошибка перекодировки</title>
	</head>
	<body>
	  <h1>При перекодировке (КИШ) депозитарного документа произошла ошибка «Ошибка регистрации депозитарного документа (Fansy)»</h1>
	  <br>
	  <div>При обработке документа @documentFileName, полученного из папки @documentSourceDirectory интеграционной шиной, произошла ошибка.</div>
	  <div>Папка недоступна: @docOutputPath.</div>
	  <br>
	  <div>@fileMoveReport</div>
	  <br>
	  <br>
	  <div class="exc">@exception</div>
	</body>
  </html>'),

--DocumentNotFound
('F5C3B8DC-2EE6-4D21-87ED-D0228C5339EE', 4, 0, 'Ошибка перекодировки КИШ', 
  N'<!DOCTYPE html>
  <html>
	<head>
	  <meta charset="utf-8">
	  <title>Ошибка перекодировки</title>
	</head>
	<body>
	  <h1>При перекодировке (КИШ) депозитарного документа произошла ошибка «Ошибка регистрации депозитарного документа (Fansy)»</h1>
	  <br>
	  <div>При перекодировке (КИШ) файла по КД @xmlFileName в директории @xmlSourceDirectory произошла ошибка перекодировки.</div>
	  <div>Обязательный файл документа @documentFileName не был найден по адресу @documentSourceDirectory.</div>
	  <br>
	  <div>@fileMoveReport</div>
	</body>
  </html>'),

--GeneralException
('5079E09D-5120-4029-8B10-30D2A3C53CFE', 5, 0, 'Ошибка перекодировки КИШ', 
  N'<!DOCTYPE html>
  <html>
	<head>
	  <meta charset="utf-8">
	  <title>Ошибка перекодировки</title>
	</head>
	<body>
	  <h1>При перекодировке (КИШ) депозитарного документа произошла ошибка «Ошибка регистрации депозитарного документа (Fansy)»</h1>
	  <br>
	  <div>Произошла ошибка при обработке файла @xmlFileName, полученного из папки @xmlSourceDirectory,
		<br>А также документа @documentFileName, полученного из папки @documentSourceDirectory</div>
	  <br>
	  <div>@fileMoveReport</div>
	  <br>
	  <br>
	  <div class="exc">@exception</div>
	</body>
  </html>'),

--MappingException
('730BD96A-F23F-42F3-8C68-9D72F9BD378F', 6, 0, 'Ошибка перекодировки КИШ', 
  N'<!DOCTYPE html>
  <html>
	<head>
	  <meta charset="utf-8">
	  <title>Ошибка перекодировки</title>
	</head>
	<body>
	  <h1>При перекодировке (КИШ) депозитарного документа произошла ошибка</h1>
	  <br>
	  <div>При перекодировке (КИШ) файла по КД <br>
		@xmlFileName в директории @xmlSourceDirectory <br> 
		произошла ошибка перекодировки.</div>
		<br>
	  <div>Указанное сопоставление идентификаторов не найдено в КХ НСИ</div>
	  <div>Не удалось перекодировать значение тега "@tagName", 
		как идентификатор сущности КХ НСИ "@entityName" из кодов @sourceSystemName в коды 
		@destinationSystemName.</div>
	  <br>
	  <div>@fileMoveReport</div>
	</body>
  </html>'),

--MDXpathNoMathchesFound
('B0CDE795-430D-4744-BE97-5662AF2A51CF', 7, 0, 'Ошибка перекодировки КИШ', 
  N'<!DOCTYPE html>
  <html>
	<head>
	  <meta charset="utf-8">
	  <title>Ошибка перекодировки</title>
	</head>
	<body>
	  <h1>При перекодировке (КИШ) депозитарного документа произошла ошибка «Ошибка регистрации депозитарного документа (Fansy)»</h1>
	  <br>
	  <div>При перекодировке (КИШ) файла по КД @xmlFileName в директории @xmlSourceDirectory произошла ошибка перекодировки.</div>
	  <div>В указанном выше XML файле, путь к документу определяется по Xpath выражению.
		 <br>
		 По XPath выражению "@xpath" не был найдено ни одного тега</div>
	  <br>
	  <div>@fileMoveReport</div>
	</body>
  </html>'),

--NoRecodingProcessFound
('6334A5DF-8731-474B-858C-77BC21622AF3', 8, 0, 'Ошибка перекодировки КИШ', 
  N'<!DOCTYPE html>
  <html>
	<head>
	  <meta charset="utf-8">
	  <title>Ошибка перекодировки</title>
	</head>
	<body>
	  <h1>При перекодировке (КИШ) депозитарного документа произошла ошибка «Ошибка регистрации депозитарного документа (Fansy)»</h1>
	  <br>
	  <div>Для полученного файла @xmlFileName с корневым тегом @rootNodeName, 
		полученного из папки @xmlSourceDirectory интеграционной шиной, не найдено процесса перекодировки.</div>
		<br>
	  <div>@fileMoveReport</div>
	</body>
  </html>'),

--CannotFindRecodeProcessForReceivedDocument
('1834DFAF-4EA6-4041-AF18-A57D04803F88', 15, 0, 'Ошибка перекодировки КИШ', 
  N'<!DOCTYPE html>
  <html>
	<head>
	  <meta charset="utf-8">
	  <title>Ошибка перекодировки</title>
	</head>
	<body>
	  <h1>При перекодировке (КИШ) депозитарного документа произошла ошибка «Ошибка регистрации депозитарного документа (Fansy)»</h1>
	  <br>
	  <div>Для полученного документа @documentFileName, полученного из папки @documentSourceDirectory интеграционной шиной, 
		не найдено процесса перекодировки.</div>
		<br>
	  <div>@fileMoveReport</div>
	</body>
  </html>'),

--NoValueToRecode
('65F786FF-63DF-4F8C-9A3A-F502FF809DED', 9, 0, 'Ошибка перекодировки КИШ', 
  N'<!DOCTYPE html>
  <html>
	<head>
	  <meta charset="utf-8">
	  <title>Ошибка перекодировки</title>
	</head>
	<body>
	  <h1>При перекодировке (КИШ) депозитарного документа произошла ошибка «Ошибка регистрации депозитарного документа (Fansy)»</h1>
	  <br>
	  <div>При перекодировке (КИШ) файла по КД @xmlFileName в директории @xmlSourceDirectory произошла ошибка перекодировки.</div>
	  <div>В указанном выше XML файле, для обязательного правила перекодировки с XPath выражением @xpath
		 <br>
		 был найден тег, но в нем отсутствует значение</div>
	  <br>
	  <div>@fileMoveReport</div>
	</body>
  </html>'),

--CannotParseXml
('6C027EFA-FC56-45AC-B9FC-109AFA663F58', 10, 0, 'Ошибка перекодировки КИШ', 
  N'<!DOCTYPE html>
  <html>
	<head>
	  <meta charset="utf-8">
	  <title>Ошибка перекодировки</title>
	</head>
	<body>
	  <h1>При перекодировке (КИШ) депозитарного документа произошла ошибка «Ошибка регистрации депозитарного документа (Fansy)»</h1>
	  <br>
	  <div>При перекодировке (КИШ) файла по КД @xmlFileName в директории @xmlSourceDirectory произошла ошибка перекодировки.</div>
	  <div>Не удалось прочитать файл, как XML. Убедитесь, что файл является валидным XML файлом</div>
	  <br>
	  <div>@fileMoveReport</div>
	</body>
  </html>'),

--XpathNoMathches
('6EE2DFEE-37A1-4C58-8ED0-6CE0A8FCEF1E', 14, 0, 'Ошибка перекодировки КИШ',
  N'<!DOCTYPE html>
	<html>
	  <head>
		<meta charset="utf-8">
		<title>Ошибка перекодировки</title>
	  </head>
	  <body>
		<h1>При перекодировке (КИШ) депозитарного документа произошла ошибка «Ошибка регистрации депозитарного документа (Fansy)»</h1>
		<br>
		<div>При перекодировке (КИШ) файла по КД @xmlFileName в директории @xmlSourceDirectory произошла ошибка перекодировки.</div>
		<div>В указанном выше XML файле, для обязательного правила перекодировки с XPath выражением @xpath
		   <br>
		   не был найдено ни одного тега</div>
		<br>
		<div>@fileMoveReport</div>
	  </body>
  </html>'),

--XmlFileWasMoved
('074664B2-F5BA-44C4-B717-116A4A5DCDDE', 12, 0, 'Ошибка перекодировки КИШ', 
  N'<div>
	Сопроводительный xml файл @xmlFileName был перемещен из папки 
	<br>@xmlSourceDirectory 
	<br> в папку
	<br>@xmlErrorPath
  </div>'),

--DocFileWasMoved
('9F01B414-E60E-416F-9DF7-C0EAC97F77AD', 13, 0, 'Ошибка перекодировки КИШ', 
  N'<div>
	Документ @documentFileName был перемещен из папки 
	<br>@documentSourceDirectory 
	<br> в папку
	<br>@docErrorPath
  </div>');

INSERT INTO [dbo].[EmailTemplates] 
(
  [Id],
  [TemplateType_Id],
  [IsDefault],
  [EmailSubject],
  [Template]
)
VALUES
--Success
('8BFC0F42-E485-4FFE-AEFD-DEDC03CAC90C', 11, 0, 'Поступление депозитарного документа', 
  N'<!DOCTYPE html>
  <html>
	<head>
	  <meta charset="utf-8">
	  <title>Депозитарный документ зарегистрирован в СПД и передан в папку Fansy</title>
	</head>
	<body>
	  <h1>Депозитарный документ зарегистрирован в СПД и передан в папку Fansy</h1>
	  <br>
	  <div>Поступил депозитарный документ. Номер входящего документа [номер входящего]. Документ зарегистрирован в СПД и передан в Fansy</div>
	<br>
	<br>
	  <div>Перекодировка файла @xmlFileName, полученного из папки @xmlSourceDirectory прошла успешно.</div>
	  <div>Файл перемещен в папку @xmlFileOutputLocation.</div>
	  @if (@documentSourceDirectory)
	  {
		<br>
		<div>Также, документ @documentFileName был перемещен из папки @documentSourceDirectory <br>
		в папку @documentOutputLocation
		</div>
	  }
	</body>
  </html>');


INSERT INTO [dbo].[EmailTemplateRecodingProcesses] 
([EmailTemplate_Id], [RecodingProcess_Id])
VALUES
('0401BBFF-A7D7-4A4D-A01D-3B4AA71B201C', '04434361-d804-4e4d-8743-ffeda4b65b80'),
('0401BBFF-A7D7-4A4D-A01D-3B4AA71B201C', '13a4eb2d-47d5-44d0-8fac-9c3854f2088f'),
('0401BBFF-A7D7-4A4D-A01D-3B4AA71B201C', '2954b204-ef52-45f6-9e75-ef2594008a9c'),
('0401BBFF-A7D7-4A4D-A01D-3B4AA71B201C', '34793c59-6ca2-451f-abc8-7c8ca1432451'),
('0401BBFF-A7D7-4A4D-A01D-3B4AA71B201C', '443f6912-30a4-405b-aa98-416647bcf252'),
('0401BBFF-A7D7-4A4D-A01D-3B4AA71B201C', '4858a5fc-ac50-44ec-894c-985c16120be4'),
('0401BBFF-A7D7-4A4D-A01D-3B4AA71B201C', '4deef7ef-9fe5-47cf-8511-3463759ea884'),
('0401BBFF-A7D7-4A4D-A01D-3B4AA71B201C', '53BF0F04-3B62-45CF-947B-3289F02AD660'),
('0401BBFF-A7D7-4A4D-A01D-3B4AA71B201C', '5d302b2e-3365-42df-b5e4-ba9d284cd1fd'),
('0401BBFF-A7D7-4A4D-A01D-3B4AA71B201C', '5e5447f4-00a3-4141-8955-574f48c62aac'),
('0401BBFF-A7D7-4A4D-A01D-3B4AA71B201C', '6ef901a1-588c-4166-96b7-35a1fc589bb5'),
('0401BBFF-A7D7-4A4D-A01D-3B4AA71B201C', '734f1e7e-308b-4e9d-9619-7774c227ef49'),
('0401BBFF-A7D7-4A4D-A01D-3B4AA71B201C', '74e81a34-19ea-426a-986a-a7acab21083a'),
('0401BBFF-A7D7-4A4D-A01D-3B4AA71B201C', '7a928ca2-e9d1-410a-b267-3abd40813aea'),
('0401BBFF-A7D7-4A4D-A01D-3B4AA71B201C', '986a1e55-c125-49e8-b910-2e69fdae4d82'),
('0401BBFF-A7D7-4A4D-A01D-3B4AA71B201C', '9e4d1cb7-f6ac-4c77-b68a-8e4066350d20'),
('0401BBFF-A7D7-4A4D-A01D-3B4AA71B201C', 'bdbe61c0-7073-4167-b4a8-d6451bb88757'),
('0401BBFF-A7D7-4A4D-A01D-3B4AA71B201C', 'c177351e-5294-49bd-bde4-51e6a0f06961'),
('0401BBFF-A7D7-4A4D-A01D-3B4AA71B201C', 'c54c1095-9758-451a-b192-9fd544f656fb'),
('0401BBFF-A7D7-4A4D-A01D-3B4AA71B201C', 'c81b1182-ceaf-4529-8417-7a367b3fb945'),
('0401BBFF-A7D7-4A4D-A01D-3B4AA71B201C', 'd615f054-eb93-4fda-8a1b-0f5d5f1fa955'),
('0401BBFF-A7D7-4A4D-A01D-3B4AA71B201C', 'd628fa31-6646-4b7b-8186-7d5bb1592732'),
('0401BBFF-A7D7-4A4D-A01D-3B4AA71B201C', 'e8760382-5dfa-4401-8366-7929b03c585a'),
('074664B2-F5BA-44C4-B717-116A4A5DCDDE', '04434361-d804-4e4d-8743-ffeda4b65b80'),
('074664B2-F5BA-44C4-B717-116A4A5DCDDE', '13a4eb2d-47d5-44d0-8fac-9c3854f2088f'),
('074664B2-F5BA-44C4-B717-116A4A5DCDDE', '2954b204-ef52-45f6-9e75-ef2594008a9c'),
('074664B2-F5BA-44C4-B717-116A4A5DCDDE', '34793c59-6ca2-451f-abc8-7c8ca1432451'),
('074664B2-F5BA-44C4-B717-116A4A5DCDDE', '443f6912-30a4-405b-aa98-416647bcf252'),
('074664B2-F5BA-44C4-B717-116A4A5DCDDE', '4858a5fc-ac50-44ec-894c-985c16120be4'),
('074664B2-F5BA-44C4-B717-116A4A5DCDDE', '4deef7ef-9fe5-47cf-8511-3463759ea884'),
('074664B2-F5BA-44C4-B717-116A4A5DCDDE', '53BF0F04-3B62-45CF-947B-3289F02AD660'),
('074664B2-F5BA-44C4-B717-116A4A5DCDDE', '5d302b2e-3365-42df-b5e4-ba9d284cd1fd'),
('074664B2-F5BA-44C4-B717-116A4A5DCDDE', '5e5447f4-00a3-4141-8955-574f48c62aac'),
('074664B2-F5BA-44C4-B717-116A4A5DCDDE', '6ef901a1-588c-4166-96b7-35a1fc589bb5'),
('074664B2-F5BA-44C4-B717-116A4A5DCDDE', '734f1e7e-308b-4e9d-9619-7774c227ef49'),
('074664B2-F5BA-44C4-B717-116A4A5DCDDE', '74e81a34-19ea-426a-986a-a7acab21083a'),
('074664B2-F5BA-44C4-B717-116A4A5DCDDE', '7a928ca2-e9d1-410a-b267-3abd40813aea'),
('074664B2-F5BA-44C4-B717-116A4A5DCDDE', '986a1e55-c125-49e8-b910-2e69fdae4d82'),
('074664B2-F5BA-44C4-B717-116A4A5DCDDE', '9e4d1cb7-f6ac-4c77-b68a-8e4066350d20'),
('074664B2-F5BA-44C4-B717-116A4A5DCDDE', 'bdbe61c0-7073-4167-b4a8-d6451bb88757'),
('074664B2-F5BA-44C4-B717-116A4A5DCDDE', 'c177351e-5294-49bd-bde4-51e6a0f06961'),
('074664B2-F5BA-44C4-B717-116A4A5DCDDE', 'c54c1095-9758-451a-b192-9fd544f656fb'),
('074664B2-F5BA-44C4-B717-116A4A5DCDDE', 'c81b1182-ceaf-4529-8417-7a367b3fb945'),
('074664B2-F5BA-44C4-B717-116A4A5DCDDE', 'd615f054-eb93-4fda-8a1b-0f5d5f1fa955'),
('074664B2-F5BA-44C4-B717-116A4A5DCDDE', 'd628fa31-6646-4b7b-8186-7d5bb1592732'),
('074664B2-F5BA-44C4-B717-116A4A5DCDDE', 'e8760382-5dfa-4401-8366-7929b03c585a'),
('1834DFAF-4EA6-4041-AF18-A57D04803F88', '04434361-d804-4e4d-8743-ffeda4b65b80'),
('1834DFAF-4EA6-4041-AF18-A57D04803F88', '13a4eb2d-47d5-44d0-8fac-9c3854f2088f'),
('1834DFAF-4EA6-4041-AF18-A57D04803F88', '2954b204-ef52-45f6-9e75-ef2594008a9c'),
('1834DFAF-4EA6-4041-AF18-A57D04803F88', '34793c59-6ca2-451f-abc8-7c8ca1432451'),
('1834DFAF-4EA6-4041-AF18-A57D04803F88', '443f6912-30a4-405b-aa98-416647bcf252'),
('1834DFAF-4EA6-4041-AF18-A57D04803F88', '4858a5fc-ac50-44ec-894c-985c16120be4'),
('1834DFAF-4EA6-4041-AF18-A57D04803F88', '4deef7ef-9fe5-47cf-8511-3463759ea884'),
('1834DFAF-4EA6-4041-AF18-A57D04803F88', '53BF0F04-3B62-45CF-947B-3289F02AD660'),
('1834DFAF-4EA6-4041-AF18-A57D04803F88', '5d302b2e-3365-42df-b5e4-ba9d284cd1fd'),
('1834DFAF-4EA6-4041-AF18-A57D04803F88', '5e5447f4-00a3-4141-8955-574f48c62aac'),
('1834DFAF-4EA6-4041-AF18-A57D04803F88', '6ef901a1-588c-4166-96b7-35a1fc589bb5'),
('1834DFAF-4EA6-4041-AF18-A57D04803F88', '734f1e7e-308b-4e9d-9619-7774c227ef49'),
('1834DFAF-4EA6-4041-AF18-A57D04803F88', '74e81a34-19ea-426a-986a-a7acab21083a'),
('1834DFAF-4EA6-4041-AF18-A57D04803F88', '7a928ca2-e9d1-410a-b267-3abd40813aea'),
('1834DFAF-4EA6-4041-AF18-A57D04803F88', '986a1e55-c125-49e8-b910-2e69fdae4d82'),
('1834DFAF-4EA6-4041-AF18-A57D04803F88', '9e4d1cb7-f6ac-4c77-b68a-8e4066350d20'),
('1834DFAF-4EA6-4041-AF18-A57D04803F88', 'bdbe61c0-7073-4167-b4a8-d6451bb88757'),
('1834DFAF-4EA6-4041-AF18-A57D04803F88', 'c177351e-5294-49bd-bde4-51e6a0f06961'),
('1834DFAF-4EA6-4041-AF18-A57D04803F88', 'c54c1095-9758-451a-b192-9fd544f656fb'),
('1834DFAF-4EA6-4041-AF18-A57D04803F88', 'c81b1182-ceaf-4529-8417-7a367b3fb945'),
('1834DFAF-4EA6-4041-AF18-A57D04803F88', 'd615f054-eb93-4fda-8a1b-0f5d5f1fa955'),
('1834DFAF-4EA6-4041-AF18-A57D04803F88', 'd628fa31-6646-4b7b-8186-7d5bb1592732'),
('1834DFAF-4EA6-4041-AF18-A57D04803F88', 'e8760382-5dfa-4401-8366-7929b03c585a'),
('5079E09D-5120-4029-8B10-30D2A3C53CFE', '04434361-d804-4e4d-8743-ffeda4b65b80'),
('5079E09D-5120-4029-8B10-30D2A3C53CFE', '13a4eb2d-47d5-44d0-8fac-9c3854f2088f'),
('5079E09D-5120-4029-8B10-30D2A3C53CFE', '2954b204-ef52-45f6-9e75-ef2594008a9c'),
('5079E09D-5120-4029-8B10-30D2A3C53CFE', '34793c59-6ca2-451f-abc8-7c8ca1432451'),
('5079E09D-5120-4029-8B10-30D2A3C53CFE', '443f6912-30a4-405b-aa98-416647bcf252'),
('5079E09D-5120-4029-8B10-30D2A3C53CFE', '4858a5fc-ac50-44ec-894c-985c16120be4'),
('5079E09D-5120-4029-8B10-30D2A3C53CFE', '4deef7ef-9fe5-47cf-8511-3463759ea884'),
('5079E09D-5120-4029-8B10-30D2A3C53CFE', '53BF0F04-3B62-45CF-947B-3289F02AD660'),
('5079E09D-5120-4029-8B10-30D2A3C53CFE', '5d302b2e-3365-42df-b5e4-ba9d284cd1fd'),
('5079E09D-5120-4029-8B10-30D2A3C53CFE', '5e5447f4-00a3-4141-8955-574f48c62aac'),
('5079E09D-5120-4029-8B10-30D2A3C53CFE', '6ef901a1-588c-4166-96b7-35a1fc589bb5'),
('5079E09D-5120-4029-8B10-30D2A3C53CFE', '734f1e7e-308b-4e9d-9619-7774c227ef49'),
('5079E09D-5120-4029-8B10-30D2A3C53CFE', '74e81a34-19ea-426a-986a-a7acab21083a'),
('5079E09D-5120-4029-8B10-30D2A3C53CFE', '7a928ca2-e9d1-410a-b267-3abd40813aea'),
('5079E09D-5120-4029-8B10-30D2A3C53CFE', '986a1e55-c125-49e8-b910-2e69fdae4d82'),
('5079E09D-5120-4029-8B10-30D2A3C53CFE', '9e4d1cb7-f6ac-4c77-b68a-8e4066350d20'),
('5079E09D-5120-4029-8B10-30D2A3C53CFE', 'bdbe61c0-7073-4167-b4a8-d6451bb88757'),
('5079E09D-5120-4029-8B10-30D2A3C53CFE', 'c177351e-5294-49bd-bde4-51e6a0f06961'),
('5079E09D-5120-4029-8B10-30D2A3C53CFE', 'c54c1095-9758-451a-b192-9fd544f656fb'),
('5079E09D-5120-4029-8B10-30D2A3C53CFE', 'c81b1182-ceaf-4529-8417-7a367b3fb945'),
('5079E09D-5120-4029-8B10-30D2A3C53CFE', 'd615f054-eb93-4fda-8a1b-0f5d5f1fa955'),
('5079E09D-5120-4029-8B10-30D2A3C53CFE', 'd628fa31-6646-4b7b-8186-7d5bb1592732'),
('5079E09D-5120-4029-8B10-30D2A3C53CFE', 'e8760382-5dfa-4401-8366-7929b03c585a'),
('59CAE334-9C1C-44C3-88A9-2388FA994465', '04434361-d804-4e4d-8743-ffeda4b65b80'),
('59CAE334-9C1C-44C3-88A9-2388FA994465', '13a4eb2d-47d5-44d0-8fac-9c3854f2088f'),
('59CAE334-9C1C-44C3-88A9-2388FA994465', '2954b204-ef52-45f6-9e75-ef2594008a9c'),
('59CAE334-9C1C-44C3-88A9-2388FA994465', '34793c59-6ca2-451f-abc8-7c8ca1432451'),
('59CAE334-9C1C-44C3-88A9-2388FA994465', '443f6912-30a4-405b-aa98-416647bcf252'),
('59CAE334-9C1C-44C3-88A9-2388FA994465', '4858a5fc-ac50-44ec-894c-985c16120be4'),
('59CAE334-9C1C-44C3-88A9-2388FA994465', '4deef7ef-9fe5-47cf-8511-3463759ea884'),
('59CAE334-9C1C-44C3-88A9-2388FA994465', '53BF0F04-3B62-45CF-947B-3289F02AD660'),
('59CAE334-9C1C-44C3-88A9-2388FA994465', '5d302b2e-3365-42df-b5e4-ba9d284cd1fd'),
('59CAE334-9C1C-44C3-88A9-2388FA994465', '5e5447f4-00a3-4141-8955-574f48c62aac'),
('59CAE334-9C1C-44C3-88A9-2388FA994465', '6ef901a1-588c-4166-96b7-35a1fc589bb5'),
('59CAE334-9C1C-44C3-88A9-2388FA994465', '734f1e7e-308b-4e9d-9619-7774c227ef49'),
('59CAE334-9C1C-44C3-88A9-2388FA994465', '74e81a34-19ea-426a-986a-a7acab21083a'),
('59CAE334-9C1C-44C3-88A9-2388FA994465', '7a928ca2-e9d1-410a-b267-3abd40813aea'),
('59CAE334-9C1C-44C3-88A9-2388FA994465', '986a1e55-c125-49e8-b910-2e69fdae4d82'),
('59CAE334-9C1C-44C3-88A9-2388FA994465', '9e4d1cb7-f6ac-4c77-b68a-8e4066350d20'),
('59CAE334-9C1C-44C3-88A9-2388FA994465', 'bdbe61c0-7073-4167-b4a8-d6451bb88757'),
('59CAE334-9C1C-44C3-88A9-2388FA994465', 'c177351e-5294-49bd-bde4-51e6a0f06961'),
('59CAE334-9C1C-44C3-88A9-2388FA994465', 'c54c1095-9758-451a-b192-9fd544f656fb'),
('59CAE334-9C1C-44C3-88A9-2388FA994465', 'c81b1182-ceaf-4529-8417-7a367b3fb945'),
('59CAE334-9C1C-44C3-88A9-2388FA994465', 'd615f054-eb93-4fda-8a1b-0f5d5f1fa955'),
('59CAE334-9C1C-44C3-88A9-2388FA994465', 'd628fa31-6646-4b7b-8186-7d5bb1592732'),
('59CAE334-9C1C-44C3-88A9-2388FA994465', 'e8760382-5dfa-4401-8366-7929b03c585a'),
('6334A5DF-8731-474B-858C-77BC21622AF3', '04434361-d804-4e4d-8743-ffeda4b65b80'),
('6334A5DF-8731-474B-858C-77BC21622AF3', '13a4eb2d-47d5-44d0-8fac-9c3854f2088f'),
('6334A5DF-8731-474B-858C-77BC21622AF3', '2954b204-ef52-45f6-9e75-ef2594008a9c'),
('6334A5DF-8731-474B-858C-77BC21622AF3', '34793c59-6ca2-451f-abc8-7c8ca1432451'),
('6334A5DF-8731-474B-858C-77BC21622AF3', '443f6912-30a4-405b-aa98-416647bcf252'),
('6334A5DF-8731-474B-858C-77BC21622AF3', '4858a5fc-ac50-44ec-894c-985c16120be4'),
('6334A5DF-8731-474B-858C-77BC21622AF3', '4deef7ef-9fe5-47cf-8511-3463759ea884'),
('6334A5DF-8731-474B-858C-77BC21622AF3', '53BF0F04-3B62-45CF-947B-3289F02AD660'),
('6334A5DF-8731-474B-858C-77BC21622AF3', '5d302b2e-3365-42df-b5e4-ba9d284cd1fd'),
('6334A5DF-8731-474B-858C-77BC21622AF3', '5e5447f4-00a3-4141-8955-574f48c62aac'),
('6334A5DF-8731-474B-858C-77BC21622AF3', '6ef901a1-588c-4166-96b7-35a1fc589bb5'),
('6334A5DF-8731-474B-858C-77BC21622AF3', '734f1e7e-308b-4e9d-9619-7774c227ef49'),
('6334A5DF-8731-474B-858C-77BC21622AF3', '74e81a34-19ea-426a-986a-a7acab21083a'),
('6334A5DF-8731-474B-858C-77BC21622AF3', '7a928ca2-e9d1-410a-b267-3abd40813aea'),
('6334A5DF-8731-474B-858C-77BC21622AF3', '986a1e55-c125-49e8-b910-2e69fdae4d82'),
('6334A5DF-8731-474B-858C-77BC21622AF3', '9e4d1cb7-f6ac-4c77-b68a-8e4066350d20'),
('6334A5DF-8731-474B-858C-77BC21622AF3', 'bdbe61c0-7073-4167-b4a8-d6451bb88757'),
('6334A5DF-8731-474B-858C-77BC21622AF3', 'c177351e-5294-49bd-bde4-51e6a0f06961'),
('6334A5DF-8731-474B-858C-77BC21622AF3', 'c54c1095-9758-451a-b192-9fd544f656fb'),
('6334A5DF-8731-474B-858C-77BC21622AF3', 'c81b1182-ceaf-4529-8417-7a367b3fb945'),
('6334A5DF-8731-474B-858C-77BC21622AF3', 'd615f054-eb93-4fda-8a1b-0f5d5f1fa955'),
('6334A5DF-8731-474B-858C-77BC21622AF3', 'd628fa31-6646-4b7b-8186-7d5bb1592732'),
('6334A5DF-8731-474B-858C-77BC21622AF3', 'e8760382-5dfa-4401-8366-7929b03c585a'),
('65F786FF-63DF-4F8C-9A3A-F502FF809DED', '04434361-d804-4e4d-8743-ffeda4b65b80'),
('65F786FF-63DF-4F8C-9A3A-F502FF809DED', '13a4eb2d-47d5-44d0-8fac-9c3854f2088f'),
('65F786FF-63DF-4F8C-9A3A-F502FF809DED', '2954b204-ef52-45f6-9e75-ef2594008a9c'),
('65F786FF-63DF-4F8C-9A3A-F502FF809DED', '34793c59-6ca2-451f-abc8-7c8ca1432451'),
('65F786FF-63DF-4F8C-9A3A-F502FF809DED', '443f6912-30a4-405b-aa98-416647bcf252'),
('65F786FF-63DF-4F8C-9A3A-F502FF809DED', '4858a5fc-ac50-44ec-894c-985c16120be4'),
('65F786FF-63DF-4F8C-9A3A-F502FF809DED', '4deef7ef-9fe5-47cf-8511-3463759ea884'),
('65F786FF-63DF-4F8C-9A3A-F502FF809DED', '53BF0F04-3B62-45CF-947B-3289F02AD660'),
('65F786FF-63DF-4F8C-9A3A-F502FF809DED', '5d302b2e-3365-42df-b5e4-ba9d284cd1fd'),
('65F786FF-63DF-4F8C-9A3A-F502FF809DED', '5e5447f4-00a3-4141-8955-574f48c62aac'),
('65F786FF-63DF-4F8C-9A3A-F502FF809DED', '6ef901a1-588c-4166-96b7-35a1fc589bb5'),
('65F786FF-63DF-4F8C-9A3A-F502FF809DED', '734f1e7e-308b-4e9d-9619-7774c227ef49'),
('65F786FF-63DF-4F8C-9A3A-F502FF809DED', '74e81a34-19ea-426a-986a-a7acab21083a'),
('65F786FF-63DF-4F8C-9A3A-F502FF809DED', '7a928ca2-e9d1-410a-b267-3abd40813aea'),
('65F786FF-63DF-4F8C-9A3A-F502FF809DED', '986a1e55-c125-49e8-b910-2e69fdae4d82'),
('65F786FF-63DF-4F8C-9A3A-F502FF809DED', '9e4d1cb7-f6ac-4c77-b68a-8e4066350d20'),
('65F786FF-63DF-4F8C-9A3A-F502FF809DED', 'bdbe61c0-7073-4167-b4a8-d6451bb88757'),
('65F786FF-63DF-4F8C-9A3A-F502FF809DED', 'c177351e-5294-49bd-bde4-51e6a0f06961'),
('65F786FF-63DF-4F8C-9A3A-F502FF809DED', 'c54c1095-9758-451a-b192-9fd544f656fb'),
('65F786FF-63DF-4F8C-9A3A-F502FF809DED', 'c81b1182-ceaf-4529-8417-7a367b3fb945'),
('65F786FF-63DF-4F8C-9A3A-F502FF809DED', 'd615f054-eb93-4fda-8a1b-0f5d5f1fa955'),
('65F786FF-63DF-4F8C-9A3A-F502FF809DED', 'd628fa31-6646-4b7b-8186-7d5bb1592732'),
('65F786FF-63DF-4F8C-9A3A-F502FF809DED', 'e8760382-5dfa-4401-8366-7929b03c585a'),
('6C027EFA-FC56-45AC-B9FC-109AFA663F58', '04434361-d804-4e4d-8743-ffeda4b65b80'),
('6C027EFA-FC56-45AC-B9FC-109AFA663F58', '13a4eb2d-47d5-44d0-8fac-9c3854f2088f'),
('6C027EFA-FC56-45AC-B9FC-109AFA663F58', '2954b204-ef52-45f6-9e75-ef2594008a9c'),
('6C027EFA-FC56-45AC-B9FC-109AFA663F58', '34793c59-6ca2-451f-abc8-7c8ca1432451'),
('6C027EFA-FC56-45AC-B9FC-109AFA663F58', '443f6912-30a4-405b-aa98-416647bcf252'),
('6C027EFA-FC56-45AC-B9FC-109AFA663F58', '4858a5fc-ac50-44ec-894c-985c16120be4'),
('6C027EFA-FC56-45AC-B9FC-109AFA663F58', '4deef7ef-9fe5-47cf-8511-3463759ea884'),
('6C027EFA-FC56-45AC-B9FC-109AFA663F58', '53BF0F04-3B62-45CF-947B-3289F02AD660'),
('6C027EFA-FC56-45AC-B9FC-109AFA663F58', '5d302b2e-3365-42df-b5e4-ba9d284cd1fd'),
('6C027EFA-FC56-45AC-B9FC-109AFA663F58', '5e5447f4-00a3-4141-8955-574f48c62aac'),
('6C027EFA-FC56-45AC-B9FC-109AFA663F58', '6ef901a1-588c-4166-96b7-35a1fc589bb5'),
('6C027EFA-FC56-45AC-B9FC-109AFA663F58', '734f1e7e-308b-4e9d-9619-7774c227ef49'),
('6C027EFA-FC56-45AC-B9FC-109AFA663F58', '74e81a34-19ea-426a-986a-a7acab21083a'),
('6C027EFA-FC56-45AC-B9FC-109AFA663F58', '7a928ca2-e9d1-410a-b267-3abd40813aea'),
('6C027EFA-FC56-45AC-B9FC-109AFA663F58', '986a1e55-c125-49e8-b910-2e69fdae4d82'),
('6C027EFA-FC56-45AC-B9FC-109AFA663F58', '9e4d1cb7-f6ac-4c77-b68a-8e4066350d20'),
('6C027EFA-FC56-45AC-B9FC-109AFA663F58', 'bdbe61c0-7073-4167-b4a8-d6451bb88757'),
('6C027EFA-FC56-45AC-B9FC-109AFA663F58', 'c177351e-5294-49bd-bde4-51e6a0f06961'),
('6C027EFA-FC56-45AC-B9FC-109AFA663F58', 'c54c1095-9758-451a-b192-9fd544f656fb'),
('6C027EFA-FC56-45AC-B9FC-109AFA663F58', 'c81b1182-ceaf-4529-8417-7a367b3fb945'),
('6C027EFA-FC56-45AC-B9FC-109AFA663F58', 'd615f054-eb93-4fda-8a1b-0f5d5f1fa955'),
('6C027EFA-FC56-45AC-B9FC-109AFA663F58', 'd628fa31-6646-4b7b-8186-7d5bb1592732'),
('6C027EFA-FC56-45AC-B9FC-109AFA663F58', 'e8760382-5dfa-4401-8366-7929b03c585a'),
('6EE2DFEE-37A1-4C58-8ED0-6CE0A8FCEF1E', '04434361-d804-4e4d-8743-ffeda4b65b80'),
('6EE2DFEE-37A1-4C58-8ED0-6CE0A8FCEF1E', '13a4eb2d-47d5-44d0-8fac-9c3854f2088f'),
('6EE2DFEE-37A1-4C58-8ED0-6CE0A8FCEF1E', '2954b204-ef52-45f6-9e75-ef2594008a9c'),
('6EE2DFEE-37A1-4C58-8ED0-6CE0A8FCEF1E', '34793c59-6ca2-451f-abc8-7c8ca1432451'),
('6EE2DFEE-37A1-4C58-8ED0-6CE0A8FCEF1E', '443f6912-30a4-405b-aa98-416647bcf252'),
('6EE2DFEE-37A1-4C58-8ED0-6CE0A8FCEF1E', '4858a5fc-ac50-44ec-894c-985c16120be4'),
('6EE2DFEE-37A1-4C58-8ED0-6CE0A8FCEF1E', '4deef7ef-9fe5-47cf-8511-3463759ea884'),
('6EE2DFEE-37A1-4C58-8ED0-6CE0A8FCEF1E', '53BF0F04-3B62-45CF-947B-3289F02AD660'),
('6EE2DFEE-37A1-4C58-8ED0-6CE0A8FCEF1E', '5d302b2e-3365-42df-b5e4-ba9d284cd1fd'),
('6EE2DFEE-37A1-4C58-8ED0-6CE0A8FCEF1E', '5e5447f4-00a3-4141-8955-574f48c62aac'),
('6EE2DFEE-37A1-4C58-8ED0-6CE0A8FCEF1E', '6ef901a1-588c-4166-96b7-35a1fc589bb5'),
('6EE2DFEE-37A1-4C58-8ED0-6CE0A8FCEF1E', '734f1e7e-308b-4e9d-9619-7774c227ef49'),
('6EE2DFEE-37A1-4C58-8ED0-6CE0A8FCEF1E', '74e81a34-19ea-426a-986a-a7acab21083a'),
('6EE2DFEE-37A1-4C58-8ED0-6CE0A8FCEF1E', '7a928ca2-e9d1-410a-b267-3abd40813aea'),
('6EE2DFEE-37A1-4C58-8ED0-6CE0A8FCEF1E', '986a1e55-c125-49e8-b910-2e69fdae4d82'),
('6EE2DFEE-37A1-4C58-8ED0-6CE0A8FCEF1E', '9e4d1cb7-f6ac-4c77-b68a-8e4066350d20'),
('6EE2DFEE-37A1-4C58-8ED0-6CE0A8FCEF1E', 'bdbe61c0-7073-4167-b4a8-d6451bb88757'),
('6EE2DFEE-37A1-4C58-8ED0-6CE0A8FCEF1E', 'c177351e-5294-49bd-bde4-51e6a0f06961'),
('6EE2DFEE-37A1-4C58-8ED0-6CE0A8FCEF1E', 'c54c1095-9758-451a-b192-9fd544f656fb'),
('6EE2DFEE-37A1-4C58-8ED0-6CE0A8FCEF1E', 'c81b1182-ceaf-4529-8417-7a367b3fb945'),
('6EE2DFEE-37A1-4C58-8ED0-6CE0A8FCEF1E', 'd615f054-eb93-4fda-8a1b-0f5d5f1fa955'),
('6EE2DFEE-37A1-4C58-8ED0-6CE0A8FCEF1E', 'd628fa31-6646-4b7b-8186-7d5bb1592732'),
('6EE2DFEE-37A1-4C58-8ED0-6CE0A8FCEF1E', 'e8760382-5dfa-4401-8366-7929b03c585a'),
('730BD96A-F23F-42F3-8C68-9D72F9BD378F', '04434361-d804-4e4d-8743-ffeda4b65b80'),
('730BD96A-F23F-42F3-8C68-9D72F9BD378F', '13a4eb2d-47d5-44d0-8fac-9c3854f2088f'),
('730BD96A-F23F-42F3-8C68-9D72F9BD378F', '2954b204-ef52-45f6-9e75-ef2594008a9c'),
('730BD96A-F23F-42F3-8C68-9D72F9BD378F', '34793c59-6ca2-451f-abc8-7c8ca1432451'),
('730BD96A-F23F-42F3-8C68-9D72F9BD378F', '443f6912-30a4-405b-aa98-416647bcf252'),
('730BD96A-F23F-42F3-8C68-9D72F9BD378F', '4858a5fc-ac50-44ec-894c-985c16120be4'),
('730BD96A-F23F-42F3-8C68-9D72F9BD378F', '4deef7ef-9fe5-47cf-8511-3463759ea884'),
('730BD96A-F23F-42F3-8C68-9D72F9BD378F', '53BF0F04-3B62-45CF-947B-3289F02AD660'),
('730BD96A-F23F-42F3-8C68-9D72F9BD378F', '5d302b2e-3365-42df-b5e4-ba9d284cd1fd'),
('730BD96A-F23F-42F3-8C68-9D72F9BD378F', '5e5447f4-00a3-4141-8955-574f48c62aac'),
('730BD96A-F23F-42F3-8C68-9D72F9BD378F', '6ef901a1-588c-4166-96b7-35a1fc589bb5'),
('730BD96A-F23F-42F3-8C68-9D72F9BD378F', '734f1e7e-308b-4e9d-9619-7774c227ef49'),
('730BD96A-F23F-42F3-8C68-9D72F9BD378F', '74e81a34-19ea-426a-986a-a7acab21083a'),
('730BD96A-F23F-42F3-8C68-9D72F9BD378F', '7a928ca2-e9d1-410a-b267-3abd40813aea'),
('730BD96A-F23F-42F3-8C68-9D72F9BD378F', '986a1e55-c125-49e8-b910-2e69fdae4d82'),
('730BD96A-F23F-42F3-8C68-9D72F9BD378F', '9e4d1cb7-f6ac-4c77-b68a-8e4066350d20'),
('730BD96A-F23F-42F3-8C68-9D72F9BD378F', 'bdbe61c0-7073-4167-b4a8-d6451bb88757'),
('730BD96A-F23F-42F3-8C68-9D72F9BD378F', 'c177351e-5294-49bd-bde4-51e6a0f06961'),
('730BD96A-F23F-42F3-8C68-9D72F9BD378F', 'c54c1095-9758-451a-b192-9fd544f656fb'),
('730BD96A-F23F-42F3-8C68-9D72F9BD378F', 'c81b1182-ceaf-4529-8417-7a367b3fb945'),
('730BD96A-F23F-42F3-8C68-9D72F9BD378F', 'd615f054-eb93-4fda-8a1b-0f5d5f1fa955'),
('730BD96A-F23F-42F3-8C68-9D72F9BD378F', 'd628fa31-6646-4b7b-8186-7d5bb1592732'),
('730BD96A-F23F-42F3-8C68-9D72F9BD378F', 'e8760382-5dfa-4401-8366-7929b03c585a'),
('8BFC0F42-E485-4FFE-AEFD-DEDC03CAC90C', '04434361-d804-4e4d-8743-ffeda4b65b80'),
('8BFC0F42-E485-4FFE-AEFD-DEDC03CAC90C', '13a4eb2d-47d5-44d0-8fac-9c3854f2088f'),
('8BFC0F42-E485-4FFE-AEFD-DEDC03CAC90C', '2954b204-ef52-45f6-9e75-ef2594008a9c'),
('8BFC0F42-E485-4FFE-AEFD-DEDC03CAC90C', '34793c59-6ca2-451f-abc8-7c8ca1432451'),
('8BFC0F42-E485-4FFE-AEFD-DEDC03CAC90C', '443f6912-30a4-405b-aa98-416647bcf252'),
('8BFC0F42-E485-4FFE-AEFD-DEDC03CAC90C', '4858a5fc-ac50-44ec-894c-985c16120be4'),
('8BFC0F42-E485-4FFE-AEFD-DEDC03CAC90C', '4deef7ef-9fe5-47cf-8511-3463759ea884'),
('8BFC0F42-E485-4FFE-AEFD-DEDC03CAC90C', '53BF0F04-3B62-45CF-947B-3289F02AD660'),
('8BFC0F42-E485-4FFE-AEFD-DEDC03CAC90C', '5d302b2e-3365-42df-b5e4-ba9d284cd1fd'),
('8BFC0F42-E485-4FFE-AEFD-DEDC03CAC90C', '5e5447f4-00a3-4141-8955-574f48c62aac'),
('8BFC0F42-E485-4FFE-AEFD-DEDC03CAC90C', '6ef901a1-588c-4166-96b7-35a1fc589bb5'),
('8BFC0F42-E485-4FFE-AEFD-DEDC03CAC90C', '734f1e7e-308b-4e9d-9619-7774c227ef49'),
('8BFC0F42-E485-4FFE-AEFD-DEDC03CAC90C', '74e81a34-19ea-426a-986a-a7acab21083a'),
('8BFC0F42-E485-4FFE-AEFD-DEDC03CAC90C', '7a928ca2-e9d1-410a-b267-3abd40813aea'),
('8BFC0F42-E485-4FFE-AEFD-DEDC03CAC90C', '986a1e55-c125-49e8-b910-2e69fdae4d82'),
('8BFC0F42-E485-4FFE-AEFD-DEDC03CAC90C', '9e4d1cb7-f6ac-4c77-b68a-8e4066350d20'),
('8BFC0F42-E485-4FFE-AEFD-DEDC03CAC90C', 'bdbe61c0-7073-4167-b4a8-d6451bb88757'),
('8BFC0F42-E485-4FFE-AEFD-DEDC03CAC90C', 'c177351e-5294-49bd-bde4-51e6a0f06961'),
('8BFC0F42-E485-4FFE-AEFD-DEDC03CAC90C', 'c54c1095-9758-451a-b192-9fd544f656fb'),
('8BFC0F42-E485-4FFE-AEFD-DEDC03CAC90C', 'c81b1182-ceaf-4529-8417-7a367b3fb945'),
('8BFC0F42-E485-4FFE-AEFD-DEDC03CAC90C', 'd615f054-eb93-4fda-8a1b-0f5d5f1fa955'),
('8BFC0F42-E485-4FFE-AEFD-DEDC03CAC90C', 'd628fa31-6646-4b7b-8186-7d5bb1592732'),
('8BFC0F42-E485-4FFE-AEFD-DEDC03CAC90C', 'e8760382-5dfa-4401-8366-7929b03c585a'),
('9F01B414-E60E-416F-9DF7-C0EAC97F77AD', '04434361-d804-4e4d-8743-ffeda4b65b80'),
('9F01B414-E60E-416F-9DF7-C0EAC97F77AD', '13a4eb2d-47d5-44d0-8fac-9c3854f2088f'),
('9F01B414-E60E-416F-9DF7-C0EAC97F77AD', '2954b204-ef52-45f6-9e75-ef2594008a9c'),
('9F01B414-E60E-416F-9DF7-C0EAC97F77AD', '34793c59-6ca2-451f-abc8-7c8ca1432451'),
('9F01B414-E60E-416F-9DF7-C0EAC97F77AD', '443f6912-30a4-405b-aa98-416647bcf252'),
('9F01B414-E60E-416F-9DF7-C0EAC97F77AD', '4858a5fc-ac50-44ec-894c-985c16120be4'),
('9F01B414-E60E-416F-9DF7-C0EAC97F77AD', '4deef7ef-9fe5-47cf-8511-3463759ea884'),
('9F01B414-E60E-416F-9DF7-C0EAC97F77AD', '53BF0F04-3B62-45CF-947B-3289F02AD660'),
('9F01B414-E60E-416F-9DF7-C0EAC97F77AD', '5d302b2e-3365-42df-b5e4-ba9d284cd1fd'),
('9F01B414-E60E-416F-9DF7-C0EAC97F77AD', '5e5447f4-00a3-4141-8955-574f48c62aac'),
('9F01B414-E60E-416F-9DF7-C0EAC97F77AD', '6ef901a1-588c-4166-96b7-35a1fc589bb5'),
('9F01B414-E60E-416F-9DF7-C0EAC97F77AD', '734f1e7e-308b-4e9d-9619-7774c227ef49'),
('9F01B414-E60E-416F-9DF7-C0EAC97F77AD', '74e81a34-19ea-426a-986a-a7acab21083a'),
('9F01B414-E60E-416F-9DF7-C0EAC97F77AD', '7a928ca2-e9d1-410a-b267-3abd40813aea'),
('9F01B414-E60E-416F-9DF7-C0EAC97F77AD', '986a1e55-c125-49e8-b910-2e69fdae4d82'),
('9F01B414-E60E-416F-9DF7-C0EAC97F77AD', '9e4d1cb7-f6ac-4c77-b68a-8e4066350d20'),
('9F01B414-E60E-416F-9DF7-C0EAC97F77AD', 'bdbe61c0-7073-4167-b4a8-d6451bb88757'),
('9F01B414-E60E-416F-9DF7-C0EAC97F77AD', 'c177351e-5294-49bd-bde4-51e6a0f06961'),
('9F01B414-E60E-416F-9DF7-C0EAC97F77AD', 'c54c1095-9758-451a-b192-9fd544f656fb'),
('9F01B414-E60E-416F-9DF7-C0EAC97F77AD', 'c81b1182-ceaf-4529-8417-7a367b3fb945'),
('9F01B414-E60E-416F-9DF7-C0EAC97F77AD', 'd615f054-eb93-4fda-8a1b-0f5d5f1fa955'),
('9F01B414-E60E-416F-9DF7-C0EAC97F77AD', 'd628fa31-6646-4b7b-8186-7d5bb1592732'),
('9F01B414-E60E-416F-9DF7-C0EAC97F77AD', 'e8760382-5dfa-4401-8366-7929b03c585a'),
('B0CDE795-430D-4744-BE97-5662AF2A51CF', '04434361-d804-4e4d-8743-ffeda4b65b80'),
('B0CDE795-430D-4744-BE97-5662AF2A51CF', '13a4eb2d-47d5-44d0-8fac-9c3854f2088f'),
('B0CDE795-430D-4744-BE97-5662AF2A51CF', '2954b204-ef52-45f6-9e75-ef2594008a9c'),
('B0CDE795-430D-4744-BE97-5662AF2A51CF', '34793c59-6ca2-451f-abc8-7c8ca1432451'),
('B0CDE795-430D-4744-BE97-5662AF2A51CF', '443f6912-30a4-405b-aa98-416647bcf252'),
('B0CDE795-430D-4744-BE97-5662AF2A51CF', '4858a5fc-ac50-44ec-894c-985c16120be4'),
('B0CDE795-430D-4744-BE97-5662AF2A51CF', '4deef7ef-9fe5-47cf-8511-3463759ea884'),
('B0CDE795-430D-4744-BE97-5662AF2A51CF', '53BF0F04-3B62-45CF-947B-3289F02AD660'),
('B0CDE795-430D-4744-BE97-5662AF2A51CF', '5d302b2e-3365-42df-b5e4-ba9d284cd1fd'),
('B0CDE795-430D-4744-BE97-5662AF2A51CF', '5e5447f4-00a3-4141-8955-574f48c62aac'),
('B0CDE795-430D-4744-BE97-5662AF2A51CF', '6ef901a1-588c-4166-96b7-35a1fc589bb5'),
('B0CDE795-430D-4744-BE97-5662AF2A51CF', '734f1e7e-308b-4e9d-9619-7774c227ef49'),
('B0CDE795-430D-4744-BE97-5662AF2A51CF', '74e81a34-19ea-426a-986a-a7acab21083a'),
('B0CDE795-430D-4744-BE97-5662AF2A51CF', '7a928ca2-e9d1-410a-b267-3abd40813aea'),
('B0CDE795-430D-4744-BE97-5662AF2A51CF', '986a1e55-c125-49e8-b910-2e69fdae4d82'),
('B0CDE795-430D-4744-BE97-5662AF2A51CF', '9e4d1cb7-f6ac-4c77-b68a-8e4066350d20'),
('B0CDE795-430D-4744-BE97-5662AF2A51CF', 'bdbe61c0-7073-4167-b4a8-d6451bb88757'),
('B0CDE795-430D-4744-BE97-5662AF2A51CF', 'c177351e-5294-49bd-bde4-51e6a0f06961'),
('B0CDE795-430D-4744-BE97-5662AF2A51CF', 'c54c1095-9758-451a-b192-9fd544f656fb'),
('B0CDE795-430D-4744-BE97-5662AF2A51CF', 'c81b1182-ceaf-4529-8417-7a367b3fb945'),
('B0CDE795-430D-4744-BE97-5662AF2A51CF', 'd615f054-eb93-4fda-8a1b-0f5d5f1fa955'),
('B0CDE795-430D-4744-BE97-5662AF2A51CF', 'd628fa31-6646-4b7b-8186-7d5bb1592732'),
('B0CDE795-430D-4744-BE97-5662AF2A51CF', 'e8760382-5dfa-4401-8366-7929b03c585a'),
('C24B3942-D503-4DDC-B9D2-23024CC4CA7E', '04434361-d804-4e4d-8743-ffeda4b65b80'),
('C24B3942-D503-4DDC-B9D2-23024CC4CA7E', '13a4eb2d-47d5-44d0-8fac-9c3854f2088f'),
('C24B3942-D503-4DDC-B9D2-23024CC4CA7E', '2954b204-ef52-45f6-9e75-ef2594008a9c'),
('C24B3942-D503-4DDC-B9D2-23024CC4CA7E', '34793c59-6ca2-451f-abc8-7c8ca1432451'),
('C24B3942-D503-4DDC-B9D2-23024CC4CA7E', '443f6912-30a4-405b-aa98-416647bcf252'),
('C24B3942-D503-4DDC-B9D2-23024CC4CA7E', '4858a5fc-ac50-44ec-894c-985c16120be4'),
('C24B3942-D503-4DDC-B9D2-23024CC4CA7E', '4deef7ef-9fe5-47cf-8511-3463759ea884'),
('C24B3942-D503-4DDC-B9D2-23024CC4CA7E', '53BF0F04-3B62-45CF-947B-3289F02AD660'),
('C24B3942-D503-4DDC-B9D2-23024CC4CA7E', '5d302b2e-3365-42df-b5e4-ba9d284cd1fd'),
('C24B3942-D503-4DDC-B9D2-23024CC4CA7E', '5e5447f4-00a3-4141-8955-574f48c62aac'),
('C24B3942-D503-4DDC-B9D2-23024CC4CA7E', '6ef901a1-588c-4166-96b7-35a1fc589bb5'),
('C24B3942-D503-4DDC-B9D2-23024CC4CA7E', '734f1e7e-308b-4e9d-9619-7774c227ef49'),
('C24B3942-D503-4DDC-B9D2-23024CC4CA7E', '74e81a34-19ea-426a-986a-a7acab21083a'),
('C24B3942-D503-4DDC-B9D2-23024CC4CA7E', '7a928ca2-e9d1-410a-b267-3abd40813aea'),
('C24B3942-D503-4DDC-B9D2-23024CC4CA7E', '986a1e55-c125-49e8-b910-2e69fdae4d82'),
('C24B3942-D503-4DDC-B9D2-23024CC4CA7E', '9e4d1cb7-f6ac-4c77-b68a-8e4066350d20'),
('C24B3942-D503-4DDC-B9D2-23024CC4CA7E', 'bdbe61c0-7073-4167-b4a8-d6451bb88757'),
('C24B3942-D503-4DDC-B9D2-23024CC4CA7E', 'c177351e-5294-49bd-bde4-51e6a0f06961'),
('C24B3942-D503-4DDC-B9D2-23024CC4CA7E', 'c54c1095-9758-451a-b192-9fd544f656fb'),
('C24B3942-D503-4DDC-B9D2-23024CC4CA7E', 'c81b1182-ceaf-4529-8417-7a367b3fb945'),
('C24B3942-D503-4DDC-B9D2-23024CC4CA7E', 'd615f054-eb93-4fda-8a1b-0f5d5f1fa955'),
('C24B3942-D503-4DDC-B9D2-23024CC4CA7E', 'd628fa31-6646-4b7b-8186-7d5bb1592732'),
('C24B3942-D503-4DDC-B9D2-23024CC4CA7E', 'e8760382-5dfa-4401-8366-7929b03c585a'),
('DF87BC4B-99C9-4472-A843-4A5CD6DA0624', '04434361-d804-4e4d-8743-ffeda4b65b80'),
('DF87BC4B-99C9-4472-A843-4A5CD6DA0624', '13a4eb2d-47d5-44d0-8fac-9c3854f2088f'),
('DF87BC4B-99C9-4472-A843-4A5CD6DA0624', '2954b204-ef52-45f6-9e75-ef2594008a9c'),
('DF87BC4B-99C9-4472-A843-4A5CD6DA0624', '34793c59-6ca2-451f-abc8-7c8ca1432451'),
('DF87BC4B-99C9-4472-A843-4A5CD6DA0624', '443f6912-30a4-405b-aa98-416647bcf252'),
('DF87BC4B-99C9-4472-A843-4A5CD6DA0624', '4858a5fc-ac50-44ec-894c-985c16120be4'),
('DF87BC4B-99C9-4472-A843-4A5CD6DA0624', '4deef7ef-9fe5-47cf-8511-3463759ea884'),
('DF87BC4B-99C9-4472-A843-4A5CD6DA0624', '53BF0F04-3B62-45CF-947B-3289F02AD660'),
('DF87BC4B-99C9-4472-A843-4A5CD6DA0624', '5d302b2e-3365-42df-b5e4-ba9d284cd1fd'),
('DF87BC4B-99C9-4472-A843-4A5CD6DA0624', '5e5447f4-00a3-4141-8955-574f48c62aac'),
('DF87BC4B-99C9-4472-A843-4A5CD6DA0624', '6ef901a1-588c-4166-96b7-35a1fc589bb5'),
('DF87BC4B-99C9-4472-A843-4A5CD6DA0624', '734f1e7e-308b-4e9d-9619-7774c227ef49'),
('DF87BC4B-99C9-4472-A843-4A5CD6DA0624', '74e81a34-19ea-426a-986a-a7acab21083a'),
('DF87BC4B-99C9-4472-A843-4A5CD6DA0624', '7a928ca2-e9d1-410a-b267-3abd40813aea'),
('DF87BC4B-99C9-4472-A843-4A5CD6DA0624', '986a1e55-c125-49e8-b910-2e69fdae4d82'),
('DF87BC4B-99C9-4472-A843-4A5CD6DA0624', '9e4d1cb7-f6ac-4c77-b68a-8e4066350d20'),
('DF87BC4B-99C9-4472-A843-4A5CD6DA0624', 'bdbe61c0-7073-4167-b4a8-d6451bb88757'),
('DF87BC4B-99C9-4472-A843-4A5CD6DA0624', 'c177351e-5294-49bd-bde4-51e6a0f06961'),
('DF87BC4B-99C9-4472-A843-4A5CD6DA0624', 'c54c1095-9758-451a-b192-9fd544f656fb'),
('DF87BC4B-99C9-4472-A843-4A5CD6DA0624', 'c81b1182-ceaf-4529-8417-7a367b3fb945'),
('DF87BC4B-99C9-4472-A843-4A5CD6DA0624', 'd615f054-eb93-4fda-8a1b-0f5d5f1fa955'),
('DF87BC4B-99C9-4472-A843-4A5CD6DA0624', 'd628fa31-6646-4b7b-8186-7d5bb1592732'),
('DF87BC4B-99C9-4472-A843-4A5CD6DA0624', 'e8760382-5dfa-4401-8366-7929b03c585a'),
('F5C3B8DC-2EE6-4D21-87ED-D0228C5339EE', '04434361-d804-4e4d-8743-ffeda4b65b80'),
('F5C3B8DC-2EE6-4D21-87ED-D0228C5339EE', '13a4eb2d-47d5-44d0-8fac-9c3854f2088f'),
('F5C3B8DC-2EE6-4D21-87ED-D0228C5339EE', '2954b204-ef52-45f6-9e75-ef2594008a9c'),
('F5C3B8DC-2EE6-4D21-87ED-D0228C5339EE', '34793c59-6ca2-451f-abc8-7c8ca1432451'),
('F5C3B8DC-2EE6-4D21-87ED-D0228C5339EE', '443f6912-30a4-405b-aa98-416647bcf252'),
('F5C3B8DC-2EE6-4D21-87ED-D0228C5339EE', '4858a5fc-ac50-44ec-894c-985c16120be4'),
('F5C3B8DC-2EE6-4D21-87ED-D0228C5339EE', '4deef7ef-9fe5-47cf-8511-3463759ea884'),
('F5C3B8DC-2EE6-4D21-87ED-D0228C5339EE', '53BF0F04-3B62-45CF-947B-3289F02AD660'),
('F5C3B8DC-2EE6-4D21-87ED-D0228C5339EE', '5d302b2e-3365-42df-b5e4-ba9d284cd1fd'),
('F5C3B8DC-2EE6-4D21-87ED-D0228C5339EE', '5e5447f4-00a3-4141-8955-574f48c62aac'),
('F5C3B8DC-2EE6-4D21-87ED-D0228C5339EE', '6ef901a1-588c-4166-96b7-35a1fc589bb5'),
('F5C3B8DC-2EE6-4D21-87ED-D0228C5339EE', '734f1e7e-308b-4e9d-9619-7774c227ef49'),
('F5C3B8DC-2EE6-4D21-87ED-D0228C5339EE', '74e81a34-19ea-426a-986a-a7acab21083a'),
('F5C3B8DC-2EE6-4D21-87ED-D0228C5339EE', '7a928ca2-e9d1-410a-b267-3abd40813aea'),
('F5C3B8DC-2EE6-4D21-87ED-D0228C5339EE', '986a1e55-c125-49e8-b910-2e69fdae4d82'),
('F5C3B8DC-2EE6-4D21-87ED-D0228C5339EE', '9e4d1cb7-f6ac-4c77-b68a-8e4066350d20'),
('F5C3B8DC-2EE6-4D21-87ED-D0228C5339EE', 'bdbe61c0-7073-4167-b4a8-d6451bb88757'),
('F5C3B8DC-2EE6-4D21-87ED-D0228C5339EE', 'c177351e-5294-49bd-bde4-51e6a0f06961'),
('F5C3B8DC-2EE6-4D21-87ED-D0228C5339EE', 'c54c1095-9758-451a-b192-9fd544f656fb'),
('F5C3B8DC-2EE6-4D21-87ED-D0228C5339EE', 'c81b1182-ceaf-4529-8417-7a367b3fb945'),
('F5C3B8DC-2EE6-4D21-87ED-D0228C5339EE', 'd615f054-eb93-4fda-8a1b-0f5d5f1fa955'),
('F5C3B8DC-2EE6-4D21-87ED-D0228C5339EE', 'd628fa31-6646-4b7b-8186-7d5bb1592732'),
('F5C3B8DC-2EE6-4D21-87ED-D0228C5339EE', 'e8760382-5dfa-4401-8366-7929b03c585a');