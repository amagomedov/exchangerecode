namespace ExchangeRecode.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class XpathId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RecodingProcesses", "Xpath", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.RecodingProcesses", "Xpath");
        }
    }
}
