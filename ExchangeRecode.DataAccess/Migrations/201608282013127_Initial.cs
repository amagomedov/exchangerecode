namespace ExchangeRecode.DataAccess.Migrations
{
	using System;
	using System.Data.Entity.Migrations;
	
	public partial class Initial : DbMigration
	{
		public override void Up()
		{
			CreateTable(
				"dbo.Conditions",
				c => new
					{
						Id = c.Guid(nullable: false),
						Xpath = c.String(),
						Rule_Id = c.Guid(),
					})
				.PrimaryKey(t => t.Id)
				.ForeignKey("dbo.RecodingRules", t => t.Rule_Id)
				.Index(t => t.Rule_Id);
			
			CreateTable(
				"dbo.RecodingRules",
				c => new
					{
						Id = c.Guid(nullable: false),
						SourceSystemName = c.String(),
						DestinationSystemName = c.String(),
						EntityName = c.String(),
						IsRequired = c.Boolean(nullable: false),
						XPath = c.String(),
						SetToValue = c.String(),
						Process_Id = c.Guid(),
					})
				.PrimaryKey(t => t.Id)
				.ForeignKey("dbo.RecodingProcesses", t => t.Process_Id)
				.Index(t => t.Process_Id);
			
			CreateTable(
				"dbo.RecodingProcesses",
				c => new
					{
						Id = c.Guid(nullable: false),
						InputFolderName = c.String(),
						OutputFolderName = c.String(),
						ErrorFolder = c.String(),
						RootNodeName = c.String(),
						MailAddress = c.String(),
						MailSubject = c.String(),
						MainDocumentInputFileDirectory = c.String(),
						MainDocumentIsRequired = c.Boolean(nullable: false),
						MainDocumentInputXPath = c.String(),
						MainDocumentOutputFileDirectory = c.String(),
						MainDocumentOutputXPath = c.String(),
						MainDocumentErrorOutputFileDirectory = c.String(),
						MainDocumentErrorOutputXPath = c.String(),
						MailOnSuccess = c.Boolean(nullable: false),
					})
				.PrimaryKey(t => t.Id);
			
			CreateTable(
				"dbo.EmailTemplates",
				c => new
					{
						Id = c.Guid(nullable: false),
						Template = c.String(),
						EmailSubject = c.String(),
						IsDefault = c.Boolean(nullable: false),
						TemplateType_Id = c.Int(),
					})
				.PrimaryKey(t => t.Id)
				.ForeignKey("dbo.TemplateTypes", t => t.TemplateType_Id)
				.Index(t => t.TemplateType_Id);
			
			CreateTable(
				"dbo.TemplateTypes",
				c => new
					{
						Id = c.Int(nullable: false, identity: true),
						Name = c.String(),
					})
				.PrimaryKey(t => t.Id);
			
			CreateTable(
				"dbo.Settings",
				c => new
					{
						Name = c.String(nullable: false, maxLength: 256),
						Value = c.String(),
					})
				.PrimaryKey(t => t.Name);
			
			CreateTable(
				"dbo.EmailTemplateRecodingProcesses",
				c => new
					{
						EmailTemplate_Id = c.Guid(nullable: false),
						RecodingProcess_Id = c.Guid(nullable: false),
					})
				.PrimaryKey(t => new { t.EmailTemplate_Id, t.RecodingProcess_Id })
				.ForeignKey("dbo.EmailTemplates", t => t.EmailTemplate_Id, cascadeDelete: true)
				.ForeignKey("dbo.RecodingProcesses", t => t.RecodingProcess_Id, cascadeDelete: true)
				.Index(t => t.EmailTemplate_Id)
				.Index(t => t.RecodingProcess_Id);

			Sql(@"SET IDENTITY_INSERT TemplateTypes ON;
				INSERT INTO [dbo].[TemplateTypes] 
				(Id, Name) VALUES 
				(0,  'XmlOutputDirectoryNotFound'),
				(1,  'DocOutputDirectoryNotFound'),
				(2,  'XmlOutputDirectoryUnauthorizedAccess'),
				(3,  'DocOutputDirectoryUnauthorizedAccess'),
				(4,  'DocumentNotFound'),
				(5,  'GeneralException'),
				(6,  'MappingException'),
				(7,  'MDXpathNoMathchesFound'),
				(8,  'NoRecodingProcessFound'),
				(9,  'NoValueToRecode'),
				(10, 'CannotParseXml'),
				(11, 'Success'),
				(12, 'XmlFileWasMoved'),
				(13, 'DocFileWasMoved'),
				(14, 'XpathNoMathches'),
				(15, 'CannotFindRecodeProcessForReceivedDocument'),
				(16,  'DocumentOutputPathNotSpecified');
				SET IDENTITY_INSERT TemplateTypes  OFF;");
		}
		
		public override void Down()
		{
			DropForeignKey("dbo.RecodingRules", "Process_Id", "dbo.RecodingProcesses");
			DropForeignKey("dbo.EmailTemplates", "TemplateType_Id", "dbo.TemplateTypes");
			DropForeignKey("dbo.EmailTemplateRecodingProcesses", "RecodingProcess_Id", "dbo.RecodingProcesses");
			DropForeignKey("dbo.EmailTemplateRecodingProcesses", "EmailTemplate_Id", "dbo.EmailTemplates");
			DropForeignKey("dbo.Conditions", "Rule_Id", "dbo.RecodingRules");
			DropIndex("dbo.RecodingRules", new[] { "Process_Id" });
			DropIndex("dbo.EmailTemplates", new[] { "TemplateType_Id" });
			DropIndex("dbo.EmailTemplateRecodingProcesses", new[] { "RecodingProcess_Id" });
			DropIndex("dbo.EmailTemplateRecodingProcesses", new[] { "EmailTemplate_Id" });
			DropIndex("dbo.Conditions", new[] { "Rule_Id" });
			DropTable("dbo.EmailTemplateRecodingProcesses");
			DropTable("dbo.Settings");
			DropTable("dbo.TemplateTypes");
			DropTable("dbo.EmailTemplates");
			DropTable("dbo.RecodingProcesses");
			DropTable("dbo.RecodingRules");
			DropTable("dbo.Conditions");
		}
	}
}
