﻿using ExchangeRecode.DataAccess.SimpleHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeRecode.DataAccess.ViewModels
{
	public class Document
	{
		public string OriginalInputPath { get; set; }
		public string CurrentInputPath { get; set; }
		public string OutputPath { get; set; }
		public string ErrorPath { get; set; }
		public string FileName { get; set; }

		public Encoding Encoding {
			get { 
				return FileEncoding.DetectFileEncoding(CurrentInputPath); 
			} 
		}

	}
}
