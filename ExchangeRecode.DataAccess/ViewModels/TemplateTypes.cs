﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExchangeRecode
{
	public enum TemplateTypes
	{
		XmlOutputDirectoryNotFound,
		DocOutputDirectoryNotFound,
		XmlOutputDirectoryUnauthorizedAccess,
		DocOutputDirectoryUnauthorizedAccess,
		DocumentNotFound,
		GeneralException,
		MappingException,
		MDXpathNoMathchesFound,
		NoRecodingProcessFound,
		NoValueToRecode,
		CannotParseXml,
		Success,
		XmlFileWasMoved,
		DocFileWasMoved,
		XpathNoMathches,
		CannotFindRecodeProcessForReceivedDocument,
		XpathToDocumentNoMathchesFound,
		DocumentOutputPathNotSpecified
	}
}