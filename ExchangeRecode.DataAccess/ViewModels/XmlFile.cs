﻿using ExchangeRecode.DataAccess.SimpleHelpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ExchangeRecode.DataAccess.ViewModels
{
	public class XmlFile
	{
		public string OriginalInputPath { get; set; }
		public string CurrentInputPath { get; set; }
		public string OutputPath { get; set; }
		public string ErrorPath { get; set; }
		public string FileName { get; set; }
		public XmlDocument Xml { get; set; }
		public Encoding Encoding { get; set; }

		public XmlFile()
		{

		}

		public XmlFile(RecodingProcess recodProcess, string originalPath, string currentPath, XmlDocument xml)
		{
			FileName = Path.GetFileName(originalPath);
			OriginalInputPath = originalPath;
			CurrentInputPath = currentPath;
			OutputPath = Path.Combine(recodProcess.OutputFolderName, FileName);
			ErrorPath = Path.Combine(recodProcess.ErrorFolder, FileName);

			Xml = xml;
			Encoding = FileEncoding.DetectFileEncoding(currentPath);
		}
	}
}
