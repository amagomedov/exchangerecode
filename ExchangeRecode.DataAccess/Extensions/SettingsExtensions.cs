﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeRecode.DataAccess
{
    public static class SettingsExtensions
    {
        public static string DefaultEmailAddress(this DbSet<Settings> settings)
        {
            var emailAddr = settings.FirstOrDefault(s => s.Name == "DefaultEmailAddress");

            return emailAddr != null ? emailAddr.Value : null;
        }

        public static string DefaultErrorFolder(this DbSet<Settings> settings)
        {
            var emailFolder = settings.FirstOrDefault(s => s.Name == "DefaultErrorFolder");

            return emailFolder != null ? emailFolder.Value : null;
        }
    }
}
