﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeRecode.DataAccess
{
	public class ExchangeRecodeDbContext : DbContext
	{
		public ExchangeRecodeDbContext(): base("RecodeRulesDb")	{}

		public DbSet<RecodingProcess> RecodingProcesses { get; set; }
		public DbSet<RecodingRule> RecodingRules { get; set; }
		public DbSet<Condition> Conditions { get; set; }
		public DbSet<TemplateType> TemplateTypes { get; set; }
		public DbSet<EmailTemplate> EmailTemplates { get; set; }
		public DbSet<Settings> Settings { get; set; }
		
	}
}
