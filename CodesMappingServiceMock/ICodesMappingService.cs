﻿using System.Collections.Generic;
using System.ServiceModel;

namespace CodesMappingServiceMock
{
    [ServiceContract(Namespace = "http://navicongroup.ru/mdm/CodesMapping")]
    public interface ICodesMappingService
    {
        /// <summary>
        /// Get Mapping by entity's name, namse of source and destination systems and element's code in source system
        /// </summary>
        /// <param name="entityName">Entity's name</param>
        /// <param name="sourceName">Name of source system</param>
        /// <param name="sourceCode">Element's code in source system</param>
        /// <param name="destName">Name of destination system</param>
        /// <returns>Code's mapping</returns>
        [OperationContract]
        List<CodesMappingContract> GetMapping(string entityName, string sourceName, string sourceCode, string destName);
    }
}