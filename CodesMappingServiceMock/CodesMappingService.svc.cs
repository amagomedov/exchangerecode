﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace CodesMappingServiceMock
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
	// NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
	public class CodesMappingService : ICodesMappingService
	{

		public List<CodesMappingContract> GetMapping(string entityName, string sourceName, string sourceCode, string destName)
		{
			if (sourceCode == null || sourceCode == "recodedCode!")
			{
				throw new Exception("Cannot recode :(");
			}

			if (sourceCode == "empty")
			{
				return new List<CodesMappingContract>();
			}
			//return null;
			return new List<CodesMappingContract> {
				new CodesMappingContract() {DestCode = "recodedCode!"}
			};
		}
	}
}
