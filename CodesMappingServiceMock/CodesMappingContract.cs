﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CodesMappingServiceMock
{
    /// <summary>
    /// Class for mapping element's code between source and destination systems
    /// </summary>
    [DataContract(Namespace = "http://navicongroup.ru/mdm/CodesMapping")]
    [Serializable]
    public class CodesMappingContract
    {
        /// <summary>
        /// Name of entity
        /// </summary>
        [DataMember]
        public string EntityName { get; set; }

        /// <summary>
        /// Name of source system
        /// </summary>
        [DataMember]
        public string SourceName { get; set; }

        /// <summary>
        /// Element's code in source system
        /// </summary>
        [DataMember]
        public string SourceCode { get; set; }

        /// <summary>
        /// Name of destination system
        /// </summary>
        [DataMember]
        public string DestName { get; set; }

        /// <summary>
        /// Element's code in destination system
        /// </summary>
        [DataMember]
        public string DestCode { get; set; }


        public override int GetHashCode()
        {
            string sData = DestCode + DestName + EntityName + SourceCode + SourceName;
            return sData.GetHashCode();
        }

        public override string ToString()
        {
            string sValue = string.Format("{0} - {1}:{2}; {3}:{4}", EntityName, SourceName, SourceCode, DestName, DestCode);
            return sValue;
        }
    }

    public class CodesMappingContractComparer : IEqualityComparer<CodesMappingContract>
    {
        #region IEqualityComparer<CodesMappingContract> Members

        public bool Equals(CodesMappingContract x, CodesMappingContract y)
        {
            //Check whether the compared objects reference the same data.
            if (ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
                return false;

            return x.EntityName == y.EntityName && x.SourceName == y.SourceName && x.SourceCode == y.SourceCode
                   && x.DestName == y.DestName && x.DestCode == y.DestCode;
        }

        public int GetHashCode(CodesMappingContract obj)
        {
            //Check whether the object is null
            if (ReferenceEquals(obj, null)) return 0;

            int hashEntityName = obj.EntityName == null ? 0 : obj.EntityName.GetHashCode();
            int hashSourceName = obj.SourceName == null ? 0 : obj.SourceName.GetHashCode();
            int hashSourceCode = obj.SourceCode == null ? 0 : obj.SourceCode.GetHashCode();
            int hashDestName = obj.DestName == null ? 0 : obj.DestName.GetHashCode();
            int hashDestCode = obj.DestCode == null ? 0 : obj.DestCode.GetHashCode();

            return hashEntityName ^ hashSourceName ^ hashSourceCode ^ hashDestName ^ hashDestCode;
        }

        #endregion
    }
}