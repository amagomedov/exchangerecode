namespace ExchangeRecode {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [Schema(@"http://navicongroup.ru/mdm",@"SendMessage")]
    [Microsoft.XLANGs.BaseTypes.DistinguishedFieldAttribute(typeof(System.String), "message", XPath = @"/*[local-name()='SendMessage' and namespace-uri()='http://navicongroup.ru/mdm']/*[local-name()='message' and namespace-uri()='http://navicongroup.ru/mdm']", XsdType = @"string")]
    [Microsoft.XLANGs.BaseTypes.DistinguishedFieldAttribute(typeof(System.String), "subject", XPath = @"/*[local-name()='SendMessage' and namespace-uri()='http://navicongroup.ru/mdm']/*[local-name()='subject' and namespace-uri()='http://navicongroup.ru/mdm']", XsdType = @"string")]
    [Microsoft.XLANGs.BaseTypes.DistinguishedFieldAttribute(typeof(System.String), "mailTo", XPath = @"/*[local-name()='SendMessage' and namespace-uri()='http://navicongroup.ru/mdm']/*[local-name()='mailTo' and namespace-uri()='http://navicongroup.ru/mdm']", XsdType = @"string")]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"SendMessage"})]
    public sealed class CustomMailing : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xs:schema xmlns=""http://navicongroup.ru/mdm"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" attributeFormDefault=""qualified"" elementFormDefault=""qualified"" targetNamespace=""http://navicongroup.ru/mdm"" xmlns:xs=""http://www.w3.org/2001/XMLSchema"">
  <xs:element name=""SendMessage"">
    <xs:annotation>
      <xs:appinfo>
        <b:properties>
          <b:property distinguished=""true"" xpath=""/*[local-name()='SendMessage' and namespace-uri()='http://navicongroup.ru/mdm']/*[local-name()='message' and namespace-uri()='http://navicongroup.ru/mdm']"" />
          <b:property distinguished=""true"" xpath=""/*[local-name()='SendMessage' and namespace-uri()='http://navicongroup.ru/mdm']/*[local-name()='subject' and namespace-uri()='http://navicongroup.ru/mdm']"" />
          <b:property distinguished=""true"" xpath=""/*[local-name()='SendMessage' and namespace-uri()='http://navicongroup.ru/mdm']/*[local-name()='mailTo' and namespace-uri()='http://navicongroup.ru/mdm']"" />
        </b:properties>
      </xs:appinfo>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name=""message"" type=""xs:string"" />
        <xs:element name=""subject"" type=""xs:string"" />
        <xs:element name=""mailTo"" type=""xs:string"" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>";
        
        public CustomMailing() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [1];
                _RootElements[0] = "SendMessage";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
    }
}
