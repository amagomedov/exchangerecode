namespace ExchangeRecode {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [Microsoft.XLANGs.BaseTypes.DistinguishedFieldAttribute(typeof(System.String), "recodeRequest.OriginalXmlPath", XPath = @"/*[local-name()='RecodeMessage' and namespace-uri()='']/*[local-name()='recodeRequest' and namespace-uri()='']/*[local-name()='OriginalXmlPath' and namespace-uri()='']", XsdType = @"string")]
    [Microsoft.XLANGs.BaseTypes.DistinguishedFieldAttribute(typeof(System.String), "recodeRequest.CurrentXmlPath", XPath = @"/*[local-name()='RecodeMessage' and namespace-uri()='']/*[local-name()='recodeRequest' and namespace-uri()='']/*[local-name()='CurrentXmlPath' and namespace-uri()='']", XsdType = @"string")]
    [Microsoft.XLANGs.BaseTypes.DistinguishedFieldAttribute(typeof(System.String), "recodeRequest.OriginalDocPath", XPath = @"/*[local-name()='RecodeMessage' and namespace-uri()='']/*[local-name()='recodeRequest' and namespace-uri()='']/*[local-name()='OriginalDocPath' and namespace-uri()='']", XsdType = @"string")]
    [Microsoft.XLANGs.BaseTypes.DistinguishedFieldAttribute(typeof(System.String), "recodeRequest.CurrentDocPath", XPath = @"/*[local-name()='RecodeMessage' and namespace-uri()='']/*[local-name()='recodeRequest' and namespace-uri()='']/*[local-name()='CurrentDocPath' and namespace-uri()='']", XsdType = @"string")]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"RecodeMessageRequest", @"RecodeMessage"})]
    public sealed class RecodeMessageService : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xs:schema xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" xmlns:xs=""http://www.w3.org/2001/XMLSchema"">
  <xs:element name=""RecodeMessageRequest"" type=""RecodeMessageRequest"">
    <xs:annotation>
      <xs:appinfo>
        <b:recordInfo rootTypeName=""RecodeMessageRequest"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" />
      </xs:appinfo>
    </xs:annotation>
  </xs:element>
  <xs:complexType name=""RecodeMessageRequest"">
    <xs:sequence>
      <xs:element name=""OriginalXmlPath"" type=""xs:string"" />
      <xs:element name=""CurrentXmlPath"" type=""xs:string"" />
      <xs:element name=""OriginalDocPath"" type=""xs:string"" />
      <xs:element name=""CurrentDocPath"" type=""xs:string"" />
    </xs:sequence>
  </xs:complexType>
  <xs:element name=""RecodeMessage"">
    <xs:annotation>
      <xs:appinfo>
        <b:recordInfo rootTypeName=""RecodeMessage"" />
        <b:properties>
          <b:property distinguished=""true"" xpath=""/*[local-name()='RecodeMessage' and namespace-uri()='']/*[local-name()='recodeRequest' and namespace-uri()='']/*[local-name()='OriginalXmlPath' and namespace-uri()='']"" />
          <b:property distinguished=""true"" xpath=""/*[local-name()='RecodeMessage' and namespace-uri()='']/*[local-name()='RecodeMessageRequest' and namespace-uri()='']/*[local-name()='Message' and namespace-uri()='']"" />
          <b:property distinguished=""true"" xpath=""/*[local-name()='RecodeMessage' and namespace-uri()='']/*[local-name()='recodeRequest' and namespace-uri()='']/*[local-name()='CurrentXmlPath' and namespace-uri()='']"" />
          <b:property distinguished=""true"" xpath=""/*[local-name()='RecodeMessage' and namespace-uri()='']/*[local-name()='recodeRequest' and namespace-uri()='']/*[local-name()='OriginalDocPath' and namespace-uri()='']"" />
          <b:property distinguished=""true"" xpath=""/*[local-name()='RecodeMessage' and namespace-uri()='']/*[local-name()='recodeRequest' and namespace-uri()='']/*[local-name()='CurrentDocPath' and namespace-uri()='']"" />
        </b:properties>
      </xs:appinfo>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name=""recodeRequest"" type=""RecodeMessageRequest"" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>";
        
        public RecodeMessageService() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [2];
                _RootElements[0] = "RecodeMessageRequest";
                _RootElements[1] = "RecodeMessage";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
        
        [Schema(@"",@"RecodeMessageRequest")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"RecodeMessageRequest"})]
        public sealed class RecodeMessageRequest : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public RecodeMessageRequest() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "RecodeMessageRequest";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"",@"RecodeMessage")]
        [Microsoft.XLANGs.BaseTypes.DistinguishedFieldAttribute(typeof(System.String), "recodeRequest.OriginalXmlPath", XPath = @"/*[local-name()='RecodeMessage' and namespace-uri()='']/*[local-name()='recodeRequest' and namespace-uri()='']/*[local-name()='OriginalXmlPath' and namespace-uri()='']", XsdType = @"string")]
        [Microsoft.XLANGs.BaseTypes.DistinguishedFieldAttribute(typeof(System.String), "recodeRequest.CurrentXmlPath", XPath = @"/*[local-name()='RecodeMessage' and namespace-uri()='']/*[local-name()='recodeRequest' and namespace-uri()='']/*[local-name()='CurrentXmlPath' and namespace-uri()='']", XsdType = @"string")]
        [Microsoft.XLANGs.BaseTypes.DistinguishedFieldAttribute(typeof(System.String), "recodeRequest.OriginalDocPath", XPath = @"/*[local-name()='RecodeMessage' and namespace-uri()='']/*[local-name()='recodeRequest' and namespace-uri()='']/*[local-name()='OriginalDocPath' and namespace-uri()='']", XsdType = @"string")]
        [Microsoft.XLANGs.BaseTypes.DistinguishedFieldAttribute(typeof(System.String), "recodeRequest.CurrentDocPath", XPath = @"/*[local-name()='RecodeMessage' and namespace-uri()='']/*[local-name()='recodeRequest' and namespace-uri()='']/*[local-name()='CurrentDocPath' and namespace-uri()='']", XsdType = @"string")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"RecodeMessage"})]
        public sealed class RecodeMessage : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public RecodeMessage() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "RecodeMessage";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
    }
}
